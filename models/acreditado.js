const helperPipeDriveApi= require('../helpers/helperPipedriveApi');

async function emailExist(email){
    let users = await helperPipeDriveApi.pipeFindField({
      term: email ,
      exact_match: true,
      field_key: "email",
      field_type: "personField",
      return_item_ids: true
    });
    return users.length > 0;
}

module.exports={
    emailExist
}