const Pipedrive = require('pipedrive');
const pipedrive = new Pipedrive.Client(process.env.PD_API_TOKEN, { strictMode: true });
const util = require('util');


const pipePersonsGet = util.promisify(pipedrive.Persons.get);
const pipePersonsGetAll = util.promisify(pipedrive.Persons.getAll);
const pipeFindField = util.promisify(pipedrive.SearchResults.field);
const pipePersonAdd = util.promisify(pipedrive.Persons.add);
const pipePersonsUpdate = util.promisify(pipedrive.Persons.update);
const pipeFilesAdd= util.promisify(pipedrive.Files.add);
const pipeDealsAdd= util.promisify(pipedrive.Deals.add);
const pipePersonsDelete = util.promisify(pipedrive.Persons.remove);
const pipeDealsDelete = util.promisify(pipedrive.Deals.remove);
const pipeDealsGet = util.promisify(pipedrive.Deals.get);
const pipeDealsGetAll = util.promisify(pipedrive.Deals.getAll);
const pipeDealsUpdate = util.promisify(pipedrive.Deals.update);

module.exports={
    pipedrive,
    pipePersonsGet,
    pipePersonsGetAll,
    pipeFindField,
    pipePersonAdd,
    pipePersonsUpdate,
    pipeFilesAdd,
    pipeDealsAdd, 
    pipePersonsDelete,
    pipeDealsDelete,
    pipeDealsGet,
    pipeDealsGetAll,
    pipeDealsUpdate
};