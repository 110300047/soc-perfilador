const util = require('util');

let personGetDeals=async(person)=>{
    let deals=util.promisify(person.getDeals);
    return await deals();
}

module.exports={
    personGetDeals
}