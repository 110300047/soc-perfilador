const _ = require('lodash'),
    fs = require('fs'),
    FormData = require('form-data');



function addFileToPerson(filePath, personId) {
    return new Promise((resolve, reject) => {
        if (_.isNil(filePath) || filePath === '') {
            reject(new Error('filePath must be set.'));
            return;
        }
        if (!_.isNumber(personId) || personId < 1) {
            reject(new Error(`personId must be a positive number. You passed: ${personId}`));
            return;
        }

        let form = new FormData();
        form.append('person_id', personId);
        form.append('file', fs.createReadStream(filePath));
        let opts = {
            protocol: 'https:',
            host: 'api.pipedrive.com',
            path: `/v1/files?api_token=${process.env.PIPEDRIVE_API_TOKEN}`
        };
        form.submit(opts, (err, res) => {
            if (err) {
                reject(err);
                return;
            }

            if (res.statusCode !== 201) {
                reject(`The file upload should have returned a 201 status, but returned ${res.statusCode}.`);
                return;
            }

            let body = [];
            res.on('data', (chunk) => {
                body.push(chunk);
            });
            res.on('end', () => {
                body=body.join('');
                let json = JSON.parse(body);
                if (!json.data) {
                    reject('The file upload response JSON did not have a data property.');
                    return;
                }
                try {
                    fs.unlinkSync(filePath);
                }catch (e) {
                    reject (new Error("we had a problem sending and refreshing the file"));
                }

                resolve(json.data);
            });
        });
    });
}
function addFileToDeal(filePath, dealId) {
    return new Promise((resolve, reject) => {
        if (_.isNil(filePath) || filePath === '') {
            reject(new Error('filePath must be set.'));
            return;
        }
        if (!_.isNumber(dealId) || dealId < 1) {
            reject(new Error(`dealId must be a positive number. You passed: ${dealId}`));
            return;
        }

        // The Pipedrive Files Node.js SDK is broken with binary files
        // so we go straight to the API using an HTTPs client.
        let form = new FormData();
        form.append('deal_id', dealId);
        form.append('file', fs.createReadStream(filePath));
        let opts = {
            protocol: 'https:',
            host: 'api.pipedrive.com',
            path: `/v1/files?api_token=${process.env.PIPEDRIVE_API_TOKEN}`
        };
        form.submit(opts, (err, res) => {
            if (err) {
                reject(err);
                return;
            }

            if (res.statusCode !== 201) {
                reject(`The file upload should have returned a 201 status, but returned ${res.statusCode}.`);
                return;
            }

            let body = [];
            res.on('data', (chunk) => body.push(chunk));
            res.on('end', () => {
                let json = JSON.parse(body.join(''));
                if (!json.data) {
                    reject('The file upload response JSON did not have a data property.');
                    return;
                }
                fs.unlink(filePath);
                resolve(json.data);
            });
        });
    });
}

module.exports = {
    addFileToDeal,
    addFileToPerson
};