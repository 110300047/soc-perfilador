// ============================
//  PREPERFILAMIENTO
// ============================
/*================PREPERFILAMEINTO PERSONA=========================*/
const cuentaDeBancoMexicana = '77789d3b28d76aba8f61be0ff18291439bc621a7';
const enganche = '92736f140cb196ba2a68899d0f418ecfe2dfe6ab';
const costoDelProducto = 'c6a592c9427ba74d6100bfe546d99f6759faea2a';
const productoAAdquirir = 'ba2f0fd9c1d4ca289ed50d9a606b0cfc69138494';


const email = 'email';
const password = 'c27a19880800ebeebcd68beebfb4f56109bbafe6';
const phone = 'phone';
const completeName = 'name';
const fechaDeNacimiento = 'c0a5d2dcdfe43c0d8352a2864bff9f7415277796';
const nacionalidad ='95ff2bb045221c0ae71ad7cc676ef4c539fce10a';
const regimen = 'df097dd63967a61eee7d45ea47a8989370dec43d';
const estadoCivil = '70aa70721b85870b304034ae3c786999292f930b';
const formaMigratoria = 'f98f1638612ba589d45dd158138412ae6331d7ac';
//const deseaSerCont
//const ingresoAproximado
//const tieneHistorialCredi
//const empleadoConFormaPago
//const pagaPuntualCreditos
//const fechaAperturaCreditoBancario
//const depositaEnCuentaBancaria
const bancoDepositos = '6ea19b01ddbff17628600d423d21ce8a9a642f7c';
//const causaMontoMayorAtraso
//const causaMontoMayorAtrasoOtro
//const frecuenciaDeAtrasos
//const montoMayorAtraso

/*================PREPERFILAMEINTO TRATOS=========================*/
const nameDeal = 'title';
const personId= 'person_id';

const coacreditado = '0a738f639a6685c84ff82d402ca3452d778524ea';
//const tipoCoacreditado
//const tieneCoacreditado
const inputProduct = 'value';
const inputEnganche = 'b7d6f0136afc34f2d636ed8570ec9a49a443061c';
const typeProduct = '597b415a30390babc7586200f68eac9f64317d04';
//const cumpleIngreso


// ============================
//  PERFILAMIENTO
// ============================
/*================PERFILAMIENTO=========================*/
const altaEnHacienda = '16ca27798b539abc2b4fd814c2d28466a9d25c34';
const fechaDeAlta = '0c9c2a977f0291f109fbbd1f8c34f743abbbd5ff';
const ocupacionDeTrabajo = 'c50ac93dbf600d3024464f66cf23539c12a18c0d';
const antiguedadDeTrabajo = '0512708177777fc95a1dc20085a69717371a9739';
const numeroDeSeguroSocial = '27a1d8a2a5b2ee559ef2693b732fdc0ff063eec8';
const tipoPagadora = '971b47d6d56560ce0200a24184265c6a393b792b';
const banconomina = 'cf851387b84673b867a39eb8162a57a7a30db819';
const COFI = 'da2482a609a8b5f887041a0cb7bdb81bcc63c58a';
const fechaAltaSAT = 'f453bb01defd2605ea0aa0d617480cb56d11dd38';
const conSitucaionFiscal = '1c2600eb211fb4410fef6873f946a403a5195d8f';

const actividadLaboral = '1f74acbd39a65dfe5d0d6948217abb1551098eb8';
//const fechaDeAlta = '0c9c2a977f0291f109fbbd1f8c34f743abbbd5ff';
const actividadDeLaEmpresa = 'a5a5019bbfb17f3a1398c9a6fb60b1ad1c99f75f';
//const altaEnHacienda = '16ca27798b539abc2b4fd814c2d28466a9d25c34';
const base = 'c578d52b64555747948d240ac5c76745d522a1f1';
const comisiones = '79d976644612d13b76ff0ca3bde255c014742d54';
const bonos = 'ea6ee54789231f39246be7aa3033ac4c84262993';
const totalDeIngresos = 'f2cab165c9db1aefd6a8239688fc5a83fde26cf5';
const creditoEgresosBancarios = 'd5b6c67f6b7c130534f11c8b94c5c76a37b1f808';
const bancoEgresosBancarios = 'b5ceee44bb080679b5d67eeb989a620bf0d0efe7';
const fechaDeAperturaEgresosBancarios = '8389fa9a170b33bcbe826845aa2659a5db6a46b4';
const saldoActualEgresosBancarios = '7860258ff2d8bad118872eeba175fb06d1724bad';

/*================OTROS DATOS=========================*/
const prospecto = '5751894b489eba15076c297689b77bfcabc035f2';
const emailSecundario = 'f88820a34cf3a039d2aa4b246bed96ccf1dd6672';
const tipoDeUsuario = '8b2527258bf155acb4f178735661ab4bba5ebd73';

/*================Datos Complementarios - CONTACTO=========================*/
const telefonoCasa = 'a936be67455641da8b5ae2794a30699b8262b584';
const lugarDeNacimiento = 'e84ba7d19af748d0bfe8a1c0ff3ac528295677d6';
const tiempoVivirDomicilio = 'd13615327dcc581f687f9cd44c7a43c39dfe6490';
const tiempoVivirPoblacion = 'ce18fd54f332998bec00dc7cbe491998c2a04c73';
const escolaridad = '89c8bbbac1c63205fda27f7677ce83fb463fed91';
const titulo = '4f831324c73731f2a22c406ea221d0c921907c19';
const casaPropiaOHipotecada = 'cc812c53fa8d8c200ffc868d569c97c0c9493112';
const valorEstimadoDeCasa = 'af0c39decc9c480bc75d1dce25fbb348dbe21796';
const rentaDeCasa = '428c1a994d512ae8976c02ff3231a22d4b915592';
const dependienteEconomicoNombre1 = 'fd6437d9c902cb5803c844842484de2a65bcd05a';
const dependienteEconomicoNombre2 = 'd65bfff2f75ab279737ebf2b244de9130564c0ac';
const dependienteEconomicoNombre3 = '283c928aba2fa416afe1f6ae2fa959022a59f18e';
const dependienteEconomicoNombre4 = '2c4aedcaf1a164a0a1b277506fe9df7c608a8241';
const dependienteEconomicoEdad1 = '04f66b64c6a9d702a60852ec7d86f06147c433c0';
const dependienteEconomicoEdad2 = 'e00a1f854422511596da1d99536f7ec5d4a04303';
const dependienteEconomicoEdad3 = '71dfe79b0b775431bf80553ed1dc16498ebabc55';
const dependienteEconomicoEdad4 = 'eeecfec613d84990f0a19557faa99945973c7cf4';
const dependienteEconomicoOcupacion1 = 'ea77875eb9382ba0c5c6075bfe51a9f876f26a19';
const dependienteEconomicoOcupacion2 = '25c360bedb21fd15294c91d63f5a4ab086498d42';
const dependienteEconomicoOcupacion3 = '33abb41495ecbff11db9a4445a3042b286ace57d';
const dependienteEconomicoOcupacion4 = 'b1fc689913cab713d75874b94a3509aaa91248f0';
const dependienteEconomicoParentesco1 = '54d3707d745ece69fa7d8417aee2754dbbf45f46';
const dependienteEconomicoParentesco2 = '21a1355a69eb7cba1ee99ee1bdb29bac273ea59b';
const dependienteEconomicoParentesco3 = 'e2094f7036be1aff378536c5d49356519147d867';
const dependienteEconomicoParentesco4 = 'd189d68b0fd4055bcb7365f457548d8b0106a80d';
const peso = '02b3fce9466d3aac25c48ac8372c85ea306ea19e';
const estatura = '22d384283d0f20b235ffad0c3474489b24798fff';
const fumador='4c8809ab4f56961b68bd891379b8f46fe1df44ce';
const hospitalizadoConAnterioridad ='c55a5679da22574c121dbea489b05d2b7901c614';
const enfermedadCronica = 'c7d273396a545afbf73ed7ebe51ccfea1a0b4ff0';
const tipoDelBien1 = '97884966ddc1811d466f17872ec6a3a334c0f3d2';
const tipoDelBien2 = 'adc3bcf248fa30a37428aa1b9cd044a6f54eef7d';
const tipoDelBien3 = '2ec08c39cdf2a30bcb074170cbaf64ddbc32b6f4';
const tipoDelBien4 = 'f5967445560eb8598004759935f7f00a86c6db4d';
const valorDelBien1 = '4dff11a6dfe585f7edecc85922a39bb7f654f35e';
const valorDelBien2 = '62d5ef048b9ed78efb79eab34f4d8cb42275ef73';
const valorDelBien3 = '71eef24198aa342d42d8741c99983b6fa59ae8db';
const valorDelBien4 = '713ba465d9d0f0baa13bc8666a54b628dc400d0e';
const descripcionDelBien1 = 'ca4b4f5cfc79f487eed80d03ff4bad0df9687500';
const descripcionDelBien2 = '192964f7acd5d90665c12c909496d7f24b6d4972';
const descripcionDelBien3 = '329b1d7843db4f6d1b5198f1f27afd04f6aa8731';
const descripcionDelBien4 = '566c1f0802bc0f6f728f05ff74df9dd41acbfee1';

/*================Datos Complementarios - CREDITO=========================*/
const nombreReferenciaPersonal1 = '60f0d58c30a31348a6c6f1a9b3f48f0ed0c21f15';
const nombreReferenciaPersonal2 = 'cde7f15c70737b02044a880efe6d22cdfbceb41c';
const telefonosReferenciaPersonal1 = '8fdffe41a3f9af18cb2dd33af2c4700c43acf3dd';
const telefonosReferenciaPersonal2 = '3c21a617684d990caedd56410312fdfc1a75a085';
const tiempoConocidoReferenciaPersonal1 = '18ba6381cfcff6c0271f31292cea8843512887bb';
const tiempoConocidoReferenciaPersonal2 = '4c7d7538848f193affa538dc7c79f4da9ff15d1d';
const parentescoReferenciaPersonal1 = '1a97bab36fe9628f851436bfe921297adb49384d';
const parentescoReferenciaPersonal2 = '3169d2817c464799f3ae5359bbbaadbb3c6ad8c9';
const institucionDelCredito1 = '07103b3172d00769ac56762d52fd2c23565ad958';
const institucionDelCredito2 = '597ebb083768b0a71107f5979214da19de3036b6';
const institucionDelCredito3 = '1fe365657919127d17a13bc3bac70d9edd68eaaf';
const institucionDelCredito4 = 'bb265f48ec1d3414ab3b800651774dd1ea5f9560';
const numeroDeCuentaDelCredito1 = '26c3f8adf0bccd864ee062c773ab65dd25059672';
const numeroDeCuentaDelCredito2 = '1b01b6ce8d9268ff8f285a85354eb2c5009b6806';
const numeroDeCuentaDelCredito3 = '21add45c0761cc81d748bce8d5bd8c3f8ae7a132';
const numeroDeCuentaDelCredito4 = '5d87200c1870d4886ae0ca659fe4b1c07b7564b2';
const tipoDeCredito1 = '9127856fcecd0ec727dcbf1a696ad64248f128fe';
const tipoDeCredito2 = 'aff5c57020587dbe3ffd98142f688fffa63bfce3';
const tipoDeCredito3 = '1b62d9cc8452cac7473d0ecafc9f82a0b63eb4b5';
const tipoDeCredito4 = '5d366d60486344186108d6ecc6af302d69d981aa';
const fechaDeAperturaDelCredito1 = 'c0207d098b7a7dfb5f4005d8c18cc2527797b166';
const fechaDeAperturaDelCredito2 = 'bc3ee677c62df65ed18ee3f5d345b081cc61a357';
const fechaDeAperturaDelCredito3 = '445bc4fdb55a56599c7627930941edc1a05197dd';
const fechaDeAperturaDelCredito4 = 'fba29db1ca14c2a8ddedba304d267238a5a4157b';
const nombreDeLaEmpresaLaboral = '6266142455d3c48cffac049cb20202a38cdf465e';
const direccionCompletaLaboral = '444bd34948b850533613b24db4f8f5fcc1bec700';
const fechaDeIngresoLaboral = '56042640186214a72b29bf78bde856704eb70627';
const telefonoLaboral = '75fc6d218b5282bb3dd02439b0ead693ba1ce8d9';
const areaDepartamentoLaboral = '603ccc3831d276c3c34dd998b41003b0ba95ceff';
const profesionLaboral = '5d1580d50f3b64ab377c4a48f2efe6a33cfe1349';
const puestoLaboral = 'd6ea71794f8b854536a58b244eadcdf0c190a33f';
const antiguedadLaboral = '976fb10f3ab94820a8db45705500c8f4c804eaf1';
const funcionesLaborales = '743feddc5a11187065c1fc6252dec68d565190a8';

module.exports={
    nameDeal,
    personId,
    email,
    password,
    phone,
    completeName,
    formaMigratoria,
    cuentaDeBancoMexicana,
    fechaDeNacimiento,
    nacionalidad,
    regimen,
    estadoCivil,
    enganche,
    costoDelProducto,
    productoAAdquirir,
    coacreditado,
    inputProduct,
    inputEnganche,
    typeProduct,


    ocupacionDeTrabajo,
    antiguedadDeTrabajo,
    numeroDeSeguroSocial,
    tipoPagadora,
    banconomina,
    COFI,
    fechaAltaSAT,
    conSitucaionFiscal,
    bancoDepositos,
    actividadLaboral,
    fechaDeAlta,
    actividadDeLaEmpresa,
    altaEnHacienda,
    base,
    comisiones,
    bonos,
    totalDeIngresos,
    creditoEgresosBancarios,
    bancoEgresosBancarios,
    fechaDeAperturaEgresosBancarios,
    saldoActualEgresosBancarios,

    prospecto,
    emailSecundario,
    tipoDeUsuario,
    telefonoCasa,
    lugarDeNacimiento,
    tiempoVivirDomicilio,
    tiempoVivirPoblacion,
    escolaridad,
    titulo,
    casaPropiaOHipotecada,
    valorEstimadoDeCasa,
    rentaDeCasa,
    dependienteEconomicoNombre1,
    dependienteEconomicoNombre2,
    dependienteEconomicoNombre3,
    dependienteEconomicoNombre4,
    dependienteEconomicoEdad1,
    dependienteEconomicoEdad2,
    dependienteEconomicoEdad3,
    dependienteEconomicoEdad4,
    dependienteEconomicoOcupacion1,
    dependienteEconomicoOcupacion2,
    dependienteEconomicoOcupacion3,
    dependienteEconomicoOcupacion4,
    dependienteEconomicoParentesco1,
    dependienteEconomicoParentesco2,
    dependienteEconomicoParentesco3,
    dependienteEconomicoParentesco4,
    peso,
    estatura,
    fumador,
    hospitalizadoConAnterioridad,
    enfermedadCronica,
    tipoDelBien1,
    tipoDelBien2,
    tipoDelBien3,
    tipoDelBien4,
    valorDelBien1,
    valorDelBien2,
    valorDelBien3,
    valorDelBien4,
    descripcionDelBien1,
    descripcionDelBien2,
    descripcionDelBien3,
    descripcionDelBien4,


    nombreReferenciaPersonal1,
    nombreReferenciaPersonal2,
    telefonosReferenciaPersonal1,
    telefonosReferenciaPersonal2,
    tiempoConocidoReferenciaPersonal1,
    tiempoConocidoReferenciaPersonal2,
    parentescoReferenciaPersonal1,
    parentescoReferenciaPersonal2,
    institucionDelCredito1,
    institucionDelCredito2,
    institucionDelCredito3,
    institucionDelCredito4,
    numeroDeCuentaDelCredito1,
    numeroDeCuentaDelCredito2,
    numeroDeCuentaDelCredito3,
    numeroDeCuentaDelCredito4,
    tipoDeCredito1,
    tipoDeCredito2,
    tipoDeCredito3,
    tipoDeCredito4,
    fechaDeAperturaDelCredito1,
    fechaDeAperturaDelCredito2,
    fechaDeAperturaDelCredito3,
    fechaDeAperturaDelCredito4,
    nombreDeLaEmpresaLaboral,
    direccionCompletaLaboral,
    fechaDeIngresoLaboral,
    telefonoLaboral,
    areaDepartamentoLaboral,
    profesionLaboral,
    puestoLaboral,
    antiguedadLaboral,
    funcionesLaborales
};
