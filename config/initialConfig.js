// ============================
//  Puerto
// ============================
process.env.PORT = process.env.PORT || 4000;
// ============================
//  Entorno
// ============================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';
// ============================
//  Vencimiento del Token
// ============================
// 60 segundos
// 60 minutos
// 24 horas
// 30 días
process.env.CADUCIDAD_TOKEN = '48h';
// ============================
//  SEED de autenticación
// ============================
process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo';
// ============================
// URL del servidor
// ============================
let urlServer;
if (process.env.NODE_ENV === 'dev') {
    urlServer = `http://localhost:${process.env.PORT}/`;
} else {
    urlServer = `https://proto-soc.herokuapp.com/`;
}
process.env.URL_SERVER = urlServer;
// ============================
//  API Token PipeDrive
// ============================
process.env.PD_API_TOKEN='b6406ce6aca39771efb98e3960ce9b62d4d0957f';