require('./config/initialConfig');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const path=require('path');
const indexRouter=require('./routes/index');
const pipeFunctionApi=require('./config/pipedrive-file-uploader');
const fileUpload=require('express-fileupload');
//const expressValidator = require('express-validator');

app.use(cors());
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: false }));
app.use(fileUpload());
//app.use(expressValidator());
app.use('/api',indexRouter);

if (process.env.NODE_ENV==='production'){
    app.use(express.static(__dirname+'/client/build'));
    app.get('*',(req,res)=>{
        res.sendFile(path.resolve(__dirname,'client','build','index.html'));
    });
}

app.listen(process.env.PORT, function() {
    console.log(__dirname+'client');
    console.log("Server is running on Port: " + process.env.PORT);
});


/*Aun falta verificar extensiones del archivo y falta seguridad (tokens)*/
app.post('/api/user/upload',(req, res)=>{
    console.log(req.body);
    if(!req.files){
        return res.status(400)
            .json({
                ok:"false",
                error:{message:"No file was selected"}
            });
    }
    if(!req.body.idPerson){
        return res.status(400)
            .json({
                ok:"false",
                error:{message:"You need provide an Id person"}
            });
    }
    let idPerson=parseInt(req.body.idPerson);

    let archivo=req.files.file;
    let path=`upload/${archivo.name}`;
    console.log("aca");
    archivo.mv(path,(err)=>{
        if (err){
            return res.status(500)
                .json({
                    ok:"false",
                    err
                });
        }
        pipeFunctionApi.addFileToPerson(path,idPerson).then(data=>{
            console.log('yes');
            res.send(data);

        })
            .catch(error=>{
                console.log('nada');
                res.status(500).json({
                    message:"Your file was´nt upload to pipedrive"
                });
            });
    });
});