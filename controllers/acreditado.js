const jwt = require('jsonwebtoken');
const validations=require('../routes/validations');
const bcrypt=require('bcryptjs');
const { promisify } = require("util");
const helperPipeDriveApi=require('../helpers/helperPipedriveApi');
const campos=require('../config/camposPersonalizados');
const {validationResult} = require('express-validator');
const moment = require('moment')

async function registerUser(req,res){
    let userData = req.body;
    let result= validationResult(req);
    if (!result.isEmpty()) {
        let error = result.array().map(i => `error en ${i.param} ${i.msg}`).join(', ');
        return res.status(400).json({ status: 'failed', message: error })
    }
    //return res.status(200).json({ status: 'sucessfull'});
      //console.log("req: ",req.body);
    /*try{
        let user= await userController.registerUser(userData);
        //console.log("user",user);
        if (user.status==="failed")return res.status(400).json(user)
        return res.send(user);
      }catch (e){
        console.log(e);
        return res.status(500).json({
          status:"failed",
          error:{message:"Opss algo salio mal con pipedrive"}
          })
      }
    
    
    //console.log("es valido?: ",isValitedEmail);
    //console.log(isValitedata);
    if(isValitedata.status==="failed"){
      //console.log("te amo alexis");
      return isValitedata;
    }*/
    let salt = bcrypt.genSaltSync(10);
    let user = {};
    user[campos.completeName]=userData.payload.completeName.toUpperCase();
    user[campos.password]=bcrypt.hashSync(userData.payload.password,salt);
    user[campos.email]=[userData.payload.email];
    user[campos.phone]=[userData.payload.cellPhone];
    user[campos.fechaDeNacimiento]=moment(userData.payload.fechaNac,"DD/MM/YYYY");
    user[campos.nacionalidad]=userData.payload.nacionalidad;
    user[campos.regimen]=userData.payload.regimen;
    user[campos.estadoCivil]=userData.payload.estadoCivil;
    user[campos.formaMigratoria]=userData.payload.ExFoMi.toUpperCase();
    user[campos.bancoDepositos]=userData.payload.baPri.toUpperCase();
    let register;
    console.log(user);
    try{
        console.log("aca1");
        register = await helperPipeDriveApi.pipePersonAdd(user);
        console.log("aca2");
    }catch(e){
        console.log(e);
        return res.status(400).json({ status: 'failed', message: error })
    }
    
    /*let deal = {
        "title":"Trato "+register.name,
        'person_id': register.id,
        '597b415a30390babc7586200f68eac9f64317d04' : userData.payload.typeProduct, //
        "value":userData.payload.inputProduct,
        'b7d6f0136afc34f2d636ed8570ec9a49a443061c' : userData.payload.inputEnganche//
    };
    
    let sessionToken={
        id:register.id,
        email : register.email[0].value
    }
    let token = jwt.sign({
        usuario: sessionToken
    }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });*/
      
    let object = {
        register,
        //register2,
        status:'sucessfull',
        message: 'ok',
        //token
    };
    return object;  
  }

async function productoUpdate(dealData,sessionToken){
  let isValitedata = await validations.validateProductUpdate(dealData.payload);
  //console.log("es valido?: ",isValitedEmail);
  // console.log(isValitedata);
  if(isValitedata.status==="failed"){
    return isValitedata;
  }
  let idDeal=dealData.payload.idDeal;
  //console.log("SessionToken",sessionToken.id);
  //console.log("Deal ID",idDeal);
  let idUser = sessionToken.id;
  let userPerson=await helperPipeDriveApi.pipePersonsGet(idUser);
  let getDeals=promisify(userPerson.getDeals)
  let deals=await getDeals();
  let deal;  
  for (let i =0; i<deals.length;i++){
    if (deals[i].id===idDeal){
      deal=deals[i];
      break;
    }
  }
  
  if(deal){
    let dealUpdateData = {
      '597b415a30390babc7586200f68eac9f64317d04' : "100", //
      "value":"150",//
      'b7d6f0136afc34f2d636ed8570ec9a49a443061c' : "Construccion"//
    };
    let dealUpdate = await helperPipeDriveApi.pipeDealsUpdate(deal.id, dealUpdateData);
    let object = {
      status:'sucessfull',
      message: 'Se actualizo el producto del Crédito'
    };
    return object
  }
  return{
    status:'failed',
    message: 'No se encontro el crédito'
  };
}

async function complementData(userData,sessionData){ 
  /* Para saber si es mayor de edad
  var isMayor = Math.abs(new Date() - new Date(Fecha_nac)) / 31536000000 ;
  if(isMayor >= 18.0){}
  */
  let idUser =sessionData.id;
  if(idUser > 0){
    let user = {
      'f98f1638612ba589d45dd158138412ae6331d7ac' : userData.payload.ExFoMi,//forma migratoria
      '77789d3b28d76aba8f61be0ff18291439bc621a7' : userData.payload.ExCuBaMe,//Cuneta bancaria mexicana
      'c0a5d2dcdfe43c0d8352a2864bff9f7415277796' : userData.payload.fechaNac,
      '95ff2bb045221c0ae71ad7cc676ef4c539fce10a' : userData.payload.nacionalidad,
      'df097dd63967a61eee7d45ea47a8989370dec43d' : userData.payload.regimen,
      '70aa70721b85870b304034ae3c786999292f930b' : userData.payload.estadoCivil
    };
    let update = await helperPipeDriveApi.pipePersonsUpdate(idUser, user);
    console.log("Ya actualizo", update);
    let object={
      status:'sucessfull',
      message: 'ok'
    };
    return object
  }
  return{
    status:'failed',
    message: 'No se actualizo el usuario'
  };
}

module.exports={
    registerUser,
    productoUpdate,
    complementData
}