const express = require("express");
const router = express.Router();
const userController=require("../controllers/acreditado");
const pipeDriveApi=require('../helpers/helperPipedriveApi');
const helperPerson=require("../helpers/helperPerson");
const modelAcreditado=require("../models/acreditado");
const _=require('lodash');
const {verificaToken}= require("../config/autentificacion");
const passwordValidator = require("password-validator");
const {body} = require('express-validator');



let schema = new passwordValidator();
schema
  .is().min(6)
  .is().max(16)
  .has().digits()
  .has().not().spaces();

let validate = (method) => {
  switch (method) {
    case "createUser": {
      // console.log("entre1");
        return [
        //body("payload.email","email invalido").trim(),
        body("payload.email", "email invalido").trim().isEmail().custom(async email => {
            if (await modelAcreditado.emailExist(email)) throw new Error("email registrado");
            else return true;
        }),
        body("payload.password").custom(password => {
            if (!schema.validate(password)) throw new Error("password debe tener entre 6 y 16 caracteres, algún numero y letra");
            else return true;
        }),
        body("payload.cellPhone", "es campo obligatorio").isMobilePhone(),
        body("payload.completeName", "nombre invalido").isString(),      
        body("payload.inputProduct","es campo obligatorio y numerico").isNumeric(),
        body("payload.inputEnganche", "es campo obligatorio, numerico y debe ser menor a inputProduct").
        isNumeric().custom((enganche,{req})=>{
            if(enganche>=req.body.payload.inputProduct) throw new Error("debe ser menor a inputProduct");
            else return true;
        }),
        body("payload.typeProduct", "es campo obligatorio").isString(),
        body("payload.cumpleIngreso","es campo obligatorio").isString(),
        body("payload.tieneCo","es campo obligatorio").isString(),
        body("payload.tipoCoacreditado","debe ser texto").optional().isString(),
        body("payload.ingresoAprox","debe ser numerico").optional().isNumeric(),
        body("payload.estadoCivil","es campo obligatorio").isString(),
        body("payload.regimen","debe ser texto").optional().isString(),
        body("payload.nacionalidad","es campo obligatorio").isString(),
        body("payload.fechaNac","es campo obligatorio").isString(),
        body("payload.desContact","debe ser texto").optional().isString(),

        body("payload.ExFoMi","debe ser texto").optional().isString(),
        //body("payload.altaHac","debe ser texto").optional().isString(),
        //body("payload.feAlta","debe ser texto").optional().isString(),
        body("payload.HiCre","es campo obligatorio").isString(),
        body("payload.EmFoPa","debe ser texto").optional().isString(),
        body("payload.PaPuCre","es campo obligatorio").isString().not().isEmpty(),
        body("payload.feApCreBan","debe ser texto").optional().isString(),
        body("payload.DeInCuBa","debe ser texto").isString().not().isEmpty(),
        body("payload.baPri","debe ser texto").optional().isString(),
        //body("payload.CoHiCre","debe ser texto").optional().isString(),
        //body("payload.CoEmFoPa","debe ser texto").optional().isString(),
        //body("payload.CoPaPuCre","debe ser texto").optional().isString(),
        //body("payload.CofeApCreBan","debe ser texto").optional().isString(),
        //body("payload.CoDeInCuBa","debe ser texto").optional().isString(),
        //body("payload.CobaPri","debe ser texto").optional().isString(),
        body("payload.causaMontoMayor","debe ser texto").optional().isString(),
        body("payload.causaMontoText","debe ser texto").optional().isString(),
        body("payload.frecuencia","debe ser texto").optional().isString(),
        body("payload.montoMayorAprox","debe ser texto").optional().isInt()
      ];
    };
    case "loginUser": {
      //console.log("entre2");
        return [body("email", "formato invalido").isEmail(),
        body("idDevice", "es obligatorio").exists(),
        body("password", "obligatorio").exists(),
        body("instrumentoId", "obligatorio").isString()];
    }
  }
}
/* GET users listing. Test API */
router.get('/:id?', async function(req, res) {
  console.log(req.params.id);
  
  //obtener deals de una persona
  let personBefore=await pipeDriveApi.pipePersonsGet(req.params.id);
  deals=await helperPerson.personGetDeals(personBefore);
  //console.log(deals);

  let object={
    status: 'ok',
    message:'Api online 1',
    ok:'ok',
    personBefore
  };

  res.send(object);
});

router.post('/add',validate('createUser'),userController.registerUser);

//Update producto SOLO PARA EL PREPERFILAMIENTO
router.put('/productoUpdate',verificaToken,async (req,res)=> {
  let dealData = req.body;
  let sessionToken=req.usuario;
  //console.log("token info",req);
  /*try{
    propspectoUpdate=await userController.productoUpdate(dealData,sessionToken);
    if (propspectoUpdate.status=="failed") return res.status(400).json(propspectoUpdate)
    else return res.json(propspectoUpdate)
  }catch(e){
    console.log(e);
    return res.status(500).json({
      ok:"false",
      error:{message:"Opss algo salio mal con pipedrive"}
      })
  }*/
  return res.status(200).json({ status: 'sucessfull'});

});

//Update a person info in preperfilamiento
router.put('/complementData',verificaToken,async (req,res)=> {
  //console.log("update: ",req.usuario);
  let userData = req.body;
  try{
    let user= await userController.complementData(userData,req.usuario);
    if (user.status==="failed")return res.status(400).json(user)
    return res.send(user);
  }catch (e){
    return res.status(500).json({
      ok:"false",
      error:{message:"Opss algo salio mal con pipedrive"}
      })
  }
});






/*async function isValidateUpdate(email){
  let users = await pipeDriveApi.pipeFindField({
    term: email,
    exact_match: true,
    field_key: "email",
    field_type: "personField",
    return_item_ids: true
  });
  if (users.length>0){
    return users[0].id;
  }
  return false;
}*/


router.post ('/deal/add',(req,res)=>{
  let title=req.body.title;
  let value=req.body.value;
  let status=req.body.status;
  let stageId=req.body.stageId;
  console.log("stage: ",stageId);
  pipeDriveApi.pipeDealsAdd({title,value,status,stage_id:stageId})
      .then((data)=>res.send(data))
      .catch(err=>res.send(err));
});

//Update perfilamiento
router.put('/secondUpdate',(req,res)=> {
  console.log("update:",req.body);
  let userData=req.body;
  secondUpdate(userData).then(messag=> {
    res.send(messag);
  }).catch(err=>console.log(err));
});

//Update prospecto
router.put('/prospectoUpdate',(req,res)=> {
  let userData = req.body;
  propspectoUpdate(userData).then(messag=> {
    res.send(messag);
  }).catch(err=>console.log(err));
});

async function propspectoUpdate(userData){
  let idUser = await isValidateUpdate(userData.payload.email);
  if (idUser > 0) {
    let user = {
      '5751894b489eba15076c297689b77bfcabc035f2' : "Si"
    };
    let updatePerfilamiento = await pipeDriveApi.pipePersonsUpdate(idUser, user);
    let object = {
      updatePerfilamiento,
      status:'sucessfull',
      message: 'Se actualizo el prospecto'
    };
    return object;
  }
  return{
    status:'failed',
    message: 'No se encontro el usuario'
  };
}
//Eliminar usuario con Correo Electronico
router.delete('/deleteUser',(req,res)=> {
  let {email} = req.body;
  deleteUser(email).then(messag=> {
    res.send(messag);
  }).catch(err=>console.log(err));
});

async function deleteUser(email){
  let idUser = await isValidateUpdate(email);
  if (idUser===null || idUser===false){
    return{
      status:'failed',
      message: 'Correo no registrado'
    };
  }else{
    let deleted = await pipeDriveApi.pipePersonsDelete(idUser);
    return {
      deleted,
      status:'sucessfull',
      message: 'Usuario eliminado'
    };
  }
}

//Eliminar Deal
router.delete('/deleteDeal',(req,res)=> {
  let {email} = req.body;
  deleteDeal(email).then(messag=> {
    res.send(messag);
  }).catch(err=>console.log(err));
});

async function deleteDeal(email){
  let idUser = await isValidateUpdate(email);
  if (idUser===null || idUser===false){
    return{
      status:'failed',
      message: 'Usuario no registrado'
    };
  }else{
    let user = await pipeDriveApi.pipePersonsGet(idUser); 
    let name = user.name;
    name = "Trato "+name;
    let idDeal = await isValidateDeal(name); 
    if (idDeal===null || idDeal===false){
      return {
        status:'failed',
      message: 'Credito no registrado'
      };
    }else{
      let idDeal = await isValidateDeal(name);
      if (idDeal===null || idDeal===false){
        return {
          status:'failed',
          message: 'Credito no registrado'
        };
      }else{
        let data = await pipeDriveApi.pipeDealsDelete(idDeal); 
        return {
          data,
          status:'sucessfull',
         message: 'Credito eliminado'
        };
      }
    }
  }
}

async function isValidateDeal(name){
  let users = await pipeDriveApi.pipeFindField({
    term: name,
    exact_match: true,
    field_key: "title",
    field_type: "dealField",
    return_item_ids: true
  });
  if (users.length > 0) {
    return users[0].id;
  }
  return false;
}
module.exports = {
  router
}
//PERFILAMIENTO
async function secondUpdate(userData){
  let idUser = await isValidateUpdate(userData.payload.email);
  if (idUser > 0) {
    let user = {
      c50ac93dbf600d3024464f66cf23539c12a18c0d: userData.payload.ocupacion,
      "0512708177777fc95a1dc20085a69717371a9739": userData.payload.antiguedad,
      "27a1d8a2a5b2ee559ef2693b732fdc0ff063eec8": userData.payload.numeroSeguro,
      "971b47d6d56560ce0200a24184265c6a393b792b": userData.payload.tipoPagadora,
      cf851387b84673b867a39eb8162a57a7a30db819: userData.payload.baNomina,
      da2482a609a8b5f887041a0cb7bdb81bcc63c58a: userData.payload.COFI,
      f453bb01defd2605ea0aa0d617480cb56d11dd38: userData.payload.fechaSat,
      "1c2600eb211fb4410fef6873f946a403a5195d8f": userData.payload.CoSiFis,
      "6ea19b01ddbff17628600d423d21ce8a9a642f7c": userData.payload.baDepos,
      "1f74acbd39a65dfe5d0d6948217abb1551098eb8": userData.payload.actividadLab,
      "0c9c2a977f0291f109fbbd1f8c34f743abbbd5ff": userData.payload.fechaAlta,
      a5a5019bbfb17f3a1398c9a6fb60b1ad1c99f75f:
        userData.payload.actividadEmpresa,
      "16ca27798b539abc2b4fd814c2d28466a9d25c34":
        userData.payload.altaHaciendaAct,
      c578d52b64555747948d240ac5c76745d522a1f1: userData.payload.acreBase,
      "79d976644612d13b76ff0ca3bde255c014742d54":
        userData.payload.acreComisiones,
      ea6ee54789231f39246be7aa3033ac4c84262993: userData.payload.acreBonos,
      f2cab165c9db1aefd6a8239688fc5a83fde26cf5: userData.payload.acreTotal,
      d5b6c67f6b7c130534f11c8b94c5c76a37b1f808: userData.payload.creditExpenses,
      b5ceee44bb080679b5d67eeb989a620bf0d0efe7: userData.payload.bankExpenses,
      "8389fa9a170b33bcbe826845aa2659a5db6a46b4": userData.payload.dateExpenses,
      "7860258ff2d8bad118872eeba175fb06d1724bad":
        userData.payload.balanceExpensess
    };
    let updatePerfilamiento = await pipeDriveApi.pipePersonsUpdate(
      idUser,
      user
    );
    let object = {
      updatePerfilamiento,
      status: "sucessfull",
      message: "ok"
    };
    return object;
  }
  return{
    status:'failed',
    message: 'No se actualizo el usuario'
  };
}


//Get user
router.post("/userLogin", (req, res) => {
  const { email, pass } = req.body.payload;

  //si los campos estan vacíos entonces retorna 404
  if (email == null || pass == null || email === "" || pass === "") {
    console.log("vacio");
    return res.status(404).send("El usuario o password esta vacio");
  }
  userLogin(email, pass).then(messag=> {
    //Si no se pueder loguear el usuario, recibimos el error y lo enviamos al front
  	if (messag.status=='failed'){
      console.log("entre if");
  		res.status(404).send(messag.message);
  	}else{
      console.log("entre else");
  		res.send(messag);
  	}    
  }).catch(err=>console.log(err));
});

async function userLogin(email, pass) {
  let idUser = await isValidateUpdate(email);
  //error correo no registrado
  if (idUser===null || idUser===false){
    return{
      status:'failed',
      message: 'Correo no registrado'
    }; 
  }
  //error al conseguir el password del usuario
  let dataUser = await isRegister(idUser);
  if (dataUser===null || dataUser===false){
    return{
      status:'failed',
      message: 'Usuario no registrado con constraseña'
    }; 
  }
  //logueo exitoso
  if (bcrypt.compareSync(pass, dataUser)) {
    let object = {
      status: "sucessfull",
      message: "Bienvenido"
    };
    return object;
  }
  //error contraseña incorrecta
  return{
    status:'failed',
    message: 'constraseña incorrecta'
  };  
}

async function isRegister(id) {
  let users = await pipeDriveApi.pipePersonsGet(id);
  if (users) {
    return users.c27a19880800ebeebcd68beebfb4f56109bbafe6; //contacto(usuario) campo contraseña
  }
  return false;
}


