const express=require('express');
const router=express.Router();
const helperPipeDriveApi=require('../helpers/helperPipedriveApi');
const loginController=require('../controllers/login');

router.post('/userLogin',(req,res)=> {
    const {email, pass} = req.body.payload;
    //console.log(email,pass);
    //si los campos estan vacíos entonces retorna 404
    if (email==null || pass==null || email==="" || pass===""){
      //console.log("vacio");
       return res.status(404).json({status:'failed',messag:"El usuario o password esta vacio"});
    }
    loginController.userLogin(email, pass).then(messag=> {
      //Si no se pueder loguear el usuario, recibimos el error y lo enviamos al front
        
      if (messag.status=='failed'){
        
            res.status(404).json(messag);
        }else{
        console.log("entre else");
            res.json(messag);
        }    
    }).catch(err=>{
      console.log("router",err)
      return res.status(500).json({
      ok:"false",
      error:{message:"Opss algo salio mal con pipedrive"}
      });
    });
  });

  
  
  module.exports={
      router
  }