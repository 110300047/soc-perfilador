const express = require('express');
const router = express.Router();
const routerUser=require('./acreditado').router
const routerLogin=require('./login').router


router.use("/user",routerUser);
router.use("/login",routerLogin);

/* GET home page. */
router.get('/', function(req, res) {
  //res.json({message:"its work"})
  res.redirect(process.env.URL_SERVER+'/signin');
});

module.exports = router;
