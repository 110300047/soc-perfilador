const _=require('lodash');
const passwordValidator = require('password-validator');
const helperPipeDriveApi=require('../helpers/helperPipedriveApi');

async function validateRegisterData(data){
    let schema= new passwordValidator();
  schema
  .is().min(6)
  .is().max(25)
  .has().digits()
  .has().not().spaces();
  //console.log(data);
  if (_.isNil(data.email)||data.email===""){
    return{
      status:'failed',
      message: 'Correo electronico vacio'
    }
  }
  if (_.isNil(data.completeName)||data.completeName==="" || hasNumber(data.completeName)){
    return{
      status:'failed',
      message: 'Nombre incorrecto (vacio o numerico)'
    }
  }
  /*if (_.isNil(data.cellPhone)||data.cellPhone===""){
    return{
      status:'failed',
      message: 'Telefono es obligatorio'
    }
  }*/
  if (_.isNil(data.password)
  || !schema.validate(data.password)){
    return{
      status:'failed',
      message: 'Pass incorrecto'
    }
  }
  if(!_.isNumber(data.inputProduct)){
    return{
      status:'failed',
      message: 'precio de producto debe ser numero'
    }
  }
  if(!_.isNumber(data.inputEnganche)){
    return{
      status:'failed',
      message: 'enganche de producto debe ser numero'
    }
  }
  if(_.isNil(data.typeProduct)||data.typeProduct===""){
    return{
      status:'failed',
      message: 'Tipo de producto es obligatorio'
    }
  }
  if(data.accept!==true){
    return{
      status:'failed',
      message: 'Terminos no aceptados'
    }
  }
  if(data.inputEnganche > data.inputProduct){
    return{
      status:'failed',
      message: 'El enganche no puede ser mayor al prestamo'
    }
  }
  let isValitedEmail=false;
  try{
    isValitedEmail= await isValidate(data.email);
  }catch(e){
    return{
        status:'failed',
        message: e
      }
  }
  
  //console.log("es valido?: ",isValitedEmail);
  //console.log(userData.payload.email);
  if(!isValitedEmail){
  //if (true){
      //console.log("entre");
    return{
      status:'failed',
      message: 'Correo electronico sin formato o previamente registrado'
    }
  }
  return{
    status:'correct',
    message: 'Datos correctos'
  }
}

function hasNumber(cadena){
    let regexhasNumber= /\d+/;
    //console.log(regexhasNumber.test(cadena));
    return regexhasNumber.test(cadena);
  }

async function isValidate(email){
    
    if (!hasFormat(email)){
      return false;
    }
    let users = await helperPipeDriveApi.pipeFindField({
      term: email ,
      exact_match: true,
      field_key: "email",
      field_type: "personField",
      return_item_ids: true
    });
    //console.log(users);
    //console.log(users.length);
    return users.length <= 0;
}

async function isValidateLogin(email){
    
  if (!hasFormat(email)){
    return false;
  }
  let users = await helperPipeDriveApi.pipeFindField({
    term: email ,
    exact_match: true,
    field_key: "email",
    field_type: "personField",
    return_item_ids: true
  });
  //console.log(users[0].id);
  //console.log(users.length);
  return users.length>0?users[0]:false;
}

function hasFormat(email){
    let expReg=/^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    return expReg.test(email);
}

function validateProductUpdate(data){
  if(!_.isNumber(data.inputProduct) || _.isNil(data.inputProduct)){
    return{
      status:'failed',
      message: 'precio de producto debe ser numero'
    }
  }
  if(!_.isNumber(data.inputEnganche)|| _.isNil(data.inputEnganche)){
    return{
      status:'failed',
      message: 'enganche de producto debe ser numero'
    }
  }
  if(_.isNil(data.typeProduct)||data.typeProduct===""){
    return{
      status:'failed',
      message: 'Tipo de producto es obligatorio'
    }
  }
  if(data.inputEnganche > data.inputProduct){
    return{
      status:'failed',
      message: 'El enganche no puede ser mayor al prestamo'
    }
  }
  return{
    status:'correct',
    message: 'Datos correctos'
  }
}

module.exports={
    validateRegisterData,
    validateProductUpdate,
    hasFormat,
    hasNumber,
    isValidateLogin
}