import axios from "axios";
import { getToken } from "./auth";
const port = process.env.PORT || 4000; //Para localhost
//const API_BASE = `https://proto-soc.herokuapp.com:${port}`; //Para heroku
const API_BASE = `http://localhost:${port}`;//localhost

// https://codeburst.io/deploying-app-to-heroku-rails-backend-react-frontend-8f7e5c1ffc19

axios.defaults.baseURL = API_BASE; //Pruebas para localhost, se comenta para hacer subir a heroku
const get = (url, params = {}) => axios.get(url, { params });
const put = (url, params = {}) => axios.put(url, params);
const post = (url, params = {}) => axios.post(url, params);

export default {
  API_BASE,
  getToken,
  login: {
    do: ({ email, pass }) => {
      console.log(email, pass);
      return post(`/api/login/userLogin/${email}/${pass}`);
    }
  },
  page: {
    signup: params => {
      return post("/api/user/signup", params);
    }
  },
  preperfil: {
    createUser: params => {
      return post(`/api/user/add`, params);
    },
    productoUpdate: params => {
      return put(`/api/user/productoUpdate`, params);
    },
    complementData: params => {
      return put(`/api/user/complementData`, params);
    },
    prospectoUpdate: params => {
      console.log("prospecto", params);
      return put(`/api/user/prospectoUpdate`, params);
    }
  },
  perfilamiento: {
    updateData: params => {
      return put(`/api/user/secondUpdate`, params);
    }
  }
};
