import { takeLatest, put } from 'redux-saga/effects';
import socActions from '../../redux/preperfilamiento/actions';
import containerActions from './actions';

export function* createUserSaga({ payload }) {
  yield put(socActions.createUser(payload));
}
export function* updateDataSaga({ payload }) {
  yield put(socActions.updateGeneralData(payload));
}
export function* productoUpdateSaga({ payload }) {
  yield put(socActions.productoUpdate(payload));
}
export function* prospectoUpdateSaga({ payload }) {
  yield put(socActions.prospectoUpdate(payload));
}

export default function* OrdersViewSaga() {
  yield takeLatest(containerActions.createUser.type, createUserSaga);
  yield takeLatest(containerActions.updateGeneralData.type, updateDataSaga);
  yield takeLatest(containerActions.productoUpdate.type, productoUpdateSaga);
  yield takeLatest(containerActions.prospectoUpdate.type, prospectoUpdateSaga);
}
