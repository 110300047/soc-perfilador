import actions from './actions';

const dispatcher = dispatch => ({
  createUser: params => dispatch(actions.createUser(params)),
  productoUpdate: params => dispatch(actions.productoUpdate(params)),
  updateGeneralData: params => dispatch(actions.updateGeneralData(params)),
  prospectoUpdate: params => dispatch(actions.prospectoUpdate(params)),
});

export default dispatcher;
