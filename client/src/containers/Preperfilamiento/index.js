import React, { Component } from "react";
import { Row, Progress, Steps, Col, Icon, Layout } from "antd";
import { connect } from "react-redux";
import { RowSignin } from "../Page/content.style";
import ContentHolder from "../../components/utility/contentHolder";
import FirstStep from "../../components/preperfilamiento/firststep";
import SecondStep from "../../components/preperfilamiento/secondstep";
import ThirdStep from "../../components/preperfilamiento/thirdstep";
import FourthStep from "../../components/preperfilamiento/fourthstep";
import ResultPreProfiling from "../../components/preperfilamiento/result";
import dispatch from "./dispacher";
import NavBar from "../../components/navbar";
import { containerSelector } from "./selector";
import { Staticstep, MainContent } from "./preperfilamiento.style";
import "./icomoon/style.css";
// import './style.css';
class PrePerfilamiento extends Component {
  constructor(props) {
    super(props);
    this.onChangeProduct = this.onChangeProduct.bind(this);
    this.onChangeEnganche = this.onChangeEnganche.bind(this);
    this.onChangeMontoMayorAprox = this.onChangeMontoMayorAprox.bind(this);
    this.nextStep = this.nextStep.bind(this);
    this.previousStep = this.previousStep.bind(this);
    this.switcherStep = this.switcherStep.bind(this);
    this.state = {
      step: 0,
      // primer paso
      inputProduct: 0,
      inputEnganche: 0,
      typeProduct: "",
      cumpleIngreso: "",
      tieneCo: "",
      ingresoAprox: 0,
      tipoCoacreditado: "",
      menSug: 0,
      // segundo paso
      email: "",
      password: "",
      cellPhone: "",
      name: "",
      firstSurname: "",
      secondSurname: "",
      accept: false,
      usercreated: false,
      // tercer paso

      // tercer paso Alta Hac
      altaHac: "",
      feAlta: "",

      // tercer paso coacreditado
      CoHiCre: "",
      CoEmFoPa: "",
      CoPaPuCre: "",
      CofeApCreBan: "",
      CoDeInCuBa: "",
      CobaPri: "",

      //

      HiCre: "",
      EmFoPa: "",
      PaPuCre: "",
      feApCreBan: "",
      DeInCuBa: "",
      baPri: "",

      causaMontoMayor: "",
      causaMontoText: "",
      frecuencia: "",
      montoMayorAprox: 0,

      // cuarto paso
      estadoCivil: "",
      regimen: "",
      nacionalidad: "",
      fechaNac: "",
      ConPaTi: "",
      // ExCuBaMe: '',
      desContact: "Si",
      ExFoMi: ""

      // newProject: {},
    };
  }

  onClickProduct = e => {
    const {
      target: { value }
    } = e;
    this.setState({
      typeProduct: value
    });
  };

  onChangeEmail = e => {
    const {
      target: { value }
    } = e;
    this.setState({
      email: value
    });
  };

  onChangePassword = e => {
    const {
      target: { value }
    } = e;
    this.setState({
      password: value
    });
  };

  onChangeCellPhone = ({ value }) => {
    this.setState({
      cellPhone: value
    });
  };

  onChangeName = e => {
    const {
      target: { value }
    } = e;
    this.setState({
      name: value
    });
  };

  onChangeFirstSurname = e => {
    const {
      target: { value }
    } = e;
    this.setState({
      firstSurname: value
    });
  };

  onChangeSecondSurname = e => {
    const {
      target: { value }
    } = e;
    this.setState({
      secondSurname: value
    });
  };

  onChangeAccept = e => {
    const {
      target: { checked }
    } = e;
    this.setState({
      accept: checked
    });
  };

  nextStep = () => {
    this.setState({
      step: this.state.step + 1
    });
  };

  previousStep = () => {
    this.setState({
      step: this.state.step - 1
    });
  };

  onChangeProduct = value => {
    this.setState({
      inputProduct: value
    });
  };

  onChangeEnganche = value => {
    this.setState({
      inputEnganche: value
    });
  };

  handleChangeTiCo = value => {
    this.setState({
      tipoCoacreditado: value
    });
  };
  //tercer paso
  onChangeCumpleIngreso = e => {
    this.setState(
      {
        cumpleIngreso: e.target.value
      }
      // function() {
      //   if (this.state.cumpleIngreso === 'Si') {
      //     this.setState({
      //       tieneCo: '',
      //     });
      //   }
      // }
    );
  };

  onChangeTieneCo = e => {
    this.setState({
      tieneCo: e.target.value
    });
  };

  onChangeIngresoAprox = value => {
    this.setState({
      ingresoAprox: value
    });
  };

  onChangeHiCre = e => {
    this.setState(
      {
        HiCre: e.target.value
      },
      function() {
        if (this.state.HiCre === "Si") {
          this.setState({
            EmFoPa: ""
          });
        }
      }
    );
  };
  onChangeEmFoPa = e => {
    this.setState({
      EmFoPa: e.target.value
    });
  };
  onChangePaPuCre = e => {
    this.setState(
      {
        PaPuCre: e.target.value
      },
      function() {
        if (this.state.PaPuCre === "Si") {
          this.setState({
            causaMontoMayor: "",
            causaMontoText: "",
            frecuencia: "",
            montoMayorAprox: ""
          });
        }
      }
    );
  };
  // handleChangeFeApCreBan = value => {
  //   this.setState({
  //     feApCreBan: value,
  //   });
  // };

  fechaApertura = fechaFull => {
    // console.log("me cago en dios"+ fechaFull);
    this.setState({
      feApCreBan: fechaFull
    });
    // this.state.feApCreBan = fechaFull;
    // console.log("fecha"+ this.state.feApCreBan);
  };
  onChangeDeInCuBa = e => {
    this.setState(
      {
        DeInCuBa: e.target.value
      },
      function() {
        if (this.state.DeInCuBa === "No") {
          this.setState({
            baPri: ""
          });
        }
      }
    );
  };
  handleChangeBaPri = value => {
    this.setState({
      baPri: value
    });
  };
  //tercer paso intermedio
  onChangeCausaText = e => {
    const {
      target: { value }
    } = e;
    this.setState({
      causaMontoText: value
    });
  };
  handleChangeCausaMontoMayor = value => {
    this.setState(
      {
        causaMontoMayor: value
      },
      function() {
        if (this.state.causaMontoMayor !== "Otro") {
          this.setState({
            causaMontoText: ""
          });
        }
      }
    );
  };
  handleChangeFrecuencia = value => {
    this.setState({
      frecuencia: value
    });
  };
  onChangeMontoMayorAprox = value => {
    if (value >= 4000) {
      alert("No es sujeto de credito, contancte un asesor");
    }
    this.setState({
      montoMayorAprox: value
    });
  };
  // Tercer paso comprobacion pogo

  onChangeAltaHac = e => {
    this.setState({
      AltaHac: e.target.value
    });
  };

  fechaAltaHac = fechaFull => {
    // console.log("me cago en dios"+ fechaFull);
    this.setState({
      feAlta: fechaFull
    });
    // this.state.feApCreBan = fechaFull;
    // console.log("fecha"+ this.state.fechaNac);
  };

  //cuarta paso
  handleChangeEdoCiv = value => {
    this.setState(
      {
        estadoCivil: value
      },
      function() {
        if (
          this.state.estadoCivil === "Soltero(a)" ||
          this.state.estadoCivil === "Union libre"
        ) {
          this.setState({
            regimen: "Separados"
          });
        }
        console.log(this.state.regimen);
      }
    );
  };
  handleChangeReg = value => {
    this.setState(
      {
        regimen: value
      },
      function() {
        if (this.state.regimen !== "Mancomunado") {
          this.setState({
            ConPaTi: ""
          });
        }
      }
    );
  };
  handleChangeNac = value => {
    this.setState(
      {
        nacionalidad: value
      },
      function() {
        if (this.state.nacionalidad !== "Extranjero(a)") {
          this.setState({
            // ExCuBaMe: '',
            ExFoMi: ""
            // ExHiCre: '',
          });
        }
      }
    );
  };

  // handleChangeDate = (date, dateString) => {
  //   this.setState({
  //     fechaNac: dateString,
  //   });
  // };

  fechaNacimiento = fechaFull => {
    // console.log("me cago en dios"+ fechaFull);
    this.setState({
      fechaNac: fechaFull
    });
    // this.state.feApCreBan = fechaFull;
    // console.log("fecha"+ this.state.fechaNac);
  };
  onChangeConPaTi = e => {
    this.setState({
      ConPaTi: e.target.value
    });
  };
  onChangeDesContact = e => {
    this.setState({
      desContact: e.target.value
    });
  };
  onChangeExFoMi = e => {
    this.setState({
      ExFoMi: e.target.value
    });
  };
  // onChangeExHiCre = e => {
  //   this.setState({
  //     ExHiCre: e.target.value,
  //   });
  // };

  onCreateGeneralData = () => {
    const {
      email, //primeros datos
      password,
      cellPhone,
      name,
      firstSurname,
      secondSurname,
      inputProduct,
      inputEnganche,
      typeProduct,
      cumpleIngreso,
      tieneCo,
      tipoCoacreditado,
      ingresoAprox,
      //segundos datos
      estadoCivil,
      regimen,
      nacionalidad,
      fechaNac,
      desContact, // Para saber si desea ser contactado,
      ExFoMi,
      // agregados para arbol
      altaHac,
      feAlta,
      HiCre,
      EmFoPa,
      PaPuCre,
      feApCreBan,
      DeInCuBa,
      baPri,
      CoHiCre,
      CoEmFoPa,
      CoPaPuCre,
      CofeApCreBan,
      CoDeInCuBa,
      CobaPri,
      causaMontoMayor,
      causaMontoText,
      frecuencia,
      montoMayorAprox
    } = this.state;
    const completeName = `${name} ${firstSurname} ${secondSurname}`;
    const { updateGeneralData } = this.props;
    const data = {
      //primeros datos
      email,
      password,
      cellPhone,
      completeName,
      inputProduct,
      inputEnganche,
      typeProduct,
      cumpleIngreso,
      tieneCo,
      tipoCoacreditado,
      ingresoAprox,
      //segundos datos
      estadoCivil,
      regimen,
      nacionalidad,
      fechaNac,
      desContact, // Para saber si desea ser contactado
      ExFoMi,
      password,
      inputProduct,
      inputEnganche,
      typeProduct,
      // agregados para arbol
      altaHac,
      feAlta,
      HiCre,
      EmFoPa,
      PaPuCre,
      feApCreBan,
      DeInCuBa,
      baPri,
      CoHiCre,
      CoEmFoPa,
      CoPaPuCre,
      CofeApCreBan,
      CoDeInCuBa,
      CobaPri,
      causaMontoMayor,
      causaMontoText,
      frecuencia,
      montoMayorAprox
    };
    updateGeneralData(data);
    this.nextStep();
  };

  prospect = () => {
    const { email } = this.state;
    const { prospectoUpdate } = this.props;
    const data = { email };
    prospectoUpdate(data);
  };

  switcherStep = () => {
    const { email } = this.state;
    switch (this.state.step) {
      case 0:
        return (
          <FirstStep
            nextStep={this.nextStep}
            onChangeProduct={this.onChangeProduct}
            onChangeEnganche={this.onChangeEnganche}
            onClickProduct={this.onClickProduct}
            onChangeCumpleIngreso={this.onChangeCumpleIngreso}
            onChangeTieneCo={this.onChangeTieneCo}
            onChangeIngresoAprox={this.onChangeIngresoAprox}
            handleChangeTiCo={this.handleChangeTiCo}
            {...this.state}
          />
        );
      case 1:
        return (
          <SecondStep
            onChangeEmail={this.onChangeEmail}
            onChangePassword={this.onChangePassword}
            onChangeCellPhone={this.onChangeCellPhone}
            onChangeName={this.onChangeName}
            onChangeFirstSurname={this.onChangeFirstSurname}
            onChangeSecondSurname={this.onChangeSecondSurname}
            onChangeAccept={this.onChangeAccept}
            nextStep={this.nextStep}
            previousStep={this.previousStep}
            {...this.state}
          />
        );
      case 2:
        return (
          <ThirdStep
            // onChangeCumpleIngreso={this.onChangeCumpleIngreso}
            // onChangeTieneCo={this.onChangeTieneCo}
            // onChangeIngresoAprox={this.onChangeIngresoAprox}
            onChangeHiCre={this.onChangeHiCre}
            onChangeEmFoPa={this.onChangeEmFoPa}
            onChangePaPuCre={this.onChangePaPuCre}
            // handleChangeFeApCreBan={this.handleChangeFeApCreBan}
            onChangeDeInCuBa={this.onChangeDeInCuBa}
            handleChangeBaPri={this.handleChangeBaPri}
            nextStep={this.nextStep}
            previousStep={this.previousStep}
            onChangeCausaText={this.onChangeCausaText}
            handleChangeCausaMontoMayor={this.handleChangeCausaMontoMayor}
            handleChangeFrecuencia={this.handleChangeFrecuencia}
            onChangeMontoMayorAprox={this.onChangeMontoMayorAprox}
            fechaApertura={this.fechaApertura}
            //alta hacienda
            onChangeAltaHac={this.onChangeAltaHac}
            fechaAltaHac={this.fechaAltaHac}
            //Coacreditado
            handleChangeCoBaPri={this.handleChangeCoBaPri}
            onChangeCoDeInCuBa={this.onChangeCoDeInCuBa}
            CofechaApertura={this.CofechaApertura}
            onChangeCoHiCre={this.onChangeCoHiCre}
            onChangeCoPaPuCre={this.onChangeCoPaPuCre}
            onChangeCoEmFoPa={this.onChangeCoEmFoPa}
            {...this.state}
          />
        );
      case 3:
        return (
          <FourthStep
            handleChangeEdoCiv={this.handleChangeEdoCiv}
            handleChangeReg={this.handleChangeReg}
            handleChangeNac={this.handleChangeNac}
            // handleChangeDate={this.handleChangeDate}
            onChangeConPaTi={this.onChangeConPaTi}
            // onChangeExCuBaMe={this.onChangeExCuBaMe}
            onChangeExFoMi={this.onChangeExFoMi}
            // onChangeExHiCre={this.onChangeExHiCre}
            nextStep={this.nextStep}
            previousStep={this.previousStep}
            onCreateGeneralData={this.onCreateGeneralData}
            prospect={this.prospect}
            fechaNacimiento={this.fechaNacimiento}
            {...this.state}
          />
        );
      case 4:
        return (
          <ResultPreProfiling
            email={email}
            previousStep={this.previousStep}
            onChangeDesContact={this.onChangeDesContact}
            desContact={this.state.desContact}
          />
        );
      default:
        return <div>Error</div>;
    }
  };

  render() {
    const { step } = this.state;
    const Step = Steps.Step;
    const { Header, Footer, Sider, Content } = Layout;

    this.state.menSug =
      24000 * ((this.state.inputProduct - this.state.inputEnganche) / 1000000);
    return (
      <div>
        <RowSignin>
          <NavBar />
        </RowSignin>
        <MainContent>
          <Row type="flex" style={{ justifyContent: "left" }} align="top">
            <Col
              xs={24}
              sm={12}
              md={{ span: 24 }}
              lg={{ offset: 1, span: 6 }}
              xl={4}
            >
              <ContentHolder>
                <Staticstep>
                  <Progress percent={(step / 4) * 100} status="active" />
                  <Steps direction="vertical" current={step}>
                    <Step
                      title="Crédito"
                      icon={<Icon type="home" />}
                      description="Qué financiamiento necesitas?"
                    />
                    <Step
                      title="Usuario"
                      icon={<Icon type="user" />}
                      description="Háblanos de Ti.."
                    />
                    <Step
                      title="Complementos"
                      icon={<Icon type="snippets" />}
                      description="Comprobación de datos."
                    />
                    <Step title="Resultado" icon={<Icon type="star" />} />
                  </Steps>
                </Staticstep>
              </ContentHolder>
            </Col>
            <Col
              xs={24}
              sm={24}
              md={{ span: 24 }}
              lg={{ offset: 2, span: 14 }}
              xl={12}
            >
              <ContentHolder className="main-container-content">
                {this.switcherStep()}
              </ContentHolder>
            </Col>
          </Row>
        </MainContent>
      </div>
    );
  }
}

export default connect(
  containerSelector,
  dispatch
)(PrePerfilamiento);
