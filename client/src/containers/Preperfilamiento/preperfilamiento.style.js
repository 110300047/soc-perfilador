import styled from 'styled-components';
import { Icon, Radio, Layout } from 'antd';

const stateColor = {
  positivo: 'green',
  negativo: 'red',
  medio: 'orange',
  apagado: 'grey',
};
export const Circle = styled.div`
  background: ${({ status }) => stateColor[status]};
  width: 50px;
  height: 50px;
  margin: 10px 0px;
  -webkit-border-radius: 25px;
  -moz-border-radius: 25px;
  border-radius: 25px;
`;

export const StatusProfiling = styled.span`
  color: ${({ status }) => stateColor[status]};
  font-weight: bold;
`;

export const IconCheck = styled(Icon)`
  color: White;
  font-size: 32px;
  vertical-align: middle;
  text-align: center;
  left: 20%;
  position: relative;
  display: table-cell;
  top: 20%;
`;

export const TxtAling = styled.div`
  text-align: center;
  margin: 20px 0px;
`;
export const AlingRAdioButton = styled(TxtAling)`
  width: 100%;
`;

export const ContenForm = styled.div`
  margin-bottom: 10px;
`;

export const RadioButton = styled(Radio.Button)`
  width: 100%;
  border-color: #265b8e !important;
  background: #265b8e!important  ;
`;
export const RadioGroup = styled(Radio.Group)`
  width: 100% !important;
`;
export const Staticstep = styled.div`  
  left: 50px;  
  @media (min-width: 992px) { 
    position: fixed; 
    padding-top: 40px;
  }
`;
export const MainContent = styled.div`
  padding :0 16px;
  


  @media (min-width: 992px){
    padding :0 0px;
  }
`;
