import { createActions } from 'ractionx';

const prefix = '@soc/firstStage/preperfil';

export const types = [
  'CREATE_USER',
  'UPDATE_GENERAL_DATA',
  'PRODUCTO_UPDATE',
  'PROSPECTO_UPDATE',
];

const {
  createUser,
  updateGeneralData,
  productoUpdate,
  prospectoUpdate,
} = createActions(prefix, types);


export default {
  createUser,
  updateGeneralData,
  productoUpdate,
  prospectoUpdate,
};
