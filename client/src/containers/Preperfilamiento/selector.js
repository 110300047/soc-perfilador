export const userStatusSelector = ({ preperfilRedux }) =>
  preperfilRedux.userStatus;
export const containerSelector = ({ preperfilRedux }) => {
  const { userStatus } = preperfilRedux;
  return { userStatus };
};
export default {
  userStatusSelector,
  containerSelector,
};
