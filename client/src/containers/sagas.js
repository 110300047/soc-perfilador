import { all } from "redux-saga/effects";
import prePerfilContainer from "./Preperfilamiento/sagas";
import perfilContainer from "./Perfilamiento/sagas";
import pagesContainer from "./Page/sagas";

export default function* rootSaga() {
  yield all([prePerfilContainer(), perfilContainer(), pagesContainer()]);
}
