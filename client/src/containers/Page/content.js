import React from 'react';
import NavBar from "../../components/navbar";
import { Row, Col } from "antd";
import ButtonTest from '../../components/content/buttonTest';
import Afirme from '../../image/Afirme.png';
import Banorte from '../../image/Banorte.svg';
import Banregio from '../../image/Banregio.png';
import BBVA from '../../image/BBVA.png';
import Citibanamex from '../../image/Citibanamex.png';
import HSBC from '../../image/HSBC.png';
import Santander from '../../image/Santander.png';
import Scotiabank from '../../image/Scotiabank.png';
import ImgAsesor from '../../image/asesor.jpg';
import ImgCotiza from '../../image/cotiza.jpg';
import ImgMejora from '../../image/mejora.jpg';

import { Header, ContainerStyle, Banner } from './content.style';

const banks = {
  afirme: {
    logo: Afirme,
    height: 40,
  },
  banorte: {
    logo: Banorte,
    height: 20,
  },
  banregio: {
    logo: Banregio,
    height: 30,
  },
  bbva: {
    logo: BBVA,
    height: 30,
  },
  citibanamex: {
    logo: Citibanamex,
    height: 35,
  },
  hsbc: {
    logo: HSBC,
    height: 35,
  },
  santander: {
    logo: Santander,
    height: 30,
  },
  scotiabank: {
    logo: Scotiabank,
    height: 30,
  },
};

const BanksBanner = () =>
  Object.keys(banks).map((item, i) => (
    <img
      key={i}
      src={`${banks[item].logo}`}
      height={banks[item].height}
      style={{ margin: 10 }}
    />
  ));

const Content = () => (
  <div>
    <NavBar />
    <ContainerStyle>          
      <Header>      
        <ButtonTest />
      </Header>
      <Row gutter={48} className="content-format">
        <Col xs={24}>
          <h2 className="titulo2 text-center">Lorem ipsum</h2>
          <hr class="plecaAzul"></hr>    
        </Col>
        <Col xs={24} md={8}>
          <Row>                
            <Col xs={24} className="text-justify">
              <h2 className="titulo2 text-center">Lorem ipsum</h2>
              <hr class="plecaAzul"></hr>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor 
              in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </Col>
            <Col xs={24}>
              <img src={ImgAsesor} className="img-1"/>
            </Col> 
          </Row>
        </Col>
        <Col xs={24} md={8}>
          <Row>            
            <Col xs={24} className="text-justify">
              <h2 className="titulo2 text-center">Lorem ipsum</h2>
              <hr class="plecaAzul"></hr>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor 
              in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </Col>
            <Col xs={24}>
              <img src={ImgCotiza} className="img-1"/>
            </Col>
          </Row>          
        </Col>
        <Col xs={24} md={8}>
          <Row>            
            <Col xs={24} className="text-justify">
              <h2 className="titulo2 text-center">Lorem ipsum</h2>
              <hr class="plecaAzul"></hr>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor 
              in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </Col>
            <Col xs={24}>
              <img src={ImgMejora} className="img-1"/>
            </Col>
          </Row>          
        </Col>        
      </Row>
      <h2 className="titulo2 text-center">Nuestros socios</h2>
      <hr class="plecaAzul"></hr>
      <Banner>{BanksBanner()}</Banner>
    </ContainerStyle>

  </div>
  
);

export default Content;
