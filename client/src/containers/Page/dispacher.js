import actions from "./actions";

const dispatcher = dispatch => ({
  signUp: params => {
    dispatch(actions.signUp(params));
  }
});

export default dispatcher;
