import React, { Component } from "react";
import { connect } from "react-redux";
import { RowSignin } from "./content.style";
import { Modal } from "antd";
import { Link } from "react-router-dom";
import SignInStyleWrapper from './signin.style';
import NavBar from "../../components/navbar";
import SignUpForm from "../../components/signup";
import dispatch from "./dispacher";

class SignUp extends Component {
  state = {
    confirmDirty: false,
    redirectToReferrer: false,
    name: "",
    firstSurname: "",
    secondSurname: "",
    email: "",
    phone: "",
    constructure: ""
  };
  componentDidMount = () => {
    this.info();
  };

  createAcount = () => {
    const { signUp } = this.props;
    const {
      name,
      firstSurname,
      secondSurname,
      email,
      phone,
      constructure
    } = this.state;
    const completeName = `${name} ${firstSurname} ${secondSurname}`;
    const data = {
      email,
      phone,
      constructure,
      completeName
    };
    signUp(data);
  };

  onChangeName = e => {
    const {
      target: { value }
    } = e;
    this.setState({
      name: value
    });
  };

  onChangeFirstSurname = e => {
    const {
      target: { value }
    } = e;
    this.setState({
      firstSurname: value
    });
  };

  onChangeSecondSurname = e => {
    const {
      target: { value }
    } = e;
    this.setState({
      secondSurname: value
    });
  };

  onChangeEmail = e => {
    const {
      target: { value }
    } = e;
    this.setState({
      email: value
    });
  };

  onChangePassword = e => {
    const {
      target: { value }
    } = e;
    this.setState({
      password: value
    });
  };

  onChangeCellPhone = ({ value }) => {
    this.setState({
      cellPhone: value
    });
  };

  info() {
    Modal.warning({
      title: "Crear cuenta SOC",
      content: (
        <div>
          <p>
            Crear una cuenta SOC es dedicada a agentes. Si usted es una persona
            que quiere solicitar algún tipo de crédito, es necesario que primero
            realice la prueba de perfilamiento que se encuentra en el siguiente
            link.
          </p>
          <br />
          <a href="/simulador">Haz tu prueba</a>
        </div>
      ),
      onOk() {}
    });
  }

  render() {
    let body = document.getElementById("mainBody");
    body.classList.add("bg-azul");
    return (      
      <div>
        <RowSignin>
          <NavBar />
        </RowSignin>
        <SignInStyleWrapper >
          <SignUpForm
            createAcount={this.createAcount}
            onChangeName={this.onChangeName}
            onChangeFirstSurname={this.onChangeFirstSurname}
            onChangeSecondSurname={this.onChangeSecondSurname}
            onChangeEmail={this.onChangeEmail}
            onChangePassword={this.onChangePassword}
            onChangeCellPhone={this.onChangeCellPhone}
          />
        </SignInStyleWrapper>
      </div>
    );
  }
}

export default connect(
  null,
  dispatch
)(SignUp);
