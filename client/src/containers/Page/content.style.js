import styled from 'styled-components';
import BackgroundImg from '../../image/SOC1.jpg';
import { Row } from 'antd';

export const Header = styled.header`
  display: block;
  background-image: -webkit-linear-gradient(
      rgba(0, 0, 0, 0.3),
      rgba(0, 0, 0, 0.3)
    ),
    url(${BackgroundImg});
  background-image: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)),
    url(${BackgroundImg});
  background-size: cover;
  background-position: center;
  height: 90vh;
  background-attachment: fixed;
`;
export const Banner = styled.footer`
  display: block;
  
  height: 4em;
  lign-items: center;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const ContainerStyle = styled.div`
  background-color: #fff;
  color: #555;
  font-family: 'Lato', 'Arial', sans-serif;
  font-weight: 300;
  font-size: 20px;
  text-rendering: optimizeLegibility;
  overflow-x: hidden;
`;

export const RowSignin = styled(Row)`
  color: black;
  padding-bottom: 0px;
  height: 100px;
  font-family: 'Lato', 'Arial', sans-serif;
  font-weight: 300;
  font-size: 20px;
  text-rendering: optimizeLegibility;
  @media (min-width: 992px){
    // padding-bottom: 50px;
  }
`;
export const LogoStyle = styled.img`
  display: inline-block;
  padding: 19px 20px;
  float: none;

  @media only screen and (max-width: 470px) {
    height: 80px;
  }
  @media only screen and (max-width: 250px) {
    margin-top: 0px;
    height: 40px;
  }
`;
