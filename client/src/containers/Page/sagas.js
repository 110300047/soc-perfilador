import { takeLatest, put } from "redux-saga/effects";
import socActions from "../../redux/pages/actions";
import containerActions from "./actions";

export function* signUpSaga({ payload }) {
  yield put(socActions.signUp(payload));
}

export default function* OrdersViewSaga() {
  yield takeLatest(containerActions.signUp.type, signUpSaga);
}
