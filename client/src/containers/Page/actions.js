import { createActions } from "ractionx";

const prefix = "@soc/page/perfil";

export const types = ["SIGN_UP"];

const { signUp } = createActions(prefix, types);

export default { signUp };
