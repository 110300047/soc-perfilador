import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Input from '../../components/uielements/input';
import Checkbox from '../../components/uielements/checkbox';
import Button from '../../components/uielements/button';
import authAction from '../../redux/auth/actions';
// import appAction from '../../redux/app/actions';
// import Auth0 from '../../helpers/auth0';
// import Firebase from '../../helpers/firebase';
// import FirebaseLogin from '../../components/firebase';
import IntlMessages from '../../components/utility/intlMessages';
import SignInStyleWrapper from './signin.style';
import { RowSignin, LogoStyle } from './content.style';
import NavBar from '../../components/navbar';
import LogoSoc from '../../image/logo_soc.svg';
const { login } = authAction;

class SignIn extends Component {
  state = {
    redirectToReferrer: false,
    username: '',
    password: '',
  };
  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }

  onUsernameChange = ev => {
    this.setState({ username: ev.target.value });
  };

  onPasswordChange = ev => {
    this.setState({ password: ev.target.value });
  };

  handleLogin = () => {
    const { login } = this.props;
    const { username: email, password: pass } = this.state;

    login({ email, pass });
  };

  render() {
    const from = { pathname: '/dashboard' };
    const { redirectToReferrer } = this.state;    
    
    let body = document.getElementById("mainBody");
    body.classList.add("bg-azul");

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
    return (
      <div>
        <RowSignin>
          <NavBar />
        </RowSignin>                  
        <SignInStyleWrapper className="isoSignInPage align-flex-xy-center">
          <div className="isoLoginContentWrapper" >            
            <div className="isoLoginContent">
              <div className="logoContainerSig">
                <LogoStyle src={`${LogoSoc}`} ></LogoStyle>
              </div>
              <div className="isoSignInForm">
                <div className="isoInputWrapper">                  
                  <Input
                    size="large"
                    placeholder="Nombre"
                    onPressEnter={this.handleLogin}
                    onChange={this.onUsernameChange}
                  />
                </div>

                <div className="isoInputWrapper">
                  <Input
                    size="large"
                    type="password"
                    placeholder="Contraseña"
                    onPressEnter={this.handleLogin}
                    onChange={this.onPasswordChange}
                  />
                </div>

                <div className="isoInputWrapper isoLeftRightComponent">
                  <Checkbox>
                    <IntlMessages id="page.signInRememberMe" />
                  </Checkbox>
                  <Button type="primary" className="green-btn" onClick={this.handleLogin}>
                    Iniciar sesión
                  </Button>
                </div>
                <div className="isoCenterComponent isoHelperWrapper">
                  <Link to="/forgotpassword" className="isoForgotPass">
                    ¿Olvidaste la contraseña?
                  </Link>
                  <Link to="/signup">Crear cuenta SOC</Link>
                </div>
              </div>
            </div>
          </div>
        </SignInStyleWrapper>
      </div>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.get('idToken') !== null,
    loading: state.Auth.get('loading'),
  }),
  { login }
)(SignIn);
