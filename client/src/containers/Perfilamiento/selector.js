export const userSelector = ({ Auth }) => Auth.user;
export const containerSelector = ({ Auth }) => {
  const { user } = Auth;
  return { user };
};
export default {
  userSelector,
  containerSelector,
};
