import React, { Component } from 'react';
import LayoutContentWrapper from '../../components/utility/layoutWrapper.js';
import LayoutContent from '../../components/utility/layoutContent';
import { Row, Progress, Steps, Col } from 'antd';
import ContentHolder from '../../components/utility/contentHolder';
import FirstStep from './firststep';
import SecondStep from './secondstep';
import ResultProfiling from './result';
import { connect } from 'react-redux';
import dispatch from './dispacher';
import { containerSelector } from './selector';

class Perfilamiento extends Component {
  constructor(props) {
    super(props);
    this.nextStep = this.nextStep.bind(this);
    this.previousStep = this.previousStep.bind(this);
    this.switcherStep = this.switcherStep.bind(this);
    this.state = {
      step: 0,
      keys: 1,
      //Primer paso General
      ocupacion: '',
      antiguedad: '',
      //Primer paso Nomina
      COFI: '',
      aux2: '',
      numeroSeguro: '',
      baNomina: '',
      tipoPagadora: '',
      //Primer paso
      //Primer paso
      fechaSat: '',
      CoSiFis: '',
      baDepos: '',
      actividadLab: '',
      //Primer paso Accionista
      fechaAlta: '',
      actividadEmpresa: '',
      altaHaciendaAct: '',
      //Segundo paso
      tipoCoacreditado: '',
      acreBase: 0,
      acreComisiones: 0,
      acreBonos: 0,
      acreTotal: 0,
      coBase: 0,
      coComisiones: 0,
      coBonos: 0,
      coTotal: 0,

      egresoTar: {},
      egresoAuto: {},
      egresoPer: {},
      egresoHip: {},





      //Resultados
      ingresosTotales: 0,
      gastosTotales: 0,
      capMenMaxima: 0,
      mensualidadHipoteca: 0,
      creditoMaximo: 0,
      // egresosPrincipales: {},
      egresosTotales: 0,
    };
  }

  nextStep = () => {
    this.setState({
      step: this.state.step + 1,
    });
  };

  previousStep = () => {
    this.setState({
      step: this.state.step - 1,
    });
  };

  onChangeCOFI = e => {
    this.setState(
      {
        COFI: e.target.value,
      },
      function() {
        if (this.state.COFI == '22') {
          this.setState({
            numeroSeguro: '',
            aux2: '',
          });
          // console.log(this.state.numeroSeguro)
        }
      }
    );
  };

  onChangeNumeroSeg = e => {
    const {
      target: { value },
    } = e;
    this.setState(
      {
        aux2: value,
      },
      function() {
        const hasCat = /\d{10}-\d/;
        if (hasCat.test(this.state.aux2)) {
          this.setState({
            numeroSeguro: this.state.aux,
          });
        }
      }
    );
  };

  onChangeOcupacion = e => {
    const {
      target: { value },
    } = e;
    this.setState({
      ocupacion: value,
    });
  };

  fechaAntiguedad = fechaFull => {
    console.log("antiguedad: "+ fechaFull);
    this.setState({
      antiguedad: fechaFull,
    });

  };

  handleChangeBaNom = value => {
    this.setState({
      baNomina: value,
    });
  };
  handleChangeTipoPag = value => {
    this.setState({
      tipoPagadora: value,
    });
  };

  // handleChangeDateSat = (date, dateString) => {
  //   console.log(dateString);
  //   this.setState({
  //     fechaSat: dateString,
  //   });
  // };

  fechaSatF = fechaFull => {
    console.log("fechaSat: "+ fechaFull);
    this.setState({
      fechaSat: fechaFull,
    });

  };
  onChangeCSF = e => {
    const {
      target: { value },
    } = e;
    this.setState({
      CoSiFis: value,
    });
  };

  handleChangeBaDep = value => {
    this.setState({
      baDepos: value,
    });
  };
  handleChangeActividad = value => {
    this.setState({
      actividadLab: value,
    });
  };


  fechaAltaF = fechaFull => {
    console.log("fechaAlta: "+ fechaFull);
    this.setState({
      fechaAlta: fechaFull,
    });

  };

  handleChangeActEmp = value => {
    this.setState({
      actividadEmpresa: value,
    });
  };
  onChangeAHAct = e => {
    const {
      target: { value },
    } = e;
    this.setState({
      altaHaciendaAct: value,
    });
  };
  // segundo paso
  onChangeAcreBase = value => {
    this.setState(
      {
        acreBase: value,
      },
      function() {
        this.setState({
          acreTotal:
            this.state.acreBase +
            this.state.acreComisiones +
            this.state.acreBonos,
        });
      }
    );
  };

  handleChangeTiCo = value => {
    this.setState(
      {
        tipoCoacreditado: value,
      },
      function() {
        if (this.state.tipoCoacreditado === 'Ninguno') {
          this.setState({
            coBase: 0,
            coComisiones: 0,
            coBonos: 0,
            coTotal: 0,
          });
          // console.log(this.state.numeroSeguro)
        }
      }
    );
  };

  onChangeAcreComi = value => {
    this.setState(
      {
        acreComisiones: value,
      },
      function() {
        this.setState({
          acreTotal:
            this.state.acreBase +
            this.state.acreComisiones +
            this.state.acreBonos,
        });
      }
    );
  };
  onChangeAcreBono = value => {
    this.setState(
      {
        acreBonos: value,
      },
      function() {
        this.setState({
          acreTotal:
            this.state.acreBase +
            this.state.acreComisiones +
            this.state.acreBonos,
        });
      }
    );
  };
  // onChangeAcreTotal = value => {
  //   this.setState({
  //     acreTotal: value,
  //   });
  // };
  onChangeCoBase = value => {
    this.setState(
      {
        coBase: value,
      },
      function() {
        this.setState({
          coTotal:
            this.state.coBase + this.state.coComisiones + this.state.coBonos,
        });
      }
    );
  };
  onChangeCoComi = value => {
    this.setState(
      {
        coComisiones: value,
      },
      function() {
        this.setState({
          coTotal:
            this.state.coBase + this.state.coComisiones + this.state.coBonos,
        });
      }
    );
  };
  onChangeCoBono = value => {
    this.setState(
      {
        coBonos: value,
      },
      function() {
        this.setState({
          coTotal:
            this.state.coBase + this.state.coComisiones + this.state.coBonos,
        });
      }
    );
  };
  // onChangeCoTotal = value => {
  //   this.setState({
  //     coTotal: value,
  //   });
  // };


  onCreateGeneralData = () => {
    const { user: email } = this.props;

    const {
      ocupacion,
      antiguedad,
      COFI,
      numeroSeguro,
      baNomina,
      tipoPagadora,
      fechaSat,
      CoSiFis,
      baDepos,
      actividadLab,
      fechaAlta,
      actividadEmpresa,
      altaHaciendaAct,
      acreBase,
      acreComisiones,
      acreBonos,
      acreTotal,
      coBase,
      coComisiones,
      coBonos,
      coTotal,
      // egresosPrincipales, Esperar a que funcione bien
      egresosTotales,
      egresoTar,
      egresoAuto,
      egresoPer,
      egresoHip,



    } = this.state;

    const data = {
      email,
      ocupacion,
      antiguedad,
      COFI,
      numeroSeguro,
      baNomina,
      tipoPagadora,
      fechaSat,
      CoSiFis,
      baDepos,
      actividadLab,
      fechaAlta,
      actividadEmpresa,
      altaHaciendaAct,
      acreBase,
      acreComisiones,
      acreBonos,
      acreTotal,
      coBase,
      coComisiones,
      coBonos,
      coTotal,
      // egresosPrincipales, Esperar a que funcione bien
      egresosTotales,
      egresoTar,
      egresoAuto,
      egresoPer,
      egresoHip,


    };
    const { updateGeneralBalance } = this.props;
    console.log(data);
    updateGeneralBalance(data);
  };

  makePostData = () => {
    this.onCreateGeneralData();
  };

  // EgresosObject = Obj => {
  //   this.state.egresosPrincipales = Obj;
  //   console.log('Objeto: ', Obj);
  //   console.log('Egresos: ', this.state.egresosPrincipales);
  //   let K = Obj.keys,
  //     Pagos = Obj.pagosMin,
  //     Saldos = Obj.saldo,
  //     countS = 0,
  //     countP = 0;
  //   for (let x = 0; x < K.length; x++) {
  //     countS = countS + Saldos[K[x]];
  //     countP = countP + Pagos[K[x]];
  //     // console.log('EgresosTotales: ', count);
  //   }
  //   this.state.egresosTotales = countS + countP;
  //   console.log('EgresosTotales: ', this.state.egresosTotales);
  // };

  saveEgresoTar = (Banco, Saldo, Pagomin, Fecha) => {
    this.setState(
      {
        egresoTar: {
          BancoPrincipal: Banco,
          FechaCreditoAntiguo: Fecha, 
          SaldoActual: Saldo,
          Pagomin: Pagomin,
          EgresoTarTotal: Saldo + Pagomin,
          // value5: this.props.baNomina,
                  
          // fechaNac: this.state.fechaNacM + '/' + this.state.fechaNacA,
        },
      },
      function() {
        console.log(this.state.egresoTar);
      }
    );
  };

  saveEgresoAuto = (Banco, Saldo, Pagomin, Fecha) => {
    this.setState(
      {
        egresoAuto: {
          BancoPrincipal: Banco,
          FechaCreditoAntiguo: Fecha, 
          SaldoActual: Saldo,
          Pagomin: Pagomin,
          EgresoTarTotal: Saldo + Pagomin,
          // value5: this.props.baNomina,
                  
          // fechaNac: this.state.fechaNacM + '/' + this.state.fechaNacA,
        },
      },
      function() {
        console.log(this.state.egresoAuto);
      }
    );
  };

  saveEgresoPer = (Banco, Saldo, Pagomin, Fecha) => {
    this.setState(
      {
        egresoPer: {
          BancoPrincipal: Banco,
          FechaCreditoAntiguo: Fecha, 
          SaldoActual: Saldo,
          Pagomin: Pagomin,
          EgresoTarTotal: Saldo + Pagomin,
          // value5: this.props.baNomina,
                  
          // fechaNac: this.state.fechaNacM + '/' + this.state.fechaNacA,
        },
      },
      function() {
        console.log(this.state.egresoPer);
      }
    );
  };

  saveEgresoHip = (Banco, Saldo, Pagomin, Fecha) => {
    this.setState(
      {
        egresoHip: {
          BancoPrincipal: Banco,
          FechaCreditoAntiguo: Fecha, 
          SaldoActual: Saldo,
          Pagomin: Pagomin,
          EgresoTarTotal: Saldo + Pagomin,
          // value5: this.props.baNomina,
                  
          // fechaNac: this.state.fechaNacM + '/' + this.state.fechaNacA,
        },
      },
      function() {
        console.log(this.state.egresoHip);
      }
    );
  };



  switcherStep = () => {
    const {
      ocupacion,
      antiguedad,
      COFI,
      numeroSeguro,
      baNomina,
      tipoPagadora,
      fechaSat,
      CoSiFis,
      baDepos,
      actividadLab,
      fechaAlta,
      actividadEmpresa,
      altaHaciendaAct,
      tipoCoacreditado,
      acreBase,
      acreComisiones,
      acreBonos,
      acreTotal,
      coBase,
      coComisiones,
      coBonos,
      coTotal,

      ingresosTotales,
      aux2,
      egresosTotales,
      // egresosPrincipales, // quitar
      egresoTar,
      egresoAuto,
      egresoPer,
      egresoHip,
    } = this.state;
    switch (this.state.step) {
      case 0:
        return (
          <FirstStep
            nextStep={this.nextStep}
            onChangeCOFI={this.onChangeCOFI}
            onChangeOcupacion={this.onChangeOcupacion}
            // onChangeDateAntiguedad={this.onChangeDateAntiguedad}
            fechaAntiguedad={this.fechaAntiguedad}
            onChangeNumeroSeg={this.onChangeNumeroSeg}
            handleChangeBaNom={this.handleChangeBaNom}
            handleChangeTipoPag={this.handleChangeTipoPag}
            // handleChangeDateSat={this.handleChangeDateSat}
            fechaSatF={this.fechaSatF}
            onChangeCSF={this.onChangeCSF}
            handleChangeBaDep={this.handleChangeBaDep}
            handleChangeActividad={this.handleChangeActividad}
            // handleChangeDateAlt={this.handleChangeDateAlt}
            fechaAltaF={this.fechaAltaF}
            handleChangeActEmp={this.handleChangeActEmp}
            onChangeAHAct={this.onChangeAHAct}
            ocupacion={ocupacion}
            antiguedad={antiguedad}
            COFI={COFI}
            numeroSeguro={numeroSeguro}
            baNomina={baNomina}
            tipoPagadora={tipoPagadora}
            fechaSat={fechaSat}
            CoSiFis={CoSiFis}
            baDepos={baDepos}
            actividadLab={actividadLab}
            fechaAlta={fechaAlta}
            actividadEmpresa={actividadEmpresa}
            altaHaciendaAct={altaHaciendaAct}
            aux2={aux2}
          />
        );
      case 1:
        return (
          <SecondStep
            nextStep={this.nextStep}
            previousStep={this.previousStep}
            handleChangeTiCo={this.handleChangeTiCo}
            onChangeAcreBase={this.onChangeAcreBase}
            onChangeAcreComi={this.onChangeAcreComi}
            onChangeAcreBono={this.onChangeAcreBono}
            onChangeAcreTotal={this.onChangeAcreTotal}
            onChangeCoBase={this.onChangeCoBase}
            onChangeCoComi={this.onChangeCoComi}
            onChangeCoBono={this.onChangeCoBono}
            onChangeCoTotal={this.onChangeCoTotal}
            tipoCoacreditado={tipoCoacreditado}
            acreBase={acreBase}
            acreComisiones={acreComisiones}
            acreBonos={acreBonos}
            acreTotal={acreTotal}
            coBase={coBase}
            coComisiones={coComisiones}
            coBonos={coBonos}
            coTotal={coTotal}

            saveEgresoTar = {this.saveEgresoTar}
            saveEgresoAuto = {this.saveEgresoAuto}
            saveEgresoPer = {this.saveEgresoPer}
            saveEgresoHip = {this.saveEgresoHip}
            egresosTotales={egresosTotales}


            // EgresosObject={this.EgresosObject}
            // egresosPrincipales={egresosPrincipales}
          />
        );
      case 2:
        return (
          <ResultProfiling
            makePostData={this.makePostData}
            previousStep={this.previousStep}
            ingresosTotales={ingresosTotales}
            egresosTotales={egresosTotales}
          />
        );
      default:
        return <div>Error</div>;
    }
  };

  render() {
    const { step } = this.state;
    const { user } = this.props;
    console.log(user);
    const Step = Steps.Step;
    // if (this.state.tipoCoacreditado === 'Ninguno'){
    //   this.state.coBase = 0;
    //   this.state.coComisiones = 0;
    //   this.state.coBonos = 0;
    //   this.state.coTotal = 0;

    // }
    this.state.ingresosTotales = this.state.acreTotal + this.state.coTotal;
    this.state.egresosTotales = this.state.egresoTar.EgresoTarTotal + this.state.egresoAuto.EgresoTarTotal + this.state.egresoPer.EgresoTarTotal + this.state.egresoHip.EgresoTarTotal;
    // console.log(this.state.lol);
    return (
      <div>
        <LayoutContentWrapper style={{ height: 'auto' }}>
          <LayoutContent>
            <Row>
              <Row type="flex" justify="center" align="middle">
                <Col xs={22} sm={20} md={18} lg={14} xl={12}>
                  {/* <LayoutContentWrapper style={{ height: '100vh' }} > */}

                  <Progress percent={(step / 2) * 100} status="active" />
                  <Steps current={step}>
                    <Step
                      title="Datos de Empleo"
                      description="This is a description."
                    />
                    <Step
                      title="Ingresos y Egresos"
                      description="This is a description."
                    />
                  </Steps>
                  <ContentHolder>{this.switcherStep()}</ContentHolder>
                </Col>
              </Row>
            </Row>
          </LayoutContent>
        </LayoutContentWrapper>
      </div>
    );
  }
}

export default connect(
  containerSelector,
  dispatch
)(Perfilamiento);
