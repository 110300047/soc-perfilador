import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  Form,
  Input,
  Icon,
  Button,
  Col,
  Select,
  InputNumber,
  DatePicker,
  Row
} from "antd";
import { Number } from "core-js";
import {
  TxtAling
  // ContenForm,
  // RadioButton,
  // RadioGroup,
  // AlingRAdioButton,
} from "../../components/preperfilamiento/preperfilamiento.style";
const Option = Select.Option;

let id = 0;

class DynamicFieldSet extends Component {
  constructor(props) {
    super(props);
    this.state = { newProject: {} };
  }
  remove = k => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue("keys");
    // We need at least one passenger
    if (keys.length === 1) {
      return;
    }

    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k)
    });
  };

  add = () => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue("keys");
    console.log(keys);
    // console.log('Received values of form: ', form);
    if (keys.length < 4) {
      const nextKeys = keys.concat(id++);
      // can use data-binding to set
      // important! notify form to detect changes
      form.setFieldsValue({
        keys: nextKeys
      });
    }
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      const { nextStep, EgresosObject, yolo2 } = this.props;
      if (!err) {
        const yolo = this.props.form.getFieldsValue();
        // const lol = "150";
        // console.log('Received values of form: ', values);
        // console.log('Received values of form2: ', yolo);
        // console.log(lol.toNumber())
        // this.setState({
        //   newProject : yolo
        // });
        // this.state.newProject = this.props.form.getFieldsValue();
        // console.log('Received: ', this.state.newProject);
        EgresosObject(yolo);
        // yolo2(e);

        // nextStep();
      }
    });
  };

  render() {
    // const { MonthPicker } = DatePicker;
    const { getFieldDecorator, getFieldValue, previousStep } = this.props.form;
    const { handleChangebanco, handleChangeCreditoB } = this.props;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 }
      }
    };
    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 20, offset: 4 }
      }
    };
    getFieldDecorator("keys", { initialValue: [] });
    const { MonthPicker } = DatePicker;
    const keys = getFieldValue("keys");
    const formItems = keys.map((k, index) => (
      <Form.Item
        // {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
        // label={index === 0 ? 'Passengers' : ''}
        required={false}
        key={k}
      >
        <Col xs={4} sm={4} md={4} lg={4} xl={4}>
          {getFieldDecorator(`credito[${k}]`, {
            validateTrigger: ["onChange", "onBlur"],
            rules: [
              {
                required: true,
                whitespace: true,
                message: "Por favor introduzca tipo de credito."
              }
            ]
          })(
            <Select onChange={handleChangeCreditoB} style={{ width: "120px" }}>
              <Option value="Tarjeta Credito">Tarj. Cred.</Option>
              <Option value="Automovil">Automovil</Option>
              <Option value="Personal">Personal</Option>
              <Option value="Hipotecario">Hipotecario</Option>
            </Select>
          )}
        </Col>
        <Col xs={4} sm={4} md={4} lg={4} xl={4}>
          {getFieldDecorator(`banco[${k}]`, {
            validateTrigger: ["onChange", "onBlur"],
            rules: [
              {
                required: true,
                whitespace: true,
                message: "Por favor introduzca banco."
              }
            ]
          })(
            <Select
              onChange={handleChangebanco}
              style={{ width: "120px" }}
              // disabled={disa || doesntWork}
            >
              <Option value="Afirme">Afirme</Option>
              <Option value="Banamex">Banamex</Option>
              <Option value="Bancomer">Bancomer</Option>
              <Option value="Banorte">Banorte</Option>
              <Option value="Banregio">Banregio</Option>
              <Option value="HSBC">HSBC</Option>
              <Option value="Santander">Santander</Option>
              <Option value="Scotiabank">Scotiabank</Option>
            </Select>
          )}
        </Col>
        <Col xs={4} sm={4} md={4} lg={4} xl={4}>
          {getFieldDecorator(`fechaAp[${k}]`, {
            // validateTrigger: ['onChange', 'onBlur'],
            rules: [
              {
                required: true,
                // whitespace: true,
                message: "Por favor introduzca fecha de apertura de credito."
              }
            ]
          })(
            // <Input placeholder="passenger name" style={{ width: '60%', marginRight: 8 }} />

            <MonthPicker
              format={"MM/YYYY"}
              // onChange={onChangeDateExpenses2}
              placeholder="Mes de apertura"
              style={{ marginRight: "16px" }}
            />
          )}
        </Col>
        <Col xs={5} sm={5} md={5} lg={5} xl={5}>
          {getFieldDecorator(`saldo[${k}]`, {
            initialValue: 0,
            validateTrigger: ["onChange"],
            rules: [
              {
                required: true,
                // whitespace: true,
                message: "Por favor introduzca saldo de credito."
              }
            ]
          })(
            <InputNumber
              min={0}
              max={9000000}
              style={{ width: "100px" }}
              formatter={value =>
                `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={value => value.replace(/\$\s?|(,*)/g, "")}
              // onChange={onChangeBalanceExpenses2}
              step={100}
              // onChange={onChange}
            />
          )}
        </Col>
        <Col xs={5} sm={5} md={5} lg={5} xl={5}>
          {getFieldDecorator(`pagosMin[${k}]`, {
            initialValue: 0,
            validateTrigger: ["onChange"],
            rules: [
              {
                required: true,
                message: "Por favor introduzca pago minimo del credito."
              }
            ]
          })(
            <InputNumber
              min={0}
              max={9000000}
              style={{ width: "100px" }}
              // step={100}
              formatter={value =>
                `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={value => value.replace(/\$\s?|(,*)/g, "")}
              step={100}
              // onChange={onChange}
            />
          )}
        </Col>
        <Col xs={2} sm={2} md={2} lg={2} xl={2}>
          {keys.length > 1 ? (
            <Icon
              className="dynamic-delete-button"
              type="minus-circle-o"
              disabled={keys.length === 1}
              onClick={() => this.remove(k)}
            />
          ) : null}
        </Col>
      </Form.Item>
    ));
    return (
      <Form onSubmit={this.handleSubmit}>
        {formItems}
        <Form.Item {...formItemLayoutWithOutLabel}>
          <Button type="dashed" onClick={this.add} style={{ width: "60%" }}>
            <Icon type="plus" /> Agregar credito
          </Button>
        </Form.Item>
        {/* <Form.Item {...formItemLayoutWithOutLabel}>
          <Button type="primary" htmlType="submit">Submit</Button>
        </Form.Item> */}

        <div>
          <TxtAling>
            <Row type="flex" justify="center" align="middle">
              <Col xs={24} sm={6} md={3} lg={3} xl={3}>
                <Form.Item {...formItemLayoutWithOutLabel}>
                  <Button
                    onClick={previousStep}
                    style={{ width: "100px", marginTop: 16 }}
                  >
                    Atras
                  </Button>
                </Form.Item>
              </Col>
              <Col xs={24} sm={6} md={3} lg={3} xl={3}>
                <Form.Item {...formItemLayoutWithOutLabel}>
                  <Button
                    type="primary"
                    htmlType="submit"
                    style={{ width: "100px", marginTop: 16 }}
                  >
                    Siguiente
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </TxtAling>
        </div>
      </Form>
    );
  }
}

const WrappedDynamicFieldSet = Form.create({ name: "dynamic_form_item" })(
  DynamicFieldSet
);
// ReactDOM.render(<WrappedDynamicFieldSet />, mountNode);

export default WrappedDynamicFieldSet;
