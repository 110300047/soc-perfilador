import { takeLatest, put } from 'redux-saga/effects';
import socActions from '../../redux/perfilamiento/actions';
import containerActions from './actions';

export function* updateDataSaga({ payload }) {
  yield put(socActions.updateGeneralBalance(payload));
}

export default function* OrdersViewSaga() {
  yield takeLatest(containerActions.updateGeneralBalance.type, updateDataSaga);
}
