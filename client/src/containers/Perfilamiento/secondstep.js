import React, { Component } from "react";
import {
  Row,
  Col,
  InputNumber,
  Select,
  Button,
  Tabs,
  Form,
  DatePicker
} from "antd";
import ContentHolder from "../../components/utility/contentHolder";
import Numeral from "numeral";
import WrappedDynamicFieldSet from "./dynamicfield";
import {
  TxtAling
  // ContenForm,
  // RadioButton,
  // RadioGroup,
  // AlingRAdioButton,
} from "../../components/preperfilamiento/preperfilamiento.style";
import Egresos from './Outcome';

const TabPane = Tabs.TabPane;
const Option = Select.Option;



class SecondStep extends Component {
  constructor(props) {
    super(props);
    this.state = { newProject: {}, banco: "" };
  }

  // prueba1Men = () => {
  //     console.log(this.props.balanceExpenses2);

  //     // this.setState(
  //     //   {
  //     //     newProject: {
  //     //       value1: this.props.numeroSeguro,
  //     //       // value2: this.props.antiguedad,
  //     //       // value3: this.props.COFI,
  //     //       // value4: this.props.numeroSeguro,
  //     //       // value5: this.props.baNomina,
  //     //       // value6: this.props.tipoPagadora,
  //     //       // value7: this.props.fechaSat,
  //     //       // value8: this.props.CoSiFis,
  //     //       // value9: this.props.baDepos,
  //     //       // value10: this.props.actividadLab,
  //     //       // value11: this.props.fechaAlta,
  //     //       // value12: this.props.actividadEmpresa,
  //     //       // value13: this.props.altaHaciendaAct,

  //     //       // fechaNac: this.state.fechaNacM + '/' + this.state.fechaNacA,
  //     //     },
  //     //   },
  //     //   function() {
  //     //     console.log(this.state.newProject);

  //     //     // this.props.nextStep();
  //     //     // this.props.addProject(this.state.newProject);
  //     //   }
  //     // );
  //   };

  handleSubmit = e => {
    const { nextStep } = this.props;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        nextStep();
      }
      
      else {
        console.log(err);
        const yolo = this.props.form.getFieldsValue();
        console.log(yolo);
      }
    });
  };

  checkEgresoTar = (Banco, Saldo, Pagomin, Fecha) => {
    const {saveEgresoTar} = this.props;
    
    saveEgresoTar(Banco, Saldo, Pagomin, Fecha);
    // const {egresosTotales} = this.props; 
    console.log("funciona2");
    this.props.form.setFieldsValue({
      egresoTarjeta: "Answered"
    });

  } 
  checkEgresoAuto = (Banco, Saldo, Pagomin, Fecha) => {
    const {saveEgresoAuto} = this.props;
    
    saveEgresoAuto(Banco, Saldo, Pagomin, Fecha);
    // const {egresosTotales} = this.props; 
    console.log("funciona2");
    this.props.form.setFieldsValue({
      egresoAutomovil: "Answered"
    });

  } 
  checkEgresoPer = (Banco, Saldo, Pagomin, Fecha) => {
    const {saveEgresoPer} = this.props;
    
    saveEgresoPer(Banco, Saldo, Pagomin, Fecha);
    // const {egresosTotales} = this.props; 
    console.log("funciona2");
    this.props.form.setFieldsValue({
      egresoPersonal: "Answered"
    });

  } 
  checkEgresoHip = (Banco, Saldo, Pagomin, Fecha) => {
    const {saveEgresoHip} = this.props;
    
    saveEgresoHip(Banco, Saldo, Pagomin, Fecha);
    // const {egresosTotales} = this.props; 
    console.log("funciona2");
    this.props.form.setFieldsValue({
      egresoHipoteca: "Answered"
    });

  } 

  // yolo2 = e => {
  //   const { nextStep } = this.props;
  //   e.preventDefault();
  //   this.props.form.validateFieldsAndScroll((err, values) => {
  //     if (!err) {
  //       nextStep();
  //     }
  //   });
  // };

  handleChangebanco = value => {
    this.setState({
      banco: value
    });
  };

  handleChangeCreditoB = value => {
    // this.setState({
    //   credito: value,
    // });
  };

  formatTxt = value => {
    return Numeral(value).format("$0,0.00");
  };

  render() {
    const { MonthPicker } = DatePicker;
    const colStyle = {
      marginBottom: "16px"
    };
    const {
      nextStep,
      previousStep,
      handleChangeTiCo,
      onChangeAcreBase,
      onChangeAcreComi,
      onChangeAcreBono,
      onChangeAcreTotal,
      onChangeCoBase,
      onChangeCoComi,
      onChangeCoBono,
      onChangeCoTotal,
      tipoCoacreditado,
      acreBase,
      acreComisiones,
      acreBonos,
      acreTotal,
      coBase,
      coComisiones,
      coBonos,
      coTotal,
      egresosTotales,

      // EgresosObject
      saveEgresoTar,
      saveEgresoAuto,
      saveEgresoPer,
      saveEgresoHip

    } = this.props;
    // const { Option } = Select;

    let disa2 = true,
      lol = "16px";

    if (tipoCoacreditado == "Ninguno" || tipoCoacreditado == "") {
      disa2 = true;
    } else {
      disa2 = false;
    }
    const { getFieldDecorator } = this.props.form;

    // lol=this.props.coBase+this.props.coBonos+this.props.coComisiones;

    return (
      <Form onSubmit={this.handleSubmit}>
        <Row gutter={24}>
          <h3>Ingresos y gastos</h3>
        </Row>
        <Row gutter={24}>
          <ContentHolder>
            <Tabs defaultActiveKey="1" style={{ marginLeft: "0px" }}>
              <TabPane tab="Ingresos Netos Mensuales" key="1">
                <Row gutter={24} type="flex" align="middle">
                  {/* <Col xs={6} sm={6} md={6} lg={5} xl={4} style={colStyle} /> */}
                  <Col xs={24} sm={24} md={24} lg={24} xl={24} style={colStyle}>
                    Tiene co-acreditado:
                  </Col>
                  <Col xs={24} sm={24} md={24} lg={24} xl={24} style={colStyle}>
                    {/* <ContentHolder> */}
                    <Form.Item>
                      {getFieldDecorator("tipoCoacreditado", {
                        initialValue: tipoCoacreditado,
                        rules: [
                          {
                            required: true,
                            message: "Favor de responder"
                          }
                        ]
                      })(
                        <Select
                          setFieldsValue={tipoCoacreditado}
                          onChange={handleChangeTiCo}
                          style={{ minWidth: "120px" }}
                        >
                          <Option value="Ninguno">Ninguno</Option>
                          <Option value="Cónyuge">Cónyuge</Option>
                          <Option value="Concubino(a)">Concubino(a)</Option>
                          <Option value="Hermano(a)">Hermano(a)</Option>
                          <Option value="Hijo(a)">Hijo(a)</Option>
                          <Option value="Padres">Padres</Option>
                        </Select>
                      )}
                    </Form.Item>

                    {/* </ContentHolder> */}
                  </Col>
                  {/* <Col xs={12} sm={12} md={12} lg={12} xl={12} style={colStyle}>
                    <span> </span>
              </Col> */}
                </Row>
                <Row gutter={24} type="flex" align="middle">
                  <Col xs={6} sm={6} md={6} lg={5} xl={4} style={colStyle} />
                  <Col xs={8} sm={7} md={6} lg={5} xl={4} style={colStyle}>
                    Acreditado:
                  </Col>
                  <Col xs={10} sm={11} md={12} lg={14} xl={16} style={colStyle}>
                    Co-acreditado:
                  </Col>
                  {/* <Col xs={12} sm={12} md={12} lg={12} xl={12} style={colStyle}>
                    <span> </span>
              </Col> */}
                </Row>
                <Row gutter={24} type="flex" align="middle">
                  <Col xs={6} sm={6} md={6} lg={5} xl={4} style={colStyle}>
                    Base:
                    {/* <ContentHolder> */}
                  </Col>
                  <Col xs={8} sm={7} md={6} lg={5} xl={4} style={colStyle}>
                    <Form.Item>
                      {getFieldDecorator("acreBase", {
                        initialValue: acreBase,
                        rules: [
                          {
                            required: true,
                            message: "Favor de responder"
                          }
                        ]
                      })(
                        <InputNumber
                          min={0}
                          max={9000000}
                          // style={{ marginLeft: 16 }}
                          // value={Numeral(montoMayorAprox).format('$0,0.00')}
                          style={{ width: "100px", marginLeft: 1 }}
                          // defaultValue={acreBase}
                          value={acreBase}
                          formatter={value =>
                            `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          parser={value => value.replace(/\$\s?|(,*)/g, "")}
                          onChange={onChangeAcreBase}
                        />
                      )}
                    </Form.Item>
                    {/* </ContentHolder> */}
                  </Col>
                  <Col xs={10} sm={11} md={12} lg={14} xl={16} style={colStyle}>
                    <Form.Item>
                      {getFieldDecorator("coBase", {
                        initialValue: coBase,
                        rules: [
                          {
                            required: true,
                            message: "Favor de responder"
                          }
                        ]
                      })(
                        <InputNumber
                          min={0}
                          max={9000000}
                          disabled={disa2}
                          // value={Numeral(montoMayorAprox).format('$0,0.00')}
                          style={{ width: "100px" }}
                          // defaultValue={coBase}
                          value={coBase}
                          formatter={value =>
                            `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          parser={value => value.replace(/\$\s?|(,*)/g, "")}
                          onChange={onChangeCoBase}
                        />
                      )}
                    </Form.Item>
                    {/* </ContentHolder> */}
                  </Col>
                </Row>
                <Row gutter={24} type="flex" align="middle">
                  <Col xs={6} sm={6} md={6} lg={5} xl={4} style={colStyle}>
                    Comisiones:
                    {/* <ContentHolder> */}
                  </Col>
                  <Col xs={8} sm={7} md={6} lg={5} xl={4} style={colStyle}>
                    <Form.Item>
                      {getFieldDecorator("acreComisiones", {
                        initialValue: acreComisiones,
                        rules: [
                          {
                            required: true,
                            message: "Favor de responder"
                          }
                        ]
                      })(
                        <InputNumber
                          min={0}
                          max={9000000}
                          // style={{ marginLeft: 16 }}
                          // value={Numeral(montoMayorAprox).format('$0,0.00')}
                          style={{ width: "100px", marginLeft: 1 }}
                          defaultValue={acreComisiones}
                          formatter={value =>
                            `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          parser={value => value.replace(/\$\s?|(,*)/g, "")}
                          onChange={onChangeAcreComi}
                        />
                      )}
                    </Form.Item>
                    {/* </ContentHolder> */}
                  </Col>
                  <Col xs={10} sm={11} md={12} lg={14} xl={16} style={colStyle}>
                    <Form.Item>
                      {getFieldDecorator("coComisiones", {
                        initialValue: coComisiones,
                        rules: [
                          {
                            required: true,
                            message: "Favor de responder"
                          }
                        ]
                      })(
                        <InputNumber
                          min={0}
                          max={9000000}
                          // style={{ marginLeft: 16 }}
                          // value={Numeral(montoMayorAprox).format('$0,0.00')}
                          style={{ width: "100px" }}
                          // defaultValue={coComisiones}
                          value={coComisiones}
                          formatter={value =>
                            `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          parser={value => value.replace(/\$\s?|(,*)/g, "")}
                          onChange={onChangeCoComi}
                          disabled={disa2}
                        />
                      )}
                    </Form.Item>
                    {/* </ContentHolder> */}
                  </Col>
                </Row>
                <Row gutter={24} type="flex" align="middle">
                  <Col xs={6} sm={6} md={6} lg={5} xl={4} style={colStyle}>
                    Bonos:
                    {/* <ContentHolder> */}
                  </Col>
                  <Col xs={8} sm={7} md={6} lg={5} xl={4} style={colStyle}>
                    <Form.Item>
                      {getFieldDecorator("acreBonos", {
                        initialValue: acreBonos,
                        rules: [
                          {
                            required: true,
                            message: "Favor de responder"
                          }
                        ]
                      })(
                        <InputNumber
                          min={0}
                          max={9000000}
                          // style={{ marginLeft: 16 }}
                          // value={Numeral(montoMayorAprox).format('$0,0.00')}
                          style={{ width: "100px", marginLeft: 1 }}
                          defaultValue={acreBonos}
                          formatter={value =>
                            `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          parser={value => value.replace(/\$\s?|(,*)/g, "")}
                          onChange={onChangeAcreBono}
                        />
                      )}
                    </Form.Item>
                    {/* </ContentHolder> */}
                  </Col>
                  <Col xs={10} sm={11} md={12} lg={14} xl={16} style={colStyle}>
                    <Form.Item>
                      {getFieldDecorator("coBonos", {
                        initialValue: coBonos,
                        rules: [
                          {
                            required: true,
                            message: "Favor de responder"
                          }
                        ]
                      })(
                        <InputNumber
                          min={0}
                          max={9000000}
                          // style={{ marginLeft: 16 }}
                          // value={Numeral(montoMayorAprox).format('$0,0.00')}
                          style={{ width: "100px" }}
                          // defaultValue={coBonos}
                          value={coBonos}
                          formatter={value =>
                            `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          parser={value => value.replace(/\$\s?|(,*)/g, "")}
                          onChange={onChangeCoBono}
                          disabled={disa2}
                        />
                      )}
                    </Form.Item>
                    {/* </ContentHolder> */}
                  </Col>
                </Row>
                <Row gutter={24} type="flex" align="middle">
                  <Col xs={6} sm={6} md={6} lg={5} xl={4} style={colStyle}>
                    Total:
                    {/* <ContentHolder> */}
                  </Col>
                  <Col xs={8} sm={7} md={6} lg={5} xl={4} style={colStyle}>
                    <InputNumber
                      // min={0}
                      // max={50000}
                      // style={{ marginLeft: 16 }}
                      // value={Numeral(montoMayorAprox).format('$0,0.00')}
                      style={{ width: "100px", marginLeft: 1 }}
                      value={acreTotal}
                      formatter={value =>
                        `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={value => value.replace(/\$\s?|(,*)/g, "")}
                      // onChange={onChangeAcreTotal}
                      disabled={true}
                    />
                    {/* </ContentHolder> */}
                  </Col>
                  <Col xs={10} sm={11} md={12} lg={14} xl={16} style={colStyle}>
                    <InputNumber
                      // min={0}
                      // max={50000}
                      // style={{ marginLeft: 16 }}
                      // value={Numeral(montoMayorAprox).format('$0,0.00')}
                      style={{ width: "100px" }}
                      value={coTotal}
                      formatter={value =>
                        `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={value => value.replace(/\$\s?|(,*)/g, "")}
                      // onChange={onChangeCoTotal}
                      disabled={true}
                    />
                    {/* </ContentHolder> */}
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="Egresos Bancarios" key="2">
                <Row type="flex" justify="space-around" align="middle">
                  {/* <Col xs={24} sm={12} md={12} lg={12} xl={4} style={colStyle}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Credito
                      </Col>
                      
                    </Row>
                  </Col> */}
                  <Col xs={24} sm={12} md={12} lg={12} xl={5} style={colStyle}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Banco
                      </Col>
                      {/* <Col xs={24} sm={24} md={24} lg={24} xl={24} style={colStyle}>
                        <Select
                          onChange={onChangeBankExpenses}
                          style={{ width: '120px' }}
                        >
                          <Option value="Afirme">Afirme</Option>
                          <Option value="Banamex">Banamex</Option>
                          <Option value="Bancomer">Bancomer</Option>
                          <Option value="Banorte">Banorte</Option>
                          <Option value="Banregio">Banregio</Option>
                          <Option value="HSBC">HSBC</Option>
                          <Option value="Santander">Santander</Option>
                          <Option value="Scotiabank">Scotiabank</Option>
                        </Select>
                      </Col> */}
                    </Row>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={12} xl={7} style={colStyle}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Fecha apertura
                      </Col>
                      {/* <Col xs={24} sm={24} md={24} lg={24} xl={24} style={colStyle}>
                        <MonthPicker
                          onChange={onChangeDateExpenses}
                          placeholder="Mes de apertura"
                          style={{ marginRight: '16px'}}
                        />
                      </Col> */}
                    </Row>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={12} xl={6} style={colStyle}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Saldo actual
                      </Col>
                      {/* <Col xs={24} sm={24} md={24} lg={24} xl={24} style={colStyle}>
                        <InputNumber
                          min={0}
                          max={9000000}
                          style={{ width: '100px' }}
                          defaultValue={coBase}
                          formatter={value =>
                            `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                          }
                          parser={value => value.replace(/\$\s?|(,*)/g, '')}
                          onChange={onChangeBalanceExpenses}
                          step={100}
                          // onChange={onChange}
                        />
                      </Col> */}
                    </Row>
                  </Col>
                  <Col xs={24} sm={24} md={24} lg={24} xl={6} style={colStyle}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Pagos minimos
                      </Col>
                      {/* <Col xs={24} sm={24} md={24} lg={24} xl={24} style={colStyle}>
                        <InputNumber
                          defaultValue={1000}
                          style={{ width: '100px' }}
                          formatter={value =>
                            `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                          }
                          parser={value => value.replace(/\$\s?|(,*)/g, '')}
                          step={100}
                          // onChange={onChange}
                        />
                      </Col> */}
                    </Row>
                  </Col>
                </Row>
                <Row type="flex" justify="center" align="middle">
                  <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    {/* <DynamicFieldSet /> */}
                    {/* <WrappedDynamicFieldSet
                      handleChangebanco={this.handleChangebanco}
                      handleChangeCreditoB={this.handleChangeCreditoB}
                      nextStep={nextStep}
                      previousStep={previousStep}
                      handleSubmit={this.handleSubmit}
                      EgresosObject={EgresosObject}
                      yolo2={this.yolo2}
                    /> */}

                    <Form.Item>
                      {getFieldDecorator("egresoTarjeta", {
                        rules: [
                          {
                            required: true,
                            message: "Favor de responder"
                          }
                        ]
                      })(
                        <Egresos 
                        TipoEgreso = {"Tarjeta Credito"}
                        saveValue = {this.checkEgresoTar}
                        />
                        
                      )}
                    </Form.Item>
                  </Col>
                </Row>
                <Row type="flex" justify="center" align="middle">
                  <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    <Form.Item>
                      {getFieldDecorator("egresoAutomovil", {
                        rules: [
                          {
                            required: true,
                            message: "Favor de responder"
                          }
                        ]
                      })(
                        <Egresos 
                        TipoEgreso = {"Automovil"}
                        saveValue = {this.checkEgresoAuto}
                        />
                        
                      )}
                    </Form.Item>
                  </Col>
                </Row>
                <Row type="flex" justify="center" align="middle">
                  <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    <Form.Item>
                      {getFieldDecorator("egresoPersonal", {
                        rules: [
                          {
                            required: true,
                            message: "Favor de responder"
                          }
                        ]
                      })(
                        <Egresos 
                        TipoEgreso = {"Personal"}
                        saveValue = {this.checkEgresoPer}
                        />
                        
                      )}
                    </Form.Item>
                  </Col>
                </Row>
                <Row type="flex" justify="center" align="middle">
                  <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    <Form.Item>
                      {getFieldDecorator("egresoHipoteca", {
                        rules: [
                          {
                            required: true,
                            message: "Favor de responder"
                          }
                        ]
                      })(
                        <Egresos 
                        TipoEgreso = {"Hipotecario"}
                        saveValue = {this.checkEgresoHip}
                        />
                        
                      )}
                    </Form.Item>
                  </Col>
                </Row>
              </TabPane>
            </Tabs>
          </ContentHolder>
        </Row>
        {/* <InputNumber
                      
                      style={{ width: "100px", marginLeft: 1 }}
                      value={egresosTotales}
                      formatter={value =>
                        `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={value => value.replace(/\$\s?|(,*)/g, "")}
                      disabled={true}
                    /> */}
        {/* <Button onClick={this.prueba1Men}>Prueba 1 Men</Button> */}

        <div>
          <TxtAling>
            <Row type="flex" justify="center" align="middle">
              <Col xs={24} sm={6} md={3} lg={3} xl={3}>
                <Button
                  onClick={previousStep}
                  style={{ width: '100px', marginTop: 16 }}
                >
                  Atras
                </Button>
              </Col>
              <Col xs={24} sm={6} md={3} lg={3} xl={3}>
                <Button
                  type="primary"
                  htmlType="submit"
                  style={{ width: '100px', marginTop: 16 }}
                >
                  Siguiente
                </Button>
              </Col>
            </Row>
          </TxtAling>
        </div>
      </Form>
    );
  }
}
export default Form.create()(SecondStep);
