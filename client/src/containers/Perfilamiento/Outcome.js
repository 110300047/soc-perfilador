import React, { Component } from 'react';
import { Button, Col, Select, DatePicker, Row, Form, InputNumber, Input } from 'antd';
// import { Link } from 'react-router-dom';
// import { Circle, IconCheck, StatusProfiling } from './preperfilamiento.style';
import Fechas from '../../components/preperfilamiento/fechas';
const Option = Select.Option;

const { MonthPicker } = DatePicker;
// var today = new Date();
// var ThisYear = today.getFullYear()

export default class Egresos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkbanco: false,
      checksaldo: false,
      checkPagomin: false,
      checkFecha: false,
    //   Tipo: 'Tarjeta credito',
      banco: '',
      saldo: 0,
      pagoMinimo: 0,
      fecha: ''
    
    };
  }
  handleChangebanco = value => {
    this.setState({
      banco: value,
    //   checkbanco: true
    },
    function() {
      const {saveValue} = this.props;
      if (this.state.checkbanco && this.state.checksaldo && this.state.checkPagomin && this.state.checkFecha){
        saveValue(this.state.banco, this.state.saldo, this.state.pagoMinimo, this.state.fecha);
      }
    });
  };

  onChangeBalanceExpenses = value => {
    this.setState(
      {
        saldo: value,
      },
      function() {
        this.setState({
          checksaldo: true
        });
        const {saveValue} = this.props;
        if (this.state.checkbanco && this.state.checkPagomin && this.state.checkFecha){
          saveValue(this.state.banco, this.state.saldo, this.state.pagoMinimo, this.state.fecha);
        }
      }
    );
  };
  onChangeMinPayment = value => {
    this.setState(
      {
        pagoMinimo: value,
      },
      function() {
        this.setState({
          checkPagomin: true
        });
        const {saveValue} = this.props;
        if (this.state.checkbanco && this.state.checksaldo && this.state.checkFecha){
          saveValue(this.state.banco, this.state.saldo, this.state.pagoMinimo, this.state.fecha);
        }
      }
    );
  };
  checkDateEgreso = (day, month, year, dd, mm, aaaa) => {

    if((day == true) && (month == true) && (year == true)){
        // this.props.form.setFieldsValue({
        //   feAlta: this.props.feAlta
        // });
        this.setState({
          checkFecha: true
        });
    } 
  }
  fechaEgreso = fechaFull => {
    // console.log("me cago en dios"+ fechaFull);
    this.setState({
      fecha: fechaFull,
    },
    function() {
      const {saveValue} = this.props;
      if (this.state.checkbanco && this.state.checksaldo && this.state.checkPagomin && this.state.checkFecha){
        saveValue(this.state.banco, this.state.saldo, this.state.pagoMinimo, this.state.fecha);
      }
    });
    // this.state.feApCreBan = fechaFull;
    // console.log("fecha"+ this.state.fechaNac);
  };

  prueba = () =>{
        // const {feApCreBan} = this.props
        // const {day} = this.state;
        console.log(this.state);
        // console.log(ThisYear);
        // this.setState({
        //   feApCreBan: fechaFull,
        // });
        // console.log("fecha"+ this.state.feApCreBan);
      }



  render() {
    
    const { TipoEgreso } = this.props;
    this.state.checkbanco = this.state.banco=== ''? false : true;
    return (
      <div>                
        <h3>{TipoEgreso}</h3>
        <Row gutter={8} >
        {/* <Col xs={4} sm={4} md={4} lg={4} xl={4}>
          
            <Input value={TipoEgreso} disabled={true}  style={{ width: '100%', marginRight: 8 }} />
          
        </Col> */}
        <Col xs={5} sm={5} md={5} lg={5} xl={5}>
          
            <Select
              onChange={this.handleChangebanco}
              style={{ width: "100px" }}
              // disabled={disa || doesntWork}
            >
              <Option value="Afirme">Afirme</Option>
              <Option value="Banamex">Banamex</Option>
              <Option value="Bancomer">Bancomer</Option>
              <Option value="Banorte">Banorte</Option>
              <Option value="Banregio">Banregio</Option>
              <Option value="HSBC">HSBC</Option>
              <Option value="Santander">Santander</Option>
              <Option value="Scotiabank">Scotiabank</Option>
            </Select>
          
        </Col>
        <Col xs={7} sm={7} md={7} lg={7} xl={7}>
          
             {/* <Input placeholder="passenger name" style={{ width: '60%', marginRight: 8 }} /> */}

            {/* <MonthPicker
              format={"MM/YYYY"}
              // onChange={onChangeDateExpenses2}
              placeholder="Mes de apertura"
              style={{ marginRight: "16px" }}
            /> */}
            <Fechas fecha={this.fechaEgreso} 
                      checkDate={this.checkDateEgreso}
                      monthPicker = {true}
                      disa={false}/>
          
        </Col>
        <Col xs={6} sm={6} md={6} lg={6} xl={6}>
          
            <InputNumber
              min={0}
              max={9000000}
              style={{ width: "100px" }}
              formatter={value =>
                `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={value => value.replace(/\$\s?|(,*)/g, "")}
              onChange={this.onChangeBalanceExpenses}
              step={100}
              // onChange={onChange}
            />
          
        </Col>
        <Col xs={6} sm={6} md={6} lg={6} xl={6}>
          
            <InputNumber
              min={0}
              max={9000000}
              style={{ width: "100px" }}
              // step={100}
              formatter={value =>
                `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={value => value.replace(/\$\s?|(,*)/g, "")}
              onChange={this.onChangeMinPayment}
              step={100}
              // onChange={onChange}
            />
          
        </Col>
        </Row>
        {/* <Button onClick={this.prueba}>Prueba 1 Men</Button> */}

      </div>
    );
  }
}
