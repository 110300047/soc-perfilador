import React, { Component } from 'react';
import {
  Row,
  Col,
  Input,
  Select,
  Button,
  Tabs,
  DatePicker,
  Checkbox,
  Form,
} from 'antd';
import Radio, { RadioGroup } from '../../components/uielements/radio';
import ContentHolder from '../../components/utility/contentHolder';
import { TxtAling } from '../../components/preperfilamiento/preperfilamiento.style';
import Fechas from '../../components/preperfilamiento/fechas';
const TabPane = Tabs.TabPane;
const Option = Select.Option;
const CheckboxGroup = Checkbox.Group;

function callback(key) {}

// function onChange(checkedValues) {

//   console.log('checked = ', checkedValues);
//   lolazo = checkedValues;
// }

// const plainOptions = ['Apple', 'Pear', 'Orange'];
const options = [
  { label: 'Nomina', value: '1' },
  { label: 'Independiente', value: '2' },
  { label: 'AccionistaOrange', value: '3' },
];
// function formatter(value) {
//   return `${Numeral(value).format('$0,0.00')}`;
// }
class FirstStep extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newProject: {},
      nomina: true,
      independiente: true,
      Accionista: true,
      checkIndependiente: false,
      checkAccionista: false,
      checkCofi: false,
      chekNomina: false,
      // completeDateAntiguedad: false,
    };
  }

  handleSubmit = e => {
    const { nextStep } = this.props;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        nextStep();
      }
      else {
        console.log(err);
      }
    });
  };

  checkDateAntiguedad = (day, month, year, dd, mm, aaaa) => {
    if((day == true) && (month == true) && (year == true)){
      this.props.form.setFieldsValue({
        antiguedad: this.props.antiguedad 
      });
    }
  } 
  checkDateSat = (day, month, year, dd, mm, aaaa) => {
    if((day == true) && (month == true) && (year == true)){
      this.props.form.setFieldsValue({
        fechaSat: this.props.fechaSat 
      });
    }
  } 
  checkDateAlta = (day, month, year, dd, mm, aaaa) => {
    if((day == true) && (month == true) && (year == true)){
      this.props.form.setFieldsValue({
        fechaAlta: this.props.fechaAlta 
      });
    }
  } 

  handlePaPuCre = e => {
    this.onChange(e);
    const statusA = e.includes('1') ? true : false;
    const statusB = e.includes('2') ? true : false;
    const statusC = e.includes('3') ? true : false;
    this.setState(
      {
        checkNomina: statusA,
        checkIndependiente: statusB,
        checkAccionista: statusC,
      },
      () => {
        this.props.form.validateFields(
          [
            'COFI',
            'baNomina',
            'tipoPagadora',
            'fechaSat',
            'actividadLab',
            'baDepos',
            'CoSiFis',
            'fechaAlta',
            'actividadEmpresa',
            'altaHaciendaAct',
          ],
          {
            force: true,
          }
        );
      }
    );
  };

  onChange = checkedValues => {
    if (checkedValues.includes('1')) {
      this.setState({
        nomina: false,
      });
    } else {
      this.setState({
        nomina: true,
      });
    }
    if (checkedValues.includes('2')) {
      this.setState({
        independiente: false,
      });
    } else {
      this.setState({
        independiente: true,
      });
    }
    if (checkedValues.includes('3')) {
      this.setState({
        Accionista: false,
      });
    } else {
      this.setState({
        Accionista: true,
      });
    }
  };

  changeCOFI = e => {
    const { onChangeCOFI } = this.props;
    onChangeCOFI(e);
    const status = e.target.value != '22' ? true : false;
    this.setState(
      {
        checkCofi: status,
      },
      () => {
        this.props.form.validateFields(['numeroSeguro'], {
          force: true,
        });
      }
    );
  };

  // prueba1Men = () => {
  //   console.log(this.props.numeroSeguro);

  //   // this.setState(
  //   //   {
  //   //     newProject: {
  //   //       value1: this.props.numeroSeguro,
  //   //       // value2: this.props.antiguedad,
  //   //       // value3: this.props.COFI,
  //   //       // value4: this.props.numeroSeguro,
  //   //       // value5: this.props.baNomina,
  //   //       // value6: this.props.tipoPagadora,
  //   //       // value7: this.props.fechaSat,
  //   //       // value8: this.props.CoSiFis,
  //   //       // value9: this.props.baDepos,
  //   //       // value10: this.props.actividadLab,
  //   //       // value11: this.props.fechaAlta,
  //   //       // value12: this.props.actividadEmpresa,
  //   //       // value13: this.props.altaHaciendaAct,

  //   //       // fechaNac: this.state.fechaNacM + '/' + this.state.fechaNacA,
  //   //     },
  //   //   },
  //   //   function() {
  //   //     console.log(this.state.newProject);

  //   //     // this.props.nextStep();
  //   //     // this.props.addProject(this.state.newProject);
  //   //   }
  //   // );
  // };

  render() {
    const {
      onChangeOcupacion,
      // handleChangeDateAntiguedad,
      fechaAntiguedad,
      onChangeNumeroSeg,
      handleChangeBaNom,
      handleChangeTipoPag,
      // handleChangeDateSat,
      fechaSatF,
      onChangeCSF,
      handleChangeBaDep,
      handleChangeActividad,
      // handleChangeDateAlt,
      fechaAltaF,
      handleChangeActEmp,
      onChangeAHAct,
      COFI,
      numeroSeguro,
      baNomina,
      tipoPagadora,
      fechaSat,
      CoSiFis,
      baDepos,
      actividadLab,
      fechaAlta,
      actividadEmpresa,
      altaHaciendaAct,
      ocupacion,
      antiguedad,
    } = this.props;

    const colStyle = {
      marginBottom: '16px',
    };

    let aux = true,
      yolo = true;
    if (COFI == '11') {
      // numeroSeguro = '';
      aux = false;
      // console.log('checked = ', lolazo);
    } else {
      aux = true;
    }

    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit}>
        <Row gutter={24} type="flex" align="middle">
          <h3>Datos de Empleado</h3>
        </Row>
        <Row gutter={24} style={{ marginTop: 30 }} type="flex" align="middle">
          <Col xs={9} sm={4} md={3} lg={3} xl={3} style={{marginBottom:'16px', paddingLeft: 0 }}>
            Ocupacion / Actividad:
          </Col>
          <Col xs={15} sm={8} md={9} lg={9} xl={9} style={{marginBottom:'-6px' }}>
            <Form.Item>
              {getFieldDecorator('ocupacion', {
                initialValue: ocupacion,
                rules: [
                  {
                    required: true,
                    message: 'Favor de responder',
                  },
                ],
              })(
                <Input
                  onChange={onChangeOcupacion}
                  style={{ maxWidth: '350px' }}
                  placeholder="Basic usage"
                />
              )}
            </Form.Item>
          </Col>
          <Col xs={9} sm={4} md={3} lg={3} xl={3} style={{marginBottom:'16px', paddingLeft: 0 }}>
            Antigüedad:
          </Col>
          <Col xs={15} sm={8} md={9} lg={9} xl={9} style={{marginBottom:'25px'}} >
            <Form.Item>
              {/* {getFieldDecorator('antiguedad', {
                // initialValue: antiguedad,
                setFieldsValue: antiguedad,
                rules: [
                  {
                    required: true,
                    message: 'Favor de responder',
                  },
                ],
              })(
                <DatePicker
                  onChange={handleChangeDateAntiguedad}
                  placeholder="Antigüedad"
                  style={{ maxWidth: '350px' }}
                />
              )} */}
              {getFieldDecorator('antiguedad', {
                    rules: [
                      {
                        required: true,
                        message:
                          'Favor de responder',
                      },
                    ],
                  })(
                    <Fechas fecha={fechaAntiguedad}
                            checkDate={this.checkDateAntiguedad} 
                            monthPicker = {false}/>
                  )}
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24} style={{ marginTop: 30 }} type="flex" align="middle">
          <Col xs={24} sm={24} md={24} lg={24} xl={24} style={colStyle}>
            <Form.Item>
              {getFieldDecorator('checkedValues', {
                initialValue: [],
                rules: [
                  {
                    required: true,
                    message: 'Favor de responder',
                  },
                ],
              })(
                <CheckboxGroup
                  options={options}
                  onChange={this.handlePaPuCre}
                />
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          {/* <Box title={'lol'}> */}
          <ContentHolder>
            <Tabs
              defaultActiveKey="1"
              onChange={callback}
              style={{ marginLeft: '16px' }}
            >
              <TabPane tab="Nomina" key="1" disabled={this.state.nomina}>
                {/* tab1 */}
                <Row gutter={24} type="flex" align="middle">
                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Cofinanciamiento:
                      </Col>
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        <ContentHolder>
                          <Form.Item>
                            {getFieldDecorator('COFI', {
                              initialValue: COFI,
                              rules: [
                                {
                                  required: this.state.checkNomina,
                                  message: 'Favor de responder',
                                },
                              ],
                            })(
                              <RadioGroup
                                onChange={this.changeCOFI}
                                setFieldsValue={COFI}
                                disabled={this.state.nomina}
                              >
                                <Radio value={11}>Si</Radio>
                                <Radio value={22}>No</Radio>
                              </RadioGroup>
                            )}
                          </Form.Item>
                        </ContentHolder>
                      </Col>
                    </Row>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Número de seguro social:
                      </Col>
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        <Form.Item>
                          {getFieldDecorator('numeroSeguro', {
                            initialValue: numeroSeguro,
                            rules: [
                              {
                                required: this.state.checkCofi,
                                message: 'Favor de responder',
                              },
                            ],
                          })(
                            <Input
                              onChange={onChangeNumeroSeg}
                              // placeholder="Basic usage"
                              style={{ maxWidth: '200px', marginTop: 20 }}
                              disabled={aux || this.state.nomina}
                              maxLength={12}
                              minLength={12}
                              // value={Numeral(numeroSeguro).format('$0,0')}
                              // formatter={value =>
                              //   `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                              // }
                              // parser={value => value.replace(/\$\s?|(-*)/g, '')}
                              // value = {numeroSeguro}
                            />
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row gutter={24} type="flex" align="middle">
                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Banco Nomina:
                      </Col>
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        <ContentHolder>
                          <Form.Item>
                            {getFieldDecorator('baNomina', {
                              initialValue: baNomina,
                              rules: [
                                {
                                  required: this.state.checkNomina,
                                  message: 'Favor de responder',
                                },
                              ],
                            })(
                              <Select
                                setFieldsValue={baNomina}
                                onChange={handleChangeBaNom}
                                style={{ minWidth: '120px' }}
                                disabled={this.state.nomina}
                              >
                                <Option value="Afirme">Afirme</Option>
                                <Option value="Banamex">Banamex</Option>
                                <Option value="Bancomer">Bancomer</Option>
                                <Option value="Banorte">Banorte</Option>
                                <Option value="Banregio">Banregio</Option>
                                <Option value="HSBC">HSBC</Option>
                                <Option value="Santander">Santander</Option>
                                <Option value="Scotiabank">Scotiabank</Option>
                              </Select>
                            )}
                          </Form.Item>
                        </ContentHolder>
                      </Col>
                    </Row>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Tipo Pagadora:
                      </Col>
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        <ContentHolder>
                          <Form.Item>
                            {getFieldDecorator('tipoPagadora', {
                              initialValue: tipoPagadora,
                              rules: [
                                {
                                  required: this.state.checkNomina,
                                  message: 'Favor de responder',
                                },
                              ],
                            })(
                              <Select
                                setFieldsValue={tipoPagadora}
                                onChange={handleChangeTipoPag}
                                style={{ minWidth: '120px' }}
                                disabled={this.state.nomina}
                              >
                                <Option value="Empresa">Empresa</Option>
                                <Option value="Pagadora">Pagadora</Option>
                              </Select>
                            )}
                          </Form.Item>
                        </ContentHolder>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </TabPane>

              <TabPane
                tab="Independiente"
                key="2"
                disabled={this.state.independiente}
              >
                {/* Content of Tab Pane 2 */}
                <Row gutter={24} type="flex" align="middle">
                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Fecha de Alta/Reactivacion SAT:
                      </Col>
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        {/* <ContentHolder> */}
                          <Form.Item>
                            {/* {getFieldDecorator('fechaSat', {
                              initialValue: fechaSat,
                              rules: [
                                {
                                  required: this.state.checkIndependiente,
                                  message: 'Favor de responder',
                                },
                              ],
                            })(
                              <DatePicker
                                onChange={handleChangeDateSat}
                                placeholder="Fecha Alta/Reactivacion"
                                disabled={this.state.independiente}
                              />
                            )} */}
                            {getFieldDecorator('fechaSat', {
                              rules: [
                                {
                                  required: this.state.checkIndependiente,
                                  message:
                                    'Favor de responder',
                                },
                              ],
                            })(
                              <Fechas fecha={fechaSatF}
                                      checkDate={this.checkDateSat} 
                                      monthPicker = {false}/>
                            )}
                          </Form.Item>
                        {/* </ContentHolder> */}
                      </Col>
                    </Row>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Constancia de Situación Fiscal:
                      </Col>
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        <Form.Item>
                          {getFieldDecorator('CoSiFis', {
                            initialValue: CoSiFis,
                            rules: [
                              {
                                required: this.state.checkIndependiente,
                                message: 'Favor de responder',
                              },
                            ],
                          })(
                            <Input
                              onChange={onChangeCSF}
                              placeholder="Basic usage"
                              style={{ maxWidth: '200px', marginTop: '34px' }}
                              disabled={this.state.independiente}
                            />
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row gutter={24} type="flex" align="middle">
                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Banco de depositos:
                      </Col>
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        <ContentHolder>
                          <Form.Item>
                            {getFieldDecorator('baDepos', {
                              initialValue: baDepos,
                              rules: [
                                {
                                  required: this.state.checkIndependiente,
                                  message: 'Favor de responder',
                                },
                              ],
                            })(
                              <Select
                                setFieldsValue={baDepos}
                                onChange={handleChangeBaDep}
                                style={{ minWidth: '120px' }}
                                disabled={this.state.independiente}
                              >
                                <Option value="Afirme">Afirme</Option>
                                <Option value="Banamex">Banamex</Option>
                                <Option value="Bancomer">Bancomer</Option>
                                <Option value="Banorte">Banorte</Option>
                                <Option value="Banregio">Banregio</Option>
                                <Option value="HSBC">HSBC</Option>
                                <Option value="Santander">Santander</Option>
                                <Option value="Scotiabank">Scotiabank</Option>
                              </Select>
                            )}
                          </Form.Item>
                        </ContentHolder>
                      </Col>
                    </Row>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Actividad Laboral:
                      </Col>
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        <ContentHolder>
                          <Form.Item>
                            {getFieldDecorator('actividadLab', {
                              initialValue: actividadLab,
                              rules: [
                                {
                                  required: this.state.checkIndependiente,
                                  message: 'Favor de responder',
                                },
                              ],
                            })(
                              <Select
                                setFieldsValue={actividadLab}
                                onChange={handleChangeActividad}
                                style={{ minWidth: '120px' }}
                                disabled={this.state.independiente}
                              >
                                <Option value="Ejemplo1">Ejemplo 1</Option>
                                <Option value="Ejemplo2">Ejemplo 2</Option>
                                <Option value="Ejemplo3">Ejemplo 3</Option>
                              </Select>
                            )}
                          </Form.Item>
                        </ContentHolder>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </TabPane>

              <TabPane
                tab="Accionista"
                key="3"
                disabled={this.state.Accionista}
              >
                {/* Content of Tab Pane 3 */}
                <Row gutter={24} type="flex" align="middle">
                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Fecha de Alta:
                      </Col>
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        {/* <ContentHolder> */}
                          <Form.Item>
                            {/* {getFieldDecorator('fechaAlta', {
                              initialValue: fechaAlta,
                              rules: [
                                {
                                  required: this.state.checkAccionista,
                                  message: 'Favor de responder',
                                },
                              ],
                            })(
                              <DatePicker
                                onChange={handleChangeDateAlt}
                                placeholder="Fecha Alta"
                                disabled={this.state.Accionista}
                              />
                            )} */}
                            {getFieldDecorator('fechaAlta', {
                              rules: [
                                {
                                  required: this.state.checkAccionista,
                                  message:
                                    'Favor de responder',
                                },
                              ],
                            })(
                              <Fechas fecha={fechaAltaF}
                                      checkDate={this.checkDateAlta} 
                                      monthPicker = {false}/>
                            )}
                          </Form.Item>
                        {/* </ContentHolder> */}
                      </Col>
                    </Row>
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Actividad de la empresa:
                      </Col>
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        <ContentHolder>
                          <Form.Item>
                            {getFieldDecorator('actividadEmpresa', {
                              initialValue: actividadEmpresa,
                              rules: [
                                {
                                  required: this.state.checkAccionista,
                                  message: 'Favor de responder',
                                },
                              ],
                            })(
                              <Select
                                setFieldsValue={actividadEmpresa}
                                onChange={handleChangeActEmp}
                                style={{ width: '120px' }}
                                disabled={this.state.Accionista}
                              >
                                <Option value="Ejemplo1">Ejemplo 1</Option>
                                <Option value="Ejemplo2">Ejemplo 2</Option>
                                <Option value="Ejemplo3">Ejemplo 3</Option>
                              </Select>
                            )}
                          </Form.Item>
                        </ContentHolder>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row gutter={24} type="flex" align="middle">
                  <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    <Row type="flex" justify="space-around" align="middle">
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        Se encuentra dado de alta en hacienda con la misma
                        actividad:
                      </Col>
                      <Col
                        xs={24}
                        sm={24}
                        md={24}
                        lg={24}
                        xl={24}
                        style={colStyle}
                      >
                        <ContentHolder>
                          <Form.Item>
                            {getFieldDecorator('altaHaciendaAct', {
                              initialValue: altaHaciendaAct,
                              rules: [
                                {
                                  required: this.state.checkAccionista,
                                  message: 'Favor de responder',
                                },
                              ],
                            })(
                              <RadioGroup
                                onChange={onChangeAHAct}
                                // value={altaHaciendaAct}
                                disabled={this.state.Accionista}
                              >
                                <Radio value={11}>Si</Radio>
                                <Radio value={22}>No</Radio>
                              </RadioGroup>
                            )}
                          </Form.Item>
                        </ContentHolder>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </TabPane>
            </Tabs>
          </ContentHolder>
          {/* </Box> */}
        </Row>

        {/* <Button onClick={this.prueba1Men}>Prueba 1 Men</Button> */}

        <div>
          <TxtAling>
            <Row type="flex" justify="center" align="middle">
              <Col xs={24} sm={6} md={3} lg={3} xl={3}>
                <Form.Item>
                  <Button
                    disabled={true}
                    style={{ width: '100px', marginTop: 16 }}
                  >
                    Atras
                  </Button>
                </Form.Item>
              </Col>
              <Col xs={24} sm={6} md={3} lg={3} xl={3}>
                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                    style={{ width: '100px', marginTop: 16 }}
                  >
                    Siguiente
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </TxtAling>
        </div>
      </Form>
    );
  }
}

export default Form.create()(FirstStep);
