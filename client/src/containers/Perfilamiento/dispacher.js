import actions from './actions';

const dispatcher = dispatch => ({
  updateGeneralBalance: params => {
    dispatch(actions.updateGeneralBalance(params));
  },
});

export default dispatcher;
