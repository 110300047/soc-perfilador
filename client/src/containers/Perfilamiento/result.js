import React, { Component } from 'react';
import { Row, Col, Button, Card, InputNumber, } from 'antd';
import {
  Circle,
  IconCheck,
  StatusProfiling,
} from '../Preperfilamiento/preperfilamiento.style';

export default class ResultProfiling extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: 'negativo',
    };
  }

  render() {
    const { result } = this.state;
    const { makePostData, previousStep, ingresosTotales,  egresosTotales} = this.props;
    const colStyle = {
      marginBottom: '16px',
    };
    return (
      <div>
        <h1>Resultado Perfilamiento</h1>
        <Row type="flex" align="middle">
          <Col xs={18} sm={18} md={18} lg={18} xl={18}>
            <p>
              Acaba de terminar la precalificacion de la primera fase, sus
              resultados dieron{' '}
              <StatusProfiling status={result}>{result}</StatusProfiling>.
            </p>
            <p>
              Por favor oprima en <strong>Siguiente</strong> para continuar
            </p>

            <Row gutter={24} type="flex" align="middle" style={{ marginTop: '10px' }}>
              <Col xs={24} sm={12} md={6} lg={6} xl={6} style={colStyle}>
                Ingresos Totales Calculados:
                {/* <ContentHolder> */}
              </Col>
              <Col xs={24} sm={12} md={18} lg={18} xl={18} style={colStyle}>
                <InputNumber
                  style={{ width: '100px', marginLeft: 1 }}
                  value={ingresosTotales}
                  formatter={value =>
                    `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                  }
                  parser={value => value.replace(/\$\s?|(,*)/g, '')}
                  disabled={true}
                />
                {/* </ContentHolder> */}
              </Col>
            </Row>
            <Row gutter={24} type="flex" align="middle" >
              <Col xs={24} sm={12} md={6} lg={6} xl={6} style={colStyle}>
                Deudas por pagar:
                {/* <ContentHolder> */}
              </Col>
              <Col xs={24} sm={12} md={18} lg={18} xl={18} style={colStyle}>
                <InputNumber
                  style={{ width: '100px', marginLeft: 1 }}
                  value={egresosTotales}
                  formatter={value =>
                    `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                  }
                  parser={value => value.replace(/\$\s?|(,*)/g, '')}
                  disabled={true}
                />
                {/* </ContentHolder> */}
              </Col>
            </Row>
            {/* <hr style={{ width: '160px', marginRight: '450px'}}></hr> */}

            <Row gutter={24} type="flex" align="middle" >
              <Col xs={24} sm={12} md={6} lg={6} xl={6} >
                
              </Col>
              <Col xs={24} sm={12} md={18} lg={18} xl={18} >
                <hr style={{ width: '160px', marginLeft: '-30px'}}></hr>
              </Col>
            </Row>

            <Row gutter={24} type="flex" align="middle" >
              <Col xs={24} sm={12} md={6} lg={6} xl={6} style={colStyle}>
                Capacidad mensual maxima:
                {/* <ContentHolder> */}
              </Col>
              <Col xs={24} sm={12} md={18} lg={18} xl={18} style={colStyle}>
                <InputNumber
                  style={{ width: '100px', marginLeft: 1 }}
                  value={ingresosTotales - egresosTotales}
                  formatter={value =>
                    `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                  }
                  parser={value => value.replace(/\$\s?|(,*)/g, '')}
                  disabled={true}
                />
                {/* </ContentHolder> */}
              </Col>
            </Row>
            <Row gutter={24} type="flex" align="middle" >
              <Col xs={24} sm={12} md={6} lg={6} xl={6} style={colStyle}>
                Mensualidad máxima de hipoteca:
                {/* <ContentHolder> */}
              </Col>
              <Col xs={24} sm={12} md={18} lg={18} xl={18} style={colStyle}>
                <InputNumber
                  style={{ width: '100px', marginLeft: 1 }}
                  value={ingresosTotales}
                  formatter={value =>
                    `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                  }
                  parser={value => value.replace(/\$\s?|(,*)/g, '')}
                  disabled={true}
                />
                {/* </ContentHolder> */}
              </Col>
            </Row>
            <Row gutter={24} type="flex" align="middle" >
              <Col xs={24} sm={12} md={6} lg={6} xl={6} style={colStyle}>
                Credito máximo:
                {/* <ContentHolder> */}
              </Col>
              <Col xs={24} sm={12} md={18} lg={18} xl={18} style={colStyle}>
                <InputNumber
                  style={{ width: '100px', marginLeft: 1 }}
                  value={ingresosTotales}
                  formatter={value =>
                    `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                  }
                  parser={value => value.replace(/\$\s?|(,*)/g, '')}
                  disabled={true}
                />
                {/* </ContentHolder> */}
              </Col>
            </Row>

          </Col>
          <Col xs={6} sm={6} md={6} lg={6} xl={6}>
            <Card style={{ width: 100 }}>
              <Circle status={result === 'positivo' ? 'positivo' : 'apagado'}>
                <IconCheck type="check" />
              </Circle>{' '}
              <Circle status={result === 'medio' ? 'medio' : 'apagado'}>
                <IconCheck type="check" />
              </Circle>{' '}
              <Circle status={result === 'negativo' ? 'negativo' : 'apagado'}>
                <IconCheck type="close" />
              </Circle>{' '}
            </Card>
          </Col>          
        </Row>
        
        <div>
          
          <Row type="flex" justify="center" align="middle">
            <Col xs={6} sm={6} md={6} lg={6} xl={6}>
              <Button onClick={previousStep}>Salir</Button>
            </Col>
            <Col xs={6} sm={6} md={6} lg={6} xl={6}>
              <Button type="primary" onClick={makePostData}>
                terminar
              </Button>
              
            </Col>
          </Row>
        </div>
        
      </div>
    );
  }
}
