import { createActions } from "ractionx";

const prefix = "@soc/api/redux/perfilamiento";

export const types = ["UPDATE_GENERAL_BALANCE"];

const { updateGeneralBalance } = createActions(prefix, types);

export default { updateGeneralBalance };
