import { call, takeLatest } from "redux-saga/effects";
import actions from "./actions";
import api from "../../services/api";
import AlertDialog from "../../components/modalDialog";

export function* updateDataSaga({ payload }) {
  try {
    yield call(api.perfilamiento.updateData, { payload });
    AlertDialog("SUCCESS", "Actualización exitosa");
  } catch (e) {
    AlertDialog("Error", "Error al actualizar la información");
  }
}

export default function* SocSagas() {
  yield takeLatest(actions.updateGeneralBalance.type, updateDataSaga);
}
