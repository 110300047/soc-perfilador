import Auth from "./auth/reducer";
import App from "./app/reducer";
import ThemeSwitcher from "./themeSwitcher/reducer";
import LanguageSwitcher from "./languageSwitcher/reducer";
import preperfilRedux from "./preperfilamiento/reducer";

export default {
  Auth,
  App,
  ThemeSwitcher,
  LanguageSwitcher,
  preperfilRedux
};
