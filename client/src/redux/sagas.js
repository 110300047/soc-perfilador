import { all } from "redux-saga/effects";
import authSagas from "./auth/saga";
import SocPrePerfilSagas from "./preperfilamiento/sagas";
import SocPerfilSagas from "./perfilamiento/sagas";
import containersSagas from "../containers/sagas";
import pagesSagas from "./pages/sagas";

export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    SocPrePerfilSagas(),
    SocPerfilSagas(),
    containersSagas(),
    pagesSagas()
  ]);
}
