import actions from './actions';

const initialState = {};

function preperfilRedux(state = initialState, { type, payload }) {
  switch (type) {
    case actions.userSucessfull.type:
      console.log(payload);
      return { ...state, userStatus: payload };

    default:
      return state;
  }
}

export default preperfilRedux;
