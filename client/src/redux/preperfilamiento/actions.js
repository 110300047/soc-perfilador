import { createActions } from 'ractionx';

const prefix = '@soc/api/redux/preperfil';

export const types = [
  'CREATE_USER',
  'UPDATE_GENERAL_DATA',
  'PRODUCTO_UPDATE',
  'USER_SUCESSFULL',
  'PROSPECTO_UPDATE',
];

const {
  createUser,
  updateGeneralData,
  productoUpdate,
  userSucessfull,
  prospectoUpdate,
} = createActions(prefix, types);

export default {
  createUser,
  updateGeneralData,
  productoUpdate,
  userSucessfull,
  prospectoUpdate,
};
