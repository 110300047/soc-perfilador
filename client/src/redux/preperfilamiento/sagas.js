import { call, takeLatest, put } from "redux-saga/effects";
import actions from "./actions";
import api from "../../services/api";
import AlertDialog from "../../components/modalDialog";

export function* createUserSaga({ payload }) {
  try {
    const {
      data: { status }
      // data: { status, token }
    } = yield call(api.preperfil.createUser, { payload });
    yield put(actions.userSucessfull(status));
    // yield put(actions.userSucessfull(status, token));
    AlertDialog("SUCCESS", "Solicitud exitosa");
  } catch (e) {
    if (e.response) {
      const {
        response: { data }
      } = e;

      AlertDialog("Error", data.message);
    } else AlertDialog("Error", "Error de conección");
  }
}

export function* updateDataSaga({ payload }) {
  try {
    yield call(api.preperfil.complementData, { payload });
    AlertDialog("SUCCESS", "Actualización exitosa");
  } catch (e) {
    AlertDialog("Error", "Error al actualizar la información");
  }
}

export function* productoUpdateSaga({ payload }) {
  try {
    yield call(api.preperfil.productoUpdate, { payload });
    AlertDialog("SUCCESS", "Actualización exitosa");
  } catch (e) {
    AlertDialog("Error", "Error al actualizar la información");
  }
}

export function* prospectoUpdateSaga({ payload }) {
  try {
    yield call(api.preperfil.prospectoUpdate, { payload });
    AlertDialog("SUCCESS", "Prospecto  exitoso");
  } catch (e) {
    AlertDialog("Error", "Error al ingresar prospecto");
  }
}

export default function* SocSagas() {
  yield takeLatest(actions.createUser.type, createUserSaga);
  yield takeLatest(actions.updateGeneralData.type, updateDataSaga);
  yield takeLatest(actions.productoUpdate.type, productoUpdateSaga);
  yield takeLatest(actions.prospectoUpdate.type, prospectoUpdateSaga);
}
