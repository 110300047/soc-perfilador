import { createActions } from "ractionx";

const prefix = "@soc/api/page/perfil";

export const types = ["SIGN_UP"];

const { signUp } = createActions(prefix, types);

export default { signUp };
