import { call, takeLatest } from "redux-saga/effects";
import actions from "./actions";
import api from "../../services/api";
import AlertDialog from "../../components/modalDialog";

export function* signUpSaga({ payload }) {
  try {
    yield call(api.page.signup, { payload });
    AlertDialog("SUCCESS", "Cuenta creada");
  } catch (e) {
    AlertDialog("ERROR", "fallo al crear la cuenta");
  }
}

export default function* SocSagas() {
  yield takeLatest(actions.signUp.type, signUpSaga);
}
