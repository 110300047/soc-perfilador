import React, { Component } from 'react';
import { Button, Drawer } from 'antd';
import { Link } from 'react-router-dom';
import LogoSoc from '../../image/logo_soc.svg';
import { Body, LogoStyle } from './content.style';
import LeftMenu from '../navbar/leftmenu';
import RightMenu from '../navbar/RightMenu';

class Navbar extends Component {
  state = {
    visible: false,
  };
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    return (
      <Body>
        <nav className="menuBar">
          <div className="logo">
            <Link to="/">
              <LogoStyle src={`${LogoSoc}`} />
            </Link>
          </div>
          <div className="menuCon">
            <div className="leftMenu">
              <LeftMenu />
            </div>
            <div className="rightMenu">
              <RightMenu />
            </div>
            <Button
              className="barsMenu"
              type="primary"
              onClick={this.showDrawer}
            >
              <span className="barsBtn" />
            </Button>
            <Drawer
              title="Menu"
              placement="right"
              closable={false}
              onClose={this.onClose}
              visible={this.state.visible}
            >
              <LeftMenu />
              <RightMenu />
            </Drawer>
          </div>
        </nav>
      </Body>
    );
  }
}

export default Navbar;
