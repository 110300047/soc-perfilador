import styled from 'styled-components';
import { Button } from 'antd';

export const TextSOC = styled.div`
  position: absolute;
  width: 1140px;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);

  @media only screen and (max-width: 1200px) {
    width: 100%;
    padding: 0 2%;
    font-size: 18px;
  }
  @media only screen and (max-width: 600px) {
    font-size: 15px;
  }
  @media only screen and (max-width: 470px) {
    font-size: 13px;
  }
  @media only screen and (max-width: 350px) {
    font-size: 10px;
  }
`;

export const Body = styled.div`
  font-weight: 300px;
  text-transform: uppercase;

  .ant-menu {
    background: none;
  }
  .menuBar {
    padding: 0 20px;
    overflow: auto;
  }
  .logo {
    width: 150px;
    float: left;
  }
  .ant-menu-horizontal .ant-menu-item a {
    color: #fff;
    font-size: 120%;
  }
  .menuCon {
    width: calc(100% - 200px);
    float: left;
  }
  .menuCon .ant-menu-item {
    padding: 0px 5px;
  }
  .menuCon .ant-menu-submenu-title {
    padding: 10px 20px;
  }

  .menuCon .ant-menu-item a,
  .menuCon .ant-menu-submenu-title a {
    padding: 10px 15px;
  }
  .menuCon .ant-menu-horizontal {
    border-bottom: none;
  }
  .menuCon .leftMenu {
    float: left;
    padding-top: 10px;
  }
  .menuCon .rightMenu {
    float: right;
    padding-top: 10px;
  }
  .barsMenu {
    float: right;
    height: 32px;
    padding: 6px;
    margin-top: 8px;
    display: none;
    background: none;
    border: none;    
  }
  .barsBtn {
    display: block;
    width: 20px;
    height: 2px;
    background: #fdfdfd;
    position: relative;
  }
  .barsBtn:after,
  .barsBtn:before {
    content: attr(x);
    width: 20px;
    position: absolute;
    top: -6px;
    left: 0;
    height: 2px;
    background: #fdfdfd;
  }
  .barsBtn:after {
    top: auto;
    bottom: -6px;
  }
  .ant-drawer-body {
    padding: 0;
  }
  .ant-drawer-body .ant-menu-horizontal > .ant-menu-item,
  .ant-drawer-body .ant-menu-horizontal > .ant-menu-submenu {
    display: inline-block;
    width: 100%;
  }
  .ant-drawer-body .ant-menu-horizontal {
    border-bottom: none;
  }
  .ant-drawer-body .ant-menu-horizontal > .ant-menu-item:hover {
    border-bottom-color: transparent;
  }

  @media (max-width: 767px) {
    .barsMenu {
      display: inline-block;
    }
    .leftMenu,
    .rightMenu {
      display: none;
    }
    .logo a {
      margin-left: -20px;
    }
    .menuCon .ant-menu-item,
    .menuCon .ant-menu-submenu-title {
      padding: 1px 20px;
    }
    .logo a {
      padding: 10px 20px;
    }
  }
`;

export const TitleContent = styled.h1`
  margin-top: 0;
  margin-bottom: 20px;
  color: #fff;
  font-size: 240%;
  word-spacing: 4px;
  letter-spacing: 1px;
  font-weight: 300;
  text-transform: uppercase;
`;

export const LogoStyle = styled.img`
  display: inline-block;
  padding: 19px 20px;
  float: none;

  @media only screen and (max-width: 470px) {
    height: 80px;
  }
  @media only screen and (max-width: 250px) {
    margin-top: 0px;
    height: 40px;
  }
`;

export const ButtonStyle = styled(Button)`
  word-spacing: 4px;
  letter-spacing: 1px;
  text-transform: uppercase;
  width: auto;
  margin-right: 50px;
`;
