import React from 'react';
import { Link } from 'react-router-dom';
import { TextSOC, TitleContent, ButtonStyle } from './content.style';

const ButtonTest = () => (
  <TextSOC>
    <TitleContent>
      Asesoría sin costo.
      <br />
      Para obtener crédito Hipotecario.
    </TitleContent>

    <Link to={`/simulador`}>
      <ButtonStyle style={{    backgroundImage: 'linear-gradient( to right,rgb(63, 179, 74),rgb(13, 130, 41) )',color: '#fff',border: 'none'}}>Haz tu prueba</ButtonStyle>
    </Link>
    <ButtonStyle type="primary" href="http://www.socasesores.com/">Sitio Web</ButtonStyle>
  </TextSOC>
);
export default ButtonTest;
