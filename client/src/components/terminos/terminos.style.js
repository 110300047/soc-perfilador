import styled from 'styled-components';
import { Modal } from 'antd';

export const ModalInfo = styled(Modal)`
  width: 80%;
`;
