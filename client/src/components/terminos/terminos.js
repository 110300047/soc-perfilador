import { Row, Button } from 'antd';
import React from 'react';

const TerminosCondiciones = ({ handleOk }) => (
  <div>
    <h1>Términos y condiciones</h1>
    <h2>1. INTRODUCCIÓN AL SERVICIO Y ACEPTACIÓN DE LAS CONDICIONES DE USO</h2>
    <h4>1.1 Introducción</h4>
    <p>
      Sinergia SOC S.A de C.V. (“SOC”, nosotros), una compañía debidamente
      constituida e incorporada a las leyes de los Estados Unidos Mexicanos,
      ofrece el servicio de asesoría hipotecaria integral (asesoría de
      especialistas en los productos, procesos, políticas y requisitos de las
      opciones financieras más importantes del mercado), el cual brinda una
      serie de programas (simuladores) y otros contenidos audiovisuales de
      manera conjunta el (“Contenido”) a través de nuestras páginas que integran
      el portal de www.socasesores.com (“el sitio”) nuestra herramienta para ver
      el contenido y ciertas aplicaciones que le permiten acceder al mismo (de
      manera conjunta, el Servicio” o los “Servicios” ). Estos términos y
      condiciones detallados, que incluyen la política de Privacidad que se
      encuentran en http://www.socasesores.com/aviso.html.
    </p>
    <h4>1.2 Aceptación de las Condiciones de Uso</h4>
    <p>
      Para acceder y disfrutar de los Servicios, debe aceptar y en todo momento
      seguir las disposiciones que se establecen en los términos. Al usar los
      Servicios (incluyendo al acceso del contenido), usted acepta cumplir con
      las condiciones de Uso, por lo que pedimos dedique un tiempo a revisar
      cuidadosamente y si no está de acuerdo con las mismas, deberá abstenerse
      de acceder a utilizar el sitio.
    </p>
    <h4>1.3 El Servicio</h4>
    <p>
      Ofrecemos los servicios de asesoría hipotecaria integral
      <strong>
        {' '}
        (asesoría de especialistas en los productos, procesos, políticas y
        requisitos de las opciones financieras más importantes del mercado)
      </strong>
      . Las referencias a los “Servicios” o el “Servicio” a lo largo de estas
      condiciones.
    </p>
    <h2>2. MODIFICACIÓN DE LAS CONDICIONES DE USO POR “SOC”</h2>
    <p>
      “SOC” puede modificar las condiciones de Uso (incluyendo la política de
      privacidad) en cualquier momento, a su discreción, las cuales surtirían
      efectos en el momento en que se publiquen las Condiciones de Uso
      modificadas en nuestro sitio. La versión más actual de las Condiciones de
      Uso prevalecerá sobre tosas las versiones previstas.
    </p>
    <h2>3. ACCESO Y USO DE LOS SERVICIOS</h2>
    <h4>3.1 El contenido</h4>
    <p>
      “SOC” no, promueve, aprueba o permite se copien, programas u otros
      contenidos entregados digitalmente, ni otras actividades delictivas. Usted
      no puede, ya sea directamente o con el uso de dispositivos, software,
      sitios de internet, servicios basados en web u otros medios. (a) retirar,
      alterar, eludir, evitar, o interferir avisos, de derechos de autor, marcas
      u otro tipo de aviso de propiedad insertados en el Contenido, o mecanismos
      de administración de derechos digitales , dispositivos u otras
      protecciones de contenido o medidas de control de acceso del contenido, ni
      (b) copiar, descargar (excepto a través de la memoria de almacenamiento
      personal de la página necesaria para uso personal, o como expresamente se
      permite en las Condiciones de uso ) modificar, distribuir , transmitir,
      mostrar, efectuar, reproducir, exhibir , duplicar, publicar, licenciar.
      Crear obras derivadas u ofrecer para la venta el contenido u otra
      información contenida u obtenida de o a través del servicio, sin nuestro
      consentimiento expreso por escrito. No puede incorporar el contenido, ni
      hacer streming o retransmitirlo en aplicaciones de hardware o de software,
      ni ponerlo a disposición a través de enmarcados o vínculos en línea, a
      menos de que “SOC” expresamente lo permita por escrito. Además, no puede
      crear, recrear, distribuir o publicar un índice de la parte sustancial del
      contenido, a menos de que “SOC” expresamente lo autorice; tampoco puede
      crear un negocio utilizando el contenido, ya sea con fines de lucro o no.
      el contenido al que aplican estas restricciones incluye, entre otros,
      textos, gráficos, configuraciones, interfaces, logotipos, fotografías,
      materiales de audio y video y fonogramas. Así mismo, se le prohíbe
      estrictamente crear obras derivadas o materiales que provengan o se basen
      en cualquier firma en el contenido, incluyendo montajes, fondos de
      escritorio, protector de pantalla, tarjetas de saludos.
    </p>
    <h2>4. COMO FUNCIONA EL SERVICIO</h2>
    <h4>4.1 Descripción General</h4>
    <p>
      “SOC” es un servicio que ofrece al público acceso al portal por internet
      para ofrecer asesoría hipotecaria. Nos reservamos el derecho, a nuestra
      absoluta y exclusiva discreción, de hacer cambios en cualquier momento y
      sin notificación, sobre la forma en que manejamos el servicio, por lo que
      las descripciones de cómo funciona el servicio no debe considerarse como
      una declaración u obligación respecto a cómo funcionara siempre.
    </p>
    <h2>5.- PROPIEDAD Y DERECHOS DE PROPIEDAD INTELECTUAL.</h2>
    <p>
      El servicio , incluyendo todo el contenido, software, interfaces de
      usuario , textos gráficos , logotipos, diseños, fotografías, iconos de
      botones, imágenes, videoclips, y audio, descargas digitales, compilaciones
      de todos y software que se incluyan en “sitio-web” y/o que se entregan al
      usuario como parte del servicio, son bienes de “SOC” o de sus licenciantes
      o proveedores de contenido, y están protegidos por leyes de las Entidades
      Federativas e Internacionales , de derechos de autor , marcas, secretos
      industriales, o de propiedad intelectual de otro tipo . Toda la propiedad
      intelectual de “SOC” que sea una marca, logotipo o marca de servicio
      también es una marca registrada o no registrada de “SOC” o de otros. Se
      prohíbe estrictamente el uso de la PI de “SOC” excepto de conformidad con
      estas condiciones de usos, sin el permiso por escrito de los dueños de
      dicha Propiedad Intelectual. También se le informa que “SOC” ejercerá con
      firmeza sus derechos de propiedad intelectual en la medida más amplia
      permitida por la ley.
    </p>
    <h2>
      6.- RESPONSABILIDADES QUE LE CORRESPONDEN Y RESTRICCIONES EN EL USO.
    </h2>
    <p>
      Las siguientes Normas de conducta se aplican al sitio y a los Servicios y
      usted se obliga a no utilizar el sitio y Servicios de manera que:
    </p>

    <ul>
      <li>
        Violen los derechos de otros, incluyendo derechos de patentes, marcas
        secretos industriales, derechos de autor, privacidad, publicidad u
        otros, derechos de propiedad intelectual.
      </li>
      <li>
        Usen, transfieran, distribuyan o dispongan de la información que
        contiene el Servicio en cualquier forma que podría competir con el
        negocio de “SOC” o cualquiera de sus filiales.
      </li>
      <li>
        Resulten en el envío de correo electrónico no solicitado o spam, o en
        que se recopile información acerca de usuarios con el propósito de
        enviarles correo electrónico no solicitado o spam; introduzcan virus o
        códigos, archivos o programas de cómputo de otro tipo que interrumpan,
        destruyan o limiten la funcionalidad de software o hardware de cualquier
        equipo de telecomunicación.
      </li>
      <li>
        Dañen, inhabiliten, sobrecarguen, perjudiquen u obtengan acceso no
        autorizado a los Servicios, incluyendo Servidores de “SOC”, redes de
        cómputo o cuentas de usuarios.
      </li>
      <li>
        Retiren, modifiquen, inhabiliten, bloqueen, oculten o de otra forma
        afecten la publicidad relacionada con los Servicios (incluyendo el
        Contenido).
      </li>
      <li>
        Utilicen los Servicios para publicitar o promover servicios que “SOC” no
        haya aprobado por escrito previamente.
      </li>
      <li>
        Recaben información personal identificable violando la Política de
        Privacidad de “SOC”.
      </li>
      <li>
        Alienten conductas que constituirían un delito o den lugar a
        responsabilidad civil.
      </li>
      <li>
        Violen estas Condiciones de Uso, o lineamientos o políticas publicados
        por “SOC”.
      </li>
      <li>
        Interfieran con el uso de otro usuario o su disfrute del Servicio.
      </li>
      <li>Intenten realizar cualquiera de las acciones anteriores.</li>
    </ul>
    <p>
      No podemos asegurar –y no lo aseguramos– que otros usuarios cumplen o
      cumplirán con estas normas de conducta u otras disposiciones de las
      Condiciones de Uso y, en lo que compete a usted y a nosotros, en este acto
      usted asume todos los riesgos de daños o perjuicios que resulten de dicha
      falta de cumplimiento.
    </p>
    <h2>7.- PRIVACIDAD Y PREFERENCIAS DE COMUNICACIONES</h2>
    <p>
      Para obtener información acerca de uso de su información personal, o de la
      Política de Privacidad. La Política de Privacidad se incorpora en estas
      Condiciones de Uso y es parte de los mismos; por lo tanto, al convenir en
      las Condiciones de Uso, también acuerda que el uso de los Servicios se
      regirá por la Política de Privacidad vigente en la fecha de uso. La
      información personal proporcionada a través del Servicio está sujeta a
      nuestra Política de Privacidad.
    </p>
    <h2>
      8.- EXTENCIÓN DE GARANTÍAS, LIMITACIÓN DE RESPONSABILIDAD E INDEMNIZACIÓN
    </h2>
    <p>
      USTED ACUERDA QUE EL USO DE LOS SERVICIOS ES A SU PROPIO RIESGO. LOS
      SERVICIOS, INCLUYENDO EL SITIO, EL CONTENIDO, EL SOFTWARE, EL MATERIAL DEL
      USUARIO Y CUALQUIER OTRO MATERIAL QUE CONTENGA O SE PROPORCIONE EN EL
      SITIO, SE PROPORCIONAN “EN LA CONDICIÓN QUE SE ENCUENTRA” Y, EN LA MEDIDA
      MÁS AMPLIA PERMITIDA POR LA LEY, SE PROPORCIONAN SIN GARANTÍAS DE NINGUNA
      CLASE, YA SEA EXPRESAS O IMPLÍCITAS. SIN LIMITAR LO ANTERIOR, “SOC" NO
      EMITE NINGUNA GARANTÍA DE IDONEIDAD PARA UN FIN ESPECÍFICO, TITULARIDAD,
      COMERCIABILIDAD, INTEGRIDAD, DISPONIBILIDAD, SEGURIDAD, COMPATIBILIDAD O
      INEXISTENCIA DE VIOLACIONES; O QUE LOS SERVICIOS SERÁN ININTERRUMPIDOS O
      QUE NO TENDRÁN VIRUS U OTROS COMPONENTES DAÑINOS, O QUE SERÁN PRECISOS,
      SIN ERRORES O CONFIABLES. EN NINGÚN CASO, “SOC”, SUS AFILIADAS,
      SUBSIDIARIAS, EMPLEADOS, AGENTES Y PROVEEDORES (INCLUYENDO DISTRIBUIDORES
      Y LICENCIANTES DE CONTENIDO), SERÁN RESPONSABLES DE DAÑOS Y PERJUICIOS
      DIRECTOS, INDIRECTOS, PUNITIVOS, INCIDENTALES, ESPECIALES, RESULTANTES O
      DE OTRA NATURALEZA, QUE RESULTEN O EN CUALQUIER FORMA SE RELACIONEN CON EL
      USO DE LOS SERVICIOS DE “SOC”.
    </p>
    <h2>
      9.- NOTIFICACIÓN Y PROCEDIMIENTO PARA PRESENTAR RECLAMACIONES DE
      VIOLACIONES DE DERECHO.
    </h2>
    <p>
      Si usted considera que el Contenido, el Material del Usuario o el material
      de otro tipo que se proporciona a través del Servicio, incluso a través de
      un vínculo, viola sus derechos de autor, deberá notificar a “SOC” la
      reclamación de violación de acuerdo con el procedimiento que se prevé más
      adelante. Procesaremos todas las notificaciones de presuntas violaciones
      que “SOC” reciba y tomaremos las medidas apropiadas de acuerdo con las
      leyes de propiedad intelectual aplicables. La notificación de una
      reclamación de violación de derecho de autor deberá enviarse por correo
      electrónico al representante de derechos de autor de “SOC”, Andrea Zamora
      Reyes. También puede contactarnos por correo azamora@socasesores.com.
    </p>
    <p>
      Para que sea válida, la notificación debe efectuarse por escrito e incluir
      la siguiente información: (a) firma electrónica o física de la persona
      autorizada a actuar por cuenta del propietario de un derecho de autor
      exclusivo; (b) descripción de la obra protegida por derechos de autor que
      usted reclama se ha violado; (c) una descripción de la ubicación en el
      Servicio del material que usted reclama que se está violando que sea
      razonablemente suficiente para permitir a “SOC” identificar y ubicar el
      material; (d) cómo puede “SOC” ponerse en contacto con usted, por ejemplo
      su domicilio, número telefónico y correo electrónico; (e) declaración por
      usted de que cree de buena fe que el uso en controversia no está
      autorizado por el propietario del derecho de autor, su representante o la
      ley; y (f) declaración por usted de que la información anterior en la
      notificación es exacta, que usted es el propietario del derecho de autor o
      el propietario de un derecho exclusivo sobre el material, o que está
      autorizado a actuar por cuenta de este. Los correos electrónicos enviados
      a: azamora@socasesores.com, para fines distintos a comunicar violaciones
      de derechos de autor no serán respondidos.
    </p>
    <h2>10.- JURISDICCIÓN.</h2>
    <p>
      Estos términos y condiciones están regidos por, e interpretados de acuerdo
      a, las leyes y tribunales competentes en el Distrito Federal, sin importar
      el lugar en donde llegase a generarse la disputa que resulte en relación
      con estos términos y condiciones o cualquier disputa que resulte en
      relación con el sitio web- servicio ya sea en contrato o perjuicio o de
      otra manera, las autoridades locales no tendrán jurisdicción alguna.
    </p>
    <h2>11.- DISPOSICIONES GENERALES.</h2>
    <h4>11.1 Comunicaciones Electrónicas</h4>
    <p>
      Al utilizar el Servicio, usted aprueba recibir comunicaciones electrónicas
      de “SOC”, las cuales pueden incluir notificaciones, correos electrónicos
      de confirmación e información de operaciones de otro tipo, así como
      información concerniente o relativa a nuestro Servicio, y puede incluir
      boletines y comunicaciones promocionales de nosotros, sino han sido
      canceladas. Usted acepta que las notificaciones, acuerdos, divulgaciones u
      otras comunicaciones que le enviemos electrónicamente cumplirán con las
      disposiciones legales sobre comunicaciones, incluso que sean por escrito;
      asimismo, acuerda en actualizar su información personal inmediatamente de
      que haya un cambio en la dirección de su correo electrónico.
    </p>

    <Row type="flex" justify="end">
      <Button type="primary" onClick={handleOk}>
        Aceptar
      </Button>
    </Row>
  </div>
);

export default TerminosCondiciones;
