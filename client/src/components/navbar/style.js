import styled from 'styled-components';

export const Body = styled.div`
  font-weight: 300px;
  text-transform: uppercase;

  .ant-menu {
    background: none;
  }
  .menuBar {
    padding: 0 20px;
    overflow: auto;
    background: #fff;
    border-bottom: solid 3px #265b8e;
    border-top: solid 3px #265b8e;
  }
  .menuBar button{
    text-shadow: none;
    box-shadow: none;
    width: 2.5em;
    left: 2em;
  }
  .logo {
    width: 150px;
    float: left;
  }
  .ant-menu-horizontal .ant-menu-item a {
    color: #2b6599;
    font-size: 120%;
  }
  .menuCon {
    width: calc(100% - 200px);
    float: left;
  }
  .menuCon .ant-menu-item {
    padding: 0px 5px;
  }
  .menuCon .ant-menu-submenu-title {
    padding: 10px 20px;
  }

  .menuCon .ant-menu-item a,
  .menuCon .ant-menu-submenu-title a {
    padding: 10px 15px;
  }
  .menuCon .ant-menu-horizontal {
    border-bottom: none;
  }
  .menuCon .leftMenu {
    float: left;
    padding-top: 10px;
  }
  .menuCon .rightMenu {
    float: right;
    padding-top: 10px;
  }
  .barsMenu {
    float: right;
    height: 32px;
    padding: 0px;
    margin-top: 1.5em;
    display: none;
    background: none;
  }
  .barsBtn {
    display: block;
    width: 20px;
    height: 2px;
    background: #265b8e;
    position: relative;
    left: 0px;
  }
  .barsBtn:after,
  .barsBtn:before {
    content: attr(x);
    width: 20px;
    position: absolute;
    top: -6px;
    left:0 ; 
    height: 2px;
    background: #265b8e;
  }
  .barsBtn:after {
    top: auto;
    bottom: -6px;
  }
  .ant-drawer-body {
    padding: 0;
  }
  .ant-drawer-body .ant-menu-horizontal > .ant-menu-item,
  .ant-drawer-body .ant-menu-horizontal > .ant-menu-submenu {
    display: inline-block;
    width: 100%;
  }
  .ant-drawer-body .ant-menu-horizontal {
    border-bottom: none;
  }
  .ant-drawer-body .ant-menu-horizontal > .ant-menu-item:hover {
    border-bottom-color: transparent;
  }

  @media (max-width: 767px) {
    .barsMenu {
      display: inline-block;
    }
    .leftMenu,
    .rightMenu {
      display: none;
    }
    .logo a {
      margin-left: -20px;
    }
    .menuCon .ant-menu-item,
    .menuCon .ant-menu-submenu-title {
      padding: 1px 20px;
    }
    .logo a {
      padding: 10px 20px;
    }
  }
`;
