import React, { Component } from 'react';
import LeftMenu from './leftmenu';
import RightMenu from './RightMenu';
import { Drawer, Button } from 'antd';
import { Body } from './style';
import LogoSoc from '../../image/logo_soc.svg';
import { LogoStyle } from '../content/content.style';
import { Link } from 'react-router-dom';
import '../../containers/Preperfilamiento/style.css';
class Navbar extends Component {
  state = {
    visible: false,
  };
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    return (
      <Body>
        <nav className="menuBar ">
          <div className="logo">
            <Link to="/">
              <LogoStyle src={`${LogoSoc}`} />
            </Link>
          </div>
          <div className="menuCon">
            <div className="leftMenu">
              <LeftMenu />
            </div>
            <div className="rightMenu">
              <RightMenu />
            </div>
            <Button
              className="barsMenu"
              type="primary"
              onClick={this.showDrawer}
            >
              <span className="barsBtn" />
            </Button>
            <Drawer
              title="Basic Drawer"
              placement="right"
              closable={false}
              onClose={this.onClose}
              visible={this.state.visible}
            >
              <LeftMenu />
              <RightMenu />
            </Drawer>
          </div>
        </nav>
      </Body>
    );
  }
}

export default Navbar;
