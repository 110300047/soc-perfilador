import React, { Component } from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';

// const SubMenu = Menu.SubMenu;
// const MenuItemGroup = Menu.ItemGroup;

class RightMenu extends Component {
  render() {
    return (
      <Menu mode="horizontal">
        <Menu.Item>
          <Link to="/signin">Iniciar sesion</Link>
        </Menu.Item>
      </Menu>
    );
  }
}

export default RightMenu;
