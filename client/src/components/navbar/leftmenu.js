import React, { Component } from 'react';
import { Menu } from 'antd';
// const SubMenu = Menu.SubMenu;
// const MenuItemGroup = Menu.ItemGroup;

class LeftMenu extends Component {
  render() {
    return (
      <Menu mode="horizontal">
        <Menu.Item>
          <a href="">Inicio</a>
        </Menu.Item>
        <Menu.Item>
          <a href="">Productos</a>
        </Menu.Item>
        <Menu.Item>
          <a href="">Sucursales</a>
        </Menu.Item>
      </Menu>
    );
  }
}

export default LeftMenu;
