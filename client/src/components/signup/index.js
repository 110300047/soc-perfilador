import React, { Component } from "react";
import { Form, Input, Checkbox, Button, Row, Col } from "antd";
import { PhoneInput } from "../preperfilamiento/preperfilamiento.style";
import { Link } from "react-router-dom";
import IntlMessages from "../utility/intlMessages";
import {
  ModalTerm,
  LinkText
} from "../../components/preperfilamiento/preperfilamiento.style";
import TerminosCondiciones from "../../components/terminos/terminos";
import { LogoStyle } from "../content/content.style";
import LogoSoc from "../../image/logo_soc.svg";
class SignUpForm extends Component {
  state = {
    confirmDirty: false,
    visibleTerm: false
  };

  modal = () => {
    this.setState({
      visibleTerm: true
    });
  };

  handleOk = e => {
    this.setState({
      visibleTerm: false
    });
  };

  handleSubmit = e => {
    const { createAcount } = this.props;
    console.log("user");
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        createAcount();
      }
    });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("password")) {
      callback("no coincide con la contraseña!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmPass) {
      form.validateFields(["confirmpassword"], { force: true });
    }
    callback();
  };

  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      onChangeName,
      onChangeFirstSurname,
      onChangeSecondSurname,
      onChangeEmail,
      onChangePassword,
      onChangeCellPhone
    } = this.props;
    return (
      <div>
        <Form onSubmit={this.handleSubmit} className="socSinup">
          <div className="isoSignInPage align-flex-xy-center">
            <div className="isoLoginContent">
              <div className="align-flex-xy-center">
                <div className="logoContainerSig">
                  <LogoStyle src={`${LogoSoc}`} />
                </div>
              </div>

              <div className="isoSignInForm">
                <Row gutter={8}>
                  <Col xs={24} sm={24}>
                    <Form.Item>
                      {getFieldDecorator("name", {
                        rules: [
                          {
                            required: true,
                            message: "Ingrese su nombre(s)!"
                          }
                        ]
                      })(
                        <Input
                          onchance={onChangeName}
                          size="large"
                          placeholder="Nombre(s)"
                        />
                      )}
                    </Form.Item>
                  </Col>
                </Row>
                <Row gutter={8}>
                  <Col xs={24} sm={12}>
                    
                    <Form.Item>
                      {getFieldDecorator("firstSurname", {
                        rules: [
                          {
                            required: true,
                            message: "Ingresa primer apellido!"
                          }
                        ]
                      })(
                        <Input
                          onchance={onChangeFirstSurname}
                          size="large"
                          placeholder="Primer apellido"
                        />
                      )}
                    </Form.Item>
                  </Col>

                  <Col xs={24} sm={12}>
                    <Form.Item>
                      <Input
                          onchance={onChangeSecondSurname}
                          size="large"
                          placeholder="Segundo Apellido"
                        />
                    </Form.Item>
                  </Col>
                </Row>

                <Row gutter={8}>
                  <Col xs={24} sm={12}>
                    <Form.Item>
                      {getFieldDecorator("email", {
                        rules: [
                          {
                            required: true,
                            message: "Ingresa un correo electrónico!"
                          }
                        ]
                      })(
                        <Input
                          onchance={onChangeEmail}
                          size="large"
                          placeholder="Correo"
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={12}>
                    <Form.Item>
                      {getFieldDecorator("phone", {
                        rules: [
                          {
                            required: true,
                            message: "Ingresa un numero telefónico!"
                          }
                        ]
                      })(
                        <PhoneInput
                          format="(###) ###-####"
                          onValueChange={onChangeCellPhone}
                          mask="_"
                          placeholder="Numero de teléfono"
                        />
                      )}
                    </Form.Item>
                  </Col>
                </Row>
                <Row gutter={8}>
                  <Col xs={24} sm={12}>
                    <Form.Item>
                      {getFieldDecorator("password", {
                        rules: [
                          {
                            required: true,
                            message: "ingresa una contraseña!"
                          },
                          {
                            validator: this.validateToNextPassword
                          }
                        ]
                      })(
                        <Input
                          size="large"
                          type="password"
                          placeholder="Contraseña"
                          onchance={onChangePassword}
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={12}>
                    <Form.Item>
                      {getFieldDecorator("confirm", {
                        rules: [
                          {
                            required: true,
                            message: "confirma tu contraseña!"
                          },
                          {
                            validator: this.compareToFirstPassword
                          }
                        ]
                      })(
                        <Input
                          size="large"
                          type="password"
                          placeholder="Confirmar contraseña"
                          onBlur={this.handleConfirmBlur}
                        />
                      )}
                    </Form.Item>
                  </Col>
                </Row>
                <Row gutter={8}>
                  <Col xs={24} sm={24}>
                    <Form.Item>
                      {getFieldDecorator("acept", {
                        rules: [
                          {
                            required: true,
                            message: "Acepta términos y condiciones"
                          }
                        ]
                      })(
                        <Checkbox>
                          Acepto terminos y condiciones
                        </Checkbox>
                      )}
                      <LinkText onClick={this.modal}>Clic para ver</LinkText>
                    </Form.Item>
                    <p>
                      <ModalTerm
                        title="Terminos y Condiciones"
                        visible={this.state.visibleTerm}
                        onCancel={this.handleCancel}
                        footer={null}
                      >
                        <TerminosCondiciones handleOk={this.handleOk} />
                      </ModalTerm>
                    </p>
                  </Col>
                </Row>
                <div />
                <div style={{textAlign:'center',}} >
                  <Button
                    type="primary"
                    htmlType="submit"
                    className="green-btn"
                  >
                    Crear cuenta
                  </Button>
                </div>
                <div className="isoInputWrapper isoCenterComponent isoHelperWrapper">
                  <Link to="/signin">
                    <IntlMessages id="page.signUpAlreadyAccount" />
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

export default Form.create()(SignUpForm);
