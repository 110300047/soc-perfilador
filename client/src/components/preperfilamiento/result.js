import React, { Component } from 'react';
import { Row, Col, Button, Card, Radio,} from 'antd';
import { Link } from 'react-router-dom';
import { Circle, IconCheck, StatusProfiling ,TxtAling,} from './preperfilamiento.style';
//import Radio, { RadioGroup } from '../../components/uielements/radio';
const RadioGroup = Radio.Group;
export default class ResultPreProfiling extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: 'negativo',
      result2: 'positivo',
    };
  }

  render() {
    const { result, result2 } = this.state;
    const {desContact, onChangeDesContact} = this.props
    return (
      <div>
        <h1>Resultado Preperfilamiento</h1>
        <hr className="plecaAzul"></hr>
        <Row>
          <Col xs={18} sm={18} md={18} lg={18} xl={18}>
            <p>
              Acaba de terminar la precalificacion de la primera fase, sus
              resultados dieron{' '}


              <StatusProfiling status={result}>{result}</StatusProfiling>.

            </p>
            <br></br>
            <p>
              Tu perfilamiento fue exitoso (<StatusProfiling status={result2}>{result2}</StatusProfiling>), felicidades; para continuar con la autorización de tu crédito, un asesor especializado se pondrá en contacto contigo”. 
              
              Deseas ser contactado?
            </p>
            <RadioGroup onChange={onChangeDesContact} value = {desContact}>
              <Row gutter={16}>
                <Col xs={12}>
                  <Radio.Button style={{ width: '100%' }} value={'Si'}>Si</Radio.Button>
                </Col>
                <Col xs={12}>
                  <Radio.Button style={{ width: '100%' }} value={'No'}>No</Radio.Button>
                </Col>
              </Row>   
            </RadioGroup>
            <p>
              Por favor oprima en <strong>Siguiente</strong> para continuar
            </p>
            <TxtAling>
              <Row type="flex" justify="center" align="middle" gutter={16}>
                <Col xs={12} sm={6} md={4}>
                  <Link to={`/`} style={{ width: '100px'}}>
                    <Button style={{ width: '100px'}} >salir</Button>
                  </Link>
                </Col>
                <Col xs={12} sm={6} md={4}>
                  <Link to={`dashboard/perfilamiento/`} style={{ width: '100px'}}>
                    <Button type="primary" style={{ width: '100px'}}>Continuar</Button>
                  </Link>
                </Col>
              </Row>
            </TxtAling>
          </Col>          
          <Col xs={6} sm={6} md={6} lg={6} xl={6}>
            <Card style={{ width: 100 }}>
              <Circle status={result === 'positivo' ? 'positivo' : 'apagado'}>
                <IconCheck type="check" />
              </Circle>{' '}
              <Circle status={result === 'medio' ? 'medio' : 'apagado'}>
                <IconCheck type="check" />
              </Circle>{' '}
              <Circle status={result === 'negativo' ? 'negativo' : 'apagado'}>
                <IconCheck type="close" />
              </Circle>{' '}
            </Card>
          </Col>                    
        </Row>        
      </div>
    );
  }
}
