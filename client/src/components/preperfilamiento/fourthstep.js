import React, { Component } from "react";
import ModalSoc from "../../components/perfilamiento/modal";
import ContentHolder from "../../components/utility/contentHolder";
import { Link } from "react-router-dom";
//import Radio, { RadioGroup } from '../../components/uielements/radio';
import { TxtAling, ContenForm } from "./preperfilamiento.style";
import { Row, Col, Button, Select, Form, Radio } from "antd";
import Fechas from "../../components/preperfilamiento/fechas";
import { parseTwoDigitYear } from "moment";
const RadioGroup = Radio.Group;
class FourthStep extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkRegimen: false,
      checkNationality: false,
      visible: false,
      failForeigner: false,
      controllerWork: true,
      checkExCuBaMe: false,
      checkExFoMi: false
      // completeDate: false,
    };
  }

  checkDate = (day, month, year, dd, mm, aaaa) => {
    // const Bday = new Date(`${mm} ${dd}, ${aaaa}`);
    // var ThisYear = today.getFullYear()
    // console.log(Bday);

    if (day == true && month == true && year == true) {
      const today = new Date();
      const tday = today.getDate(),
        tmonth = today.getMonth(),
        tyear = today.getFullYear();
      const bday = Number(dd),
        bmonth = Number(mm),
        byear = Number(aaaa);

      if (
        tyear - byear > 18 ||
        (tyear - byear == 18 && bmonth - bmonth >= 1) ||
        (tyear - byear == 18 && bmonth - bmonth == 0 && tday - bday >= 1)
      ) {
        this.props.form.setFieldsValue({
          fechaNac: this.props.fechaNac
        });
      } else {
        this.props.form.setFields({
          fechaNac: {
            value: null,
            errors: [new Error("Debes ser mayor de edad!")]
          }
        });
      }
    }
  };

  handleSubmit = e => {
    const { onCreateGeneralData, fechaNac } = this.props;
    e.preventDefault();
    const yolo = this.props.form.getFieldsValue();
    console.log("Received values of form2: ", yolo);
    console.log("Fecha buena: ", fechaNac);
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        onCreateGeneralData();
        window.scrollTo({
          top: 0,
          behavior: "smooth"
        });
      }
    });
  };

  handleRequiredRegimen = e => {
    const { handleChangeReg } = this.props;
    handleChangeReg(e);
    const status = e === "Mancomunado" ? true : false;
    this.setState(
      {
        checkRegimen: status
      },
      () => {
        this.props.form.validateFields(["ConPaTip"], { force: true });
      }
    );
  };

  handleRequiredNationality = e => {
    const { handleChangeNac } = this.props;
    handleChangeNac(e);
    const status = e === "Extranjero(a)" ? true : false;
    this.setState(
      {
        checkNationality: status
      },
      () => {
        this.props.form.validateFields(["ExFoMi"], {
          force: true
        });
      }
    );
  };

  handleExFoMi = e => {
    const { onChangeExFoMi } = this.props;
    onChangeExFoMi(e);
    this.setState({
      checkExFoMi: e.target.value === "No" ? true : false
    });
    this.validateForeigner("migratory", e.target.value);
  };

  validateForeigner = (type, value) => {
    const { checkNationality } = this.state;
    if (type === "migratory" && value === "No") {
      if (checkNationality) {
        this.setState(
          {
            visible: true,
            controllerWork: false
          },
          () => {
            this.props.form.validateFields(["date-picker", "estadoCivil"], {
              force: true
            });
          }
        );
      } else {
        this.setState({
          controllerWork: true
        });
      }
    }

    if (value === "Si") {
      this.setState(
        {
          controllerWork: true
        },

        () => {
          this.props.form.validateFields(["date-picker", "estadoCivil"], {
            force: true
          });
        }
      );
    }
  };

  handleOk = e => {
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const Option = Select.Option;

    const colStyle = {
      marginBottom: "16px"
    };

    const {
      handleChangeEdoCiv,
      onChangeConPaTi,
      ConPaTi,
      ExFoMi,
      estadoCivil,
      nacionalidad,
      regimen,
      previousStep,
      fechaNac,
      prospect,
      fechaNacimiento
    } = this.props;

    let Cart,
      Man,
      disa = false,
      reg = "none",
      sol = 24;
    if (nacionalidad === "Extranjero(a)") {
      Cart = "";
    } else {
      Cart = "none";
    }
    // if (estadoCivil == 'Soltero(a)' || estadoCivil == 'Union libre') {
    //   reg = 'none';
    //   disa = true;
    //   sol = 24;
    // } else {
    //   disa = false;
    //   reg = '';
    //   sol = 12;
    // }

    if (estadoCivil !== "Casado(a)") {
      reg = "none";
      disa = true;
      sol = 24;
    } else {
      disa = false;
      reg = "";
      sol = 12;
    }

    //   let Cart;
    if (regimen === "Mancomunado") {
      Man = "";
    } else {
      Man = "none";
    }
    return (
      <Form onSubmit={this.handleSubmit}>
        <h3>Datos Generales del acreditado</h3>
        <hr className="plecaAzul" />
        <ContenForm>
          <Row gutter={24} type="flex" align="middle">
            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
              Fecha de nacimiento:
            </Col>
          </Row>
          <Row gutter={24} type="flex" align="middle">
            <Col xs={24} sm={24} md={24} lg={24} xl={24} style={colStyle}>
              {/* <ContentHolder> */}
              <Form.Item>
                {getFieldDecorator("fechaNac", {
                  // initialValue: fechaNac,
                  rules: [
                    {
                      required: true,
                      message: "Por favor, selecciona tu fecha de nacimiento!"
                    }
                  ]
                })(
                  // <DatePicker
                  //   onChange={handleChangeDate}
                  //   disabled={!this.state.controllerWork}
                  //   format={'DD/MM/YYYY'}
                  //   placeholder="Fecha de nacimiento"
                  //   style={{ width: '100%' }}
                  // />
                  <Fechas
                    fecha={fechaNacimiento}
                    checkDate={this.checkDate}
                    edad={true}
                    monthPicker={false}
                  />
                )}
              </Form.Item>
              {/* </ContentHolder> */}

              {/* <Button onClick={this.prueba}>Prueba 1 Men</Button> */}
            </Col>
          </Row>

          <Row gutter={24} type="flex" align="bottom">
            <Col xs={24} sm={24} md={12} lg={12} xl={12}>
              <Row>Nacionalidad:</Row>
              <Row>
                <Form.Item>
                  {getFieldDecorator("nacionalidad", {
                    initialValue: nacionalidad,
                    rules: [
                      {
                        required: true,
                        message: "Por favor, ingresa tu nacionalidad"
                      }
                    ]
                  })(
                    <Select
                      // defaultValue={nacionalidad}
                      onChange={this.handleRequiredNationality}
                      style={{ width: "100%" }}
                    >
                      <Option value="Mexicano(a)">Mexicano(a)</Option>
                      <Option value="Extranjero(a)">Extranjero(a)</Option>
                    </Select>
                  )}
                </Form.Item>
              </Row>
            </Col>
            <Col
              xs={24}
              sm={24}
              md={12}
              lg={12}
              xl={12}
              style={{ display: Cart }}
            >
              <Row>Tiene forma migratoria:</Row>
              <Row>
                <Form.Item>
                  {getFieldDecorator("ExFoMi", {
                    initialValue: ExFoMi,
                    rules: [
                      {
                        required: this.state.checkNationality,
                        message: "Favor de responder"
                      }
                    ]
                  })(
                    <RadioGroup onChange={this.handleExFoMi}>
                      <Row gutter={16}>
                        <Col xs={12}>
                          <Radio.Button style={{ width: "100%" }} value={"Si"}>
                            Si
                          </Radio.Button>
                        </Col>
                        <Col xs={12}>
                          <Radio.Button style={{ width: "100%" }} value={"No"}>
                            No
                          </Radio.Button>
                        </Col>
                      </Row>
                    </RadioGroup>
                  )}
                </Form.Item>
              </Row>
            </Col>
          </Row>
          <ModalSoc
            title={"Perfil Rechazado"}
            TextModal={
              "No se puede continuar con la asesoria ya que no cuenta con un trabajo con forma de pago para corroborar sus ingresos"
            }
            visibleModal={this.state.visible}
            handleOk={this.handleOk}
            handleCancel={this.handleCancel}
          />

          <Row gutter={24} type="flex" align="middle">
            <Col xs={24} sm={sol} md={sol} lg={sol} xl={sol}>
              <Row>Estado civil:</Row>
              <Row>
                <Form.Item>
                  {getFieldDecorator("estadoCivil", {
                    initialValue: estadoCivil,
                    rules: [
                      {
                        required: this.state.controllerWork,
                        message: "Por favor, ingresa tu estado civil"
                      }
                    ]
                  })(
                    <Select
                      onChange={handleChangeEdoCiv}
                      style={{ width: "100%" }}
                      disabled={!this.state.controllerWork}
                    >
                      <Option value="Soltero(a)">Soltero(a)</Option>
                      <Option value="Casado(a)">Casado(a)</Option>
                      <Option value="Union libre">Union libre</Option>
                    </Select>
                  )}
                </Form.Item>
              </Row>
            </Col>
            <Col
              xs={24}
              sm={12}
              md={12}
              lg={12}
              xl={12}
              style={{ display: reg }}
            >
              <Row>Regimen:</Row>
              <Row>
                <Form.Item>
                  {getFieldDecorator("regimen", {
                    initialValue: regimen,
                    rules: [
                      {
                        required: false,
                        message: "Por favor, ingresa tu regimen"
                      }
                    ]
                  })(
                    <Select
                      setFieldValue={regimen}
                      onChange={this.handleRequiredRegimen}
                      style={{ width: "100%" }}
                      disabled={disa}
                    >
                      <Option value="Separados">Separados</Option>
                      <Option value="Mancomunado">Mancomunado</Option>
                    </Select>
                  )}
                </Form.Item>
              </Row>
            </Col>
          </Row>
          <div style={{ display: Man }}>
            <Row gutter={24} type="flex" align="middle">
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                Cónyuge paga sus creditos a tiempo:
              </Col>
            </Row>
            <Row gutter={24} type="flex" align="middle">
              <Col xs={24} sm={24} md={24} lg={24} xl={24} style={colStyle}>
                <ContentHolder>
                  <Form.Item>
                    {getFieldDecorator("ConPaTip", {
                      initialValue: ConPaTi,
                      rules: [
                        {
                          required: this.state.checkRegimen,
                          message:
                            "Favor de responder sobre los pagos del cónyuge"
                        }
                      ]
                    })(
                      <RadioGroup onChange={onChangeConPaTi}>
                        <Row gutter={16}>
                          <Col xs={12}>
                            <Radio.Button
                              style={{ width: "100%" }}
                              value={"Si"}
                            >
                              Si
                            </Radio.Button>
                          </Col>
                          <Col xs={12}>
                            <Radio.Button
                              style={{ width: "100%" }}
                              value={"No"}
                            >
                              No
                            </Radio.Button>
                          </Col>
                        </Row>
                      </RadioGroup>
                    )}
                  </Form.Item>
                </ContentHolder>
              </Col>
            </Row>
          </div>
          {/* <div style={{ display: Cart }}>
            <Row
              style={rowStyle}
              gutter={gutter}
              justify="start"
              type="flex"
              align="middle"
            >
              <Col xs={8} sm={8} md={8} lg={8} xl={8}>
                Tiene cuenta bancaria en Mexico:
              </Col>
              <Col xs={8} sm={8} md={8} lg={8} xl={8}>
                Tiene forma migratoria:
              </Col>
              <Col xs={8} sm={8} md={8} lg={8} xl={8}>
                Tiene historial Crediticio:
              </Col>
            </Row>
            <Row gutter={24} type="flex" align="middle">
              <Col xs={8} sm={8} md={8} lg={8} xl={8} style={colStyle}>
                <ContentHolder>
                  <Form.Item>
                    {getFieldDecorator('ExCuBaMe', {
                      initialValue: ExCuBaMe,
                      rules: [
                        {
                          required: this.state.checkNationality,

                          message: 'Favor de responder',
                        },
                      ],
                    })(
                      <RadioGroup onChange={this.handleExCuBaMe}>
                        <Radio value="Si">Si</Radio>
                        <Radio value="No">No</Radio>
                      </RadioGroup>
                    )}
                  </Form.Item>
                </ContentHolder>
              </Col>
              <Col xs={8} sm={8} md={8} lg={8} xl={8} style={colStyle}>
                <ContentHolder>
                  <Form.Item>
                    {getFieldDecorator('ExFoMi', {
                      initialValue: ExFoMi,
                      rules: [
                        {
                          required: this.state.checkNationality,
                          message: 'Favor de responder',
                        },
                      ],
                    })(
                      <RadioGroup onChange={this.handleExFoMi}>
                        <Radio value="Si">Si</Radio>
                        <Radio value="No">No</Radio>
                      </RadioGroup>
                    )}
                  </Form.Item>
                </ContentHolder>
              </Col>
              <Col xs={8} sm={8} md={8} lg={8} xl={8} style={colStyle}>
                <ContentHolder>
                  <Form.Item>
                    {getFieldDecorator('ExHiCre', {
                      initialValue: ExHiCre,
                      rules: [
                        {
                          required:
                            this.state.checkNationality &&
                            this.state.controllerWork,
                          message: 'Favor de responder',
                        },
                      ],
                    })(
                      <RadioGroup
                        onChange={onChangeExHiCre}
                        disabled={!this.state.controllerWork}
                      >
                        <Radio value="Si">Si</Radio>
                        <Radio value="No">No</Radio>
                      </RadioGroup>
                    )}
                  </Form.Item>
                </ContentHolder>
              </Col>
              
            </Row>
          </div> */}
        </ContenForm>

        <TxtAling>
          <Row type="flex" justify="center" align="middle" gutter={16}>
            <Col xs={12} sm={6} md={4}>
              <Button
                onClick={previousStep}
                disabled={!this.state.controllerWork}
                className="btn-navigation"
              >
                Atras
              </Button>
            </Col>
            <Col xs={12} sm={6} md={4}>
              <Form.Item>
                {!this.state.controllerWork ? (
                  <Link to={`/`} onClick={prospect}>
                    <Button
                      type="primary"
                      style={{ width: "100px", marginTop: 16 }}
                    >
                      Finalizar
                    </Button>
                  </Link>
                ) : (
                  <Button
                    type="primary"
                    htmlType="submit"
                    style={{ width: "100px", marginTop: 16 }}
                  >
                    Siguiente
                  </Button>
                )}
              </Form.Item>
            </Col>
          </Row>
        </TxtAling>
      </Form>
    );
  }
}
export default Form.create()(FourthStep);
