import styled from 'styled-components';
import { Icon, Radio, Modal, InputNumber, Layout } from 'antd';

import NumberFormat from 'react-number-format';

const stateColor = {
  positivo: 'green',
  negativo: 'red',
  medio: 'orange',
  apagado: 'grey',
};
export const Content = styled(Layout.Content)`
  padding: 0 5px;
`;
export const Circle = styled.div`
  background: ${({ status }) => stateColor[status]};
  width: 50px;
  height: 50px;
  margin: 10px 0px;
  -webkit-border-radius: 25px;
  -moz-border-radius: 25px;
  border-radius: 25px;
`;

export const StatusProfiling = styled.span`
  color: ${({ status }) => stateColor[status]};
  font-weight: bold;
`;

export const IconCheck = styled(Icon)`
  color: White;
  font-size: 32px;
  vertical-align: middle;
  text-align: center;
  left: 20%;
  position: relative;
  display: table-cell;
  top: 20%;
`;
export const mainContainer = styled.div`
  padding: 0 5px;
  @media (min-width: 992px) {    
    padding: 0 auto;
  }  
`;
export const TxtAling = styled.div`
  text-align: center;
  margin: 20px 0px;
`;
export const AlingRAdioButton = styled(TxtAling)`
  width: 100%;
`;

export const ContenForm = styled.div`
  margin-bottom: 10px;
`;

export const Conten = styled(ContenForm)`
  width: 70%;
`;
export const Contenflex = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const RadioButton = styled(Radio.Button)`
  width: 100%;
`;
export const RadioGroup = styled(Radio.Group)`
  width: 100% !important;
  margin-bottom: 0;
  @media (min-width: 992px){
    margin-bottom: 16px;
  }
`;

export const PhoneInput = styled(NumberFormat)`
  -webkit-font-feature-settings: 'tnum';
  font-feature-settings: 'tnum';
  font-variant: tabular-nums;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  list-style: none;
  position: relative;
  display: inline-block;
  padding: 4px 11px;
  width: 100%;
  height: 40px;
  font-size: 14px;
  line-height: 1.5;
  color: rgba(0, 0, 0, 0.65);
  background-color: #fff;
  background-image: none;
  border: 1px solid #305d8a;
  border-radius: 4px;
  transition: all 0.3s;
`;

export const ModalTerm = styled(Modal)`
  width: 90% !important ;
`;

export const LinkText = styled.span`
  color: #61b1f1;
  border-bottom: 2px solid transparent;
  transition: color 0.3s cubic-bezier(0.645, 0.045, 0.355, 1), border-color 0.3s cubic-bezier(0.645, 0.045, 0.355, 1), background 0.3s cubic-bezier(0.645, 0.045, 0.355, 1), padding 0.15s cubic-bezier(0.645, 0.045, 0.355, 1);
  &:hover {    
    border-bottom: 2px solid #47bc53;
  }
`;

export const DisableInput = styled(InputNumber)`
  color: black !important;
`;
