import React, { Component } from 'react';
import { Col, Button, Select, DatePicker, Row, Form } from 'antd';
// import { Link } from 'react-router-dom';
// import { Circle, IconCheck, StatusProfiling } from './preperfilamiento.style';

const Option = Select.Option;

var today = new Date();
var ThisYear = today.getFullYear()


const day = Array.apply(null, {length: 31}).map(function(value, index){
    return index + 1;
  });
// const month = Array.apply(null, {length: 12}).map(function(value, index){
//     return index + 1;
//   });
// const month = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
const month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
let year = Array.apply(null, {length: 70}).map(function(value, index){
    return ThisYear - (index );
  });
// for (let i=1; i<32; i++){
//     day = day.concat(i)
// };

export default class Fechas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dia: '',
      mes: '',
      anio: '',
      fechaCompleta: '',
      checkDia: false,
      checkMes: false,
      checkAnio: false,
    //   day: Array.apply(null, {length: 31}).map(function(value, index){ return index + 1;})
    };
  }

//   prueba = () =>{
//     // const {feApCreBan} = this.props
//     // const {day} = this.state;
//     console.log(today);
//     console.log(ThisYear);
//     // this.setState({
//     //   feApCreBan: fechaFull,
//     // });
//     // console.log("fecha"+ this.state.feApCreBan);
//   }

  handleChangeDia = value => {
    this.setState(
      {
        dia: value,
      },
      function() {
        const {fecha, checkDate, monthPicker} = this.props;
        if (monthPicker==true){
          const fechaFull = this.state.mes + "/" + this.state.anio;
          fecha(fechaFull);
        } else {
          const fechaFull = this.state.dia + "/" + this.state.mes + "/" + this.state.anio;
          fecha(fechaFull);
        }

        
        
        checkDate(this.state.checkDia, this.state.checkMes, this.state.checkAnio, this.state.dia, this.state.mes, this.state.anio);
      }
    );
  };
  handleChangeMes = value => {
    this.setState(
      {
        mes: value,
      },
      function() {
        const {fecha, checkDate, monthPicker} = this.props;
        if (monthPicker==true){
          const fechaFull = this.state.mes + "/" + this.state.anio;
          fecha(fechaFull);
        } else {
          const fechaFull = this.state.dia + "/" + this.state.mes + "/" + this.state.anio;
          fecha(fechaFull);
        }
        
        checkDate(this.state.checkDia, this.state.checkMes, this.state.checkAnio, this.state.dia, this.state.mes, this.state.anio);
      }
    );
  };
  handleChangeAnio = value => {
    this.setState(
      {
        anio: value,
      },
      function() {
        const {fecha, checkDate, monthPicker} = this.props;
        if (monthPicker==true){
          const fechaFull = this.state.mes + "/" + this.state.anio;
          fecha(fechaFull);
        } else {
          const fechaFull = this.state.dia + "/" + this.state.mes + "/" + this.state.anio;
          fecha(fechaFull);
        }
        
        checkDate(this.state.checkDia, this.state.checkMes, this.state.checkAnio, this.state.dia, this.state.mes, this.state.anio);
      }
    );
  };
  render() {
    
    const { monthPicker, disa, edad } = this.props;  
    const colStyle2 = {
        // marginTop: '34px'
      };
    year = edad == true ? year.reverse() : year;
      const rowStyle2 = {
        width: '100%',
        display: disp,
        flexFlow: 'row wrap',
      };
      
      const rowStyle = {
        width: '100%',
        display: 'flex',
        flexFlow: 'row wrap',
      };
    let disp = '';
    let colSize = monthPicker==true ? 12 : 8;
    if (this.state.dia != ''){
      this.state.checkDia = true;
    }
    if (this.state.mes != ''){
      this.state.checkMes = true;
    }
    if (this.state.anio != ''){
      this.state.checkAnio = true;
    }
    if (monthPicker==true){
      this.state.checkDia = true;
      disp = 'none';
      
    } else {
      disp = '';
    }
    // const colStyle3 = {
    //   marginBottom: '2px',
    //   marginLeft: '0px',
    //   marginRight: '0px',
    //   paddingRight: '3px',
    //   paddingLeft: '0px',
    //   display: disp,
    //   marginTop: '34px'
    // };

    return (
      <div>                
        <Row gutter={8} className="ajusteEspacioFechas">
          <Col xs={colSize} style={{display: disp}}>
            <Row style={rowStyle}>
                    <Select
                    onChange={this.handleChangeDia}
                    disabled={disa}
                    style={{  marginLeft: 0, 
                              width: '100%',
                              minWidth: '42px',
                              // maxWidth: '200px' ,
                              // paddingRight: '0px',
                          }}
                    placeholder={"DD"}
                  >
                      {day.map(day => <Option key={day}>{day}</Option>)}
                      {/* <Option value="10">10</Option>
                      <Option value="20">20</Option>
                      <Option value="30">30</Option>
                      <Option value="31">31</Option> */}
                  
                  </Select>
                
              
                </Row>
          </Col>
          <Col xs={colSize}>
              <Row style={rowStyle}>
              <Select
                onChange={this.handleChangeMes}
                disabled={disa}
                placeholder={"MM"}
              >
              {month.map(month => <Option key={month}>{month}</Option>)}
                  {/* <Option value="01">01</Option>
                  <Option value="04">04</Option>
                  <Option value="08">08</Option>
                  <Option value="12">12</Option> */}
              </Select>
              </Row>
          </Col>
          <Col xs={colSize} >
            <Row style={rowStyle}>
                <Select
                  onChange={this.handleChangeAnio}
                  disabled={disa}
                  placeholder={"AAAA"}

                >
                    {year.map(year => <Option key={year}>{year}</Option>)}
                    {/* <Option value="1900">1900</Option>
                    <Option value="1950">1950</Option>
                    <Option value="2000">2000</Option>
                    <Option value="2019">2019</Option> */}
                </Select>
              </Row>
          </Col>
        </Row>        

      </div>
    );
  }
}
