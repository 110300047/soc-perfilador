import React, { Component } from "react";
import { Row, Col, Input, Checkbox, Button, Form, Radio } from "antd";
import {
  ContenForm,
  Conten,
  Contenflex,
  TxtAling,
  ModalTerm,
  LinkText,
  PhoneInput
} from "./preperfilamiento.style";

import TerminosCondiciones from "../terminos/terminos";
const RadioGroup = Radio.Group;
class SecondStep extends Component {
  state = {
    confirmPass: false,
    confirmEmail: false,
    visibleTerm: false
  };

  modal = () => {
    this.setState({
      visibleTerm: true
    });
  };

  handleOk = e => {
    this.setState({
      visibleTerm: false
    });
  };

  handleCancel = e => {
    this.setState({
      visibleTerm: false
    });
  };

  //pass
  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("password")) {
      callback("Las contraseñas no coinciden!");
    } else {
      callback();
    }
  };

  validatePassword = (rule, value, callback) => {
    console.log(value);
    var test = value.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,25}$/i);
    var noSpace = value.match(/\s/);

    console.log("test", test);
    console.log("noSpace", noSpace);
    if (!(!noSpace && test)) {
      callback(
        "Minimo 6 caracteres, mayusculas, minusculas, numeros y sin espacios"
      );
    } else {
      callback();
    }
  };

  validateNumber = (rule, value, callback) => {
    var test = value.match(/[_]/);
    if (test) {
      callback("Numero telefonico incorrecto");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmPass) {
      form.validateFields(["confirmpassword"], { force: true });
    }

    callback();
  };

  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmPass: this.state.confirmPass || !!value });
  };

  //email
  compareToFirstEmail = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("email")) {
      callback("los correos no coinciden!");
    } else {
      callback();
    }
  };

  validateToNextEmail = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmEmail) {
      form.validateFields(["confirmEmail"], { force: true });
    }
    callback();
  };

  handleConfirmEmail = e => {
    const value = e.target.value;
    this.setState({ confirmEmail: this.state.confirmEmail || !!value });
  };

  validateAccount = (rule, value, callback) => {
    console.log(value, value.length);
    if (value && value.length > 9) {
      callback();
    } else {
      callback(new Error("al menos 10 caracteres"));
    }
  };

  handleSubmit = e => {
    const { nextStep } = this.props;
    // console.log('user', usercreated);
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        nextStep();
        window.scrollTo({
          top: 0,
          behavior: "smooth"
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const {
      previousStep,
      onChangeEmail,
      onChangePassword,
      onChangeCellPhone,
      onChangeName,
      onChangeFirstSurname,
      onChangeSecondSurname,
      onChangeAccept,
      email,
      password,
      cellPhone,
      name,
      firstSurname,
      secondSurname
    } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        {/* <Row type="flex" justify="center" align="middle">
        <Col xs={22} sm={18} md={16} lg={14} xl={12}> */}
        <h3 style={{ textAlign: "center" }}>Háblanos de ti</h3>
        <hr className="plecaAzul" />
        <Contenflex>
          <Conten>
            <Row gutter={24} type="flex" align="middle">
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                <Row>Nombre:</Row>
                <Row>
                  <Form.Item>
                    {getFieldDecorator("name", {
                      initialValue: name,
                      rules: [
                        {
                          required: true,
                          message: "Por favor, ingresa tu nombre(s)"
                        }
                      ]
                    })(
                      <Input onChange={onChangeName} placeholder="Nombre(s)" />
                    )}
                  </Form.Item>
                </Row>
              </Col>
            </Row>

            <Row gutter={24} type="flex" align="middle">
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Row>Primer Apellido:</Row>
                <Row>
                  <Form.Item>
                    {getFieldDecorator("firstSurname", {
                      initialValue: firstSurname,
                      rules: [
                        {
                          required: true,
                          message: "Por favor, ingresa tu primer apellido"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Primer apellido"
                        onChange={onChangeFirstSurname}
                      />
                    )}
                  </Form.Item>
                </Row>
              </Col>

              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Row>Segundo Apellido:</Row>
                <Row>
                  <Form.Item>
                    {getFieldDecorator("secondSurname", {
                      initialValue: secondSurname,
                      rules: [
                        {
                          required: false,
                          message: "Por favor, ingresa tu segundo apellido"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Segundo apellido"
                        onChange={onChangeSecondSurname}
                      />
                    )}
                  </Form.Item>
                </Row>
              </Col>
            </Row>

            <Row gutter={24} type="flex" align="bottom">
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Row>Correo Electrónico:</Row>
                <Row>
                  <Form.Item>
                    {getFieldDecorator("email", {
                      initialValue: email,
                      rules: [
                        {
                          type: "email",
                          message: "No es un correo valido"
                        },
                        {
                          required: true,
                          message:
                            "crea una contraseña para personalizar tu evaluación"
                        },
                        {
                          validator: this.validateToNextEmail
                        }
                      ]
                    })(
                      <Input
                        onChange={onChangeEmail}
                        onCopy={e => {
                          e.clipboardData.setData("text/plain", "");
                          e.preventDefault();
                        }}
                        placeholder="Correo"
                      />
                    )}
                  </Form.Item>
                </Row>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Row>Confirmar Correo Electrónico:</Row>
                <Row>
                  <Form.Item>
                    {getFieldDecorator("confirmEmail", {
                      rules: [
                        {
                          type: "email",
                          message: "No es un correo valido"
                        },
                        {
                          required: true,
                          message: "Por favor, confirme tu correo electronico!"
                        },
                        {
                          validator: this.compareToFirstEmail
                        }
                      ]
                    })(
                      <Input
                        placeholder="Confirmar correo"
                        onBlur={this.handleConfirmEmail}
                      />
                    )}
                  </Form.Item>
                </Row>
              </Col>
            </Row>

            <Row gutter={24} type="flex" align="middle">
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Row>Contraseña:</Row>
                <Row>
                  <Form.Item>
                    {getFieldDecorator("password", {
                      initialValue: password,
                      rules: [
                        {
                          required: true,
                          message: "Por favor, ingrese su contraseña!"
                        },
                        {
                          validator: this.validateToNextPassword
                        },
                        {
                          validator: this.validatePassword
                        }
                      ]
                    })(
                      <Input
                        onChange={onChangePassword}
                        placeholder="Contraseña"
                        type="password"
                      />
                    )}
                  </Form.Item>
                </Row>
              </Col>

              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Row>Confirmar contraseña:</Row>
                <Row>
                  <Form.Item>
                    {getFieldDecorator("confirmpassword", {
                      rules: [
                        {
                          required: true,
                          message: "por favor confirme su contraseña!"
                        },
                        {
                          validator: this.compareToFirstPassword
                        }
                      ]
                    })(
                      <Input
                        type="password"
                        placeholder="Confirmar contraseña"
                        onBlur={this.handleConfirmBlur}
                      />
                    )}
                  </Form.Item>
                </Row>
              </Col>
            </Row>

            <Row gutter={24} type="flex" align="middle">
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Row>Numero de Celular:</Row>
                <Row>
                  <Form.Item>
                    {getFieldDecorator("phone", {
                      initialValue: cellPhone,
                      rules: [
                        {
                          required: true,
                          message: "Por favor, ingresa un numero de telefono!"
                        },
                        {
                          validator: this.validateNumber
                        }
                      ]
                    })(
                      <PhoneInput
                        format="(###) ###-####"
                        onValueChange={onChangeCellPhone}
                        mask="_"
                        placeholder="Numero de telefono"
                      />
                    )}
                  </Form.Item>
                </Row>
              </Col>

              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Row>Correo alternativo:</Row>
                <Row>
                  <Form.Item>
                    {getFieldDecorator("emailalternative", {
                      rules: [
                        {
                          type: "email",
                          message: "No es un correo valido"
                        },
                        {
                          required: true,
                          message: "Por favor, ingresa tu correo electronico!"
                        }
                      ]
                    })(<Input placeholder="Correo alternativo" />)}
                  </Form.Item>
                </Row>
              </Col>
            </Row>
          </Conten>
        </Contenflex>
        <ContenForm>
          <Row type="flex" style={{ justifyContent: "center" }}>
            <Form.Item>
              {getFieldDecorator("agreement", {
                valuePropName: "checked",
                rules: [
                  {
                    required: true,
                    message:
                      "Por favor, acepta nuestros terminos y conficiones!"
                  }
                ]
              })(
                <Checkbox onChange={onChangeAccept}>
                  Acepta Terminos y Condiciones.{" "}
                </Checkbox>
              )}
              <LinkText
                onClick={this.modal}
                style={{ marginLeft: "-8px", marginRight: "-8px" }}
              >
                Clic para ver
              </LinkText>
              <ModalTerm
                title="Términos y Condiciones"
                visible={this.state.visibleTerm}
                onCancel={this.handleCancel}
                footer={null}
              >
                <TerminosCondiciones handleOk={this.handleOk} />
              </ModalTerm>
            </Form.Item>
          </Row>
        </ContenForm>

        <TxtAling>
          <Row type="flex" justify="center" align="middle" gutter={16}>
            <Col xs={12} sm={6} md={4}>
              <Button className="btn-navigation" onClick={previousStep}>
                Atras
              </Button>
            </Col>
            <Col xs={12} sm={6} md={4}>
              <Button
                type="primary"
                htmlType="submit"
                className="btn-navigation"
              >
                Siguiente
              </Button>
            </Col>
          </Row>
        </TxtAling>
        {/* </Col>
        </Row> */}
      </Form>
    );
  }
}

export default Form.create()(SecondStep);
