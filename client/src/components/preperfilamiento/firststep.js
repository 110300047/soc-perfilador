import React, { Component } from "react";
import {
  Col,
  Row,
  InputNumber,
  Button,
  Form,
  Select,
  Radio,
  Switch
} from "antd";
import Numeral from "numeral";
import {
  TxtAling,
  ContenForm,
  AlingRAdioButton,
  DisableInput
} from "./preperfilamiento.style";
const Option = Select.Option;
const RadioGroup = Radio.Group;
class FirstStep extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkCo: false,
      checkIngresos: false,
      checkEnganche: true
    };
  }
  formatter(value) {
    return `${Numeral(value).format("$0,0")}`;
  }
  handleSubmit = e => {
    const { nextStep } = this.props;
    // console.log('user', usercreated);
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        nextStep();
        window.scrollTo({
          top: 0,
          behavior: "smooth"
        });
      } else {
        console.log(err);
      }
    });
  };
  validateProduct = (rule, value, callback) => {
    console.log(value);
    if (value < 800000) {
      callback("El valor minimo es de $800,000");
    }
    callback();
  };

  validateEnganche = (rule, value, callback) => {
    const { inputProduct } = this.props;
    const form = this.props.form;
    const min = this.minimumValue(inputProduct);
    console.log("value validate", value, min);
    if (this.state.checkEnganche) {
      if (!min) {
        callback("Selecciona el tipo de credito que estas interesado");
      }
      if (value < min || value > inputProduct * 0.9) {
        callback(
          `El valor minimo es de ${this.formatter(
            min
          )} y maximo ${this.formatter(inputProduct * 0.9)} `
        );
      }
    }
    callback();
  };

  handleTieneCo = e => {
    const { onChangeTieneCo } = this.props;
    onChangeTieneCo(e);
    const status = e.target.value === "Si" ? true : false;
    this.setState(
      {
        checkCo: status
      },
      () => {
        this.props.form.validateFields(["tipoCoacreditado"], { force: true });
      }
    );
  };

  handleonChangeCumpleIngreso = e => {
    const { onChangeCumpleIngreso } = this.props;
    onChangeCumpleIngreso(e);
    const status = e.target.value === "No" ? true : false;
    this.setState(
      {
        checkIngresos: status
      },
      () => {
        this.props.form.validateFields(["ingresoAprox"], { force: true });
      }
    );
  };

  minimumValue = inputProduct => {
    if (!this.state.checkEnganche) {
      return 0;
    }
    const form = this.props.form;
    const option = form.getFieldValue("typeProduct");
    const minValue = {
      casa: inputProduct * 0.1,
      construccion: 0,
      terreno: inputProduct * 0.3,
      terreno_Construccion: inputProduct,
      mejora_Hipoteca: 0,
      liquidez: inputProduct * 0.3
    };
    return minValue[option];
  };

  OnchanceProduct = e => {
    const {
      target: { value }
    } = e;
    const { onChangeEnganche } = this.props;
    var status;
    console.log("value", value);
    if (value === "construccion" || value === "mejora_Hipoteca") {
      status = false;
      this.props.form.setFieldsValue({
        inputEnganche: 0
      });
      onChangeEnganche(0);
    } else status = true;
    console.log(status);
    this.setState(
      {
        checkEnganche: status
      },
      () => {
        this.props.form.validateFields(["inputEnganche"], { force: true });
      }
    );

    const { onClickProduct } = this.props;
    onClickProduct(e);
  };

  render() {
    const colStyle = {
      marginBottom: "16px"
    };
    const { getFieldDecorator } = this.props.form;

    const {
      inputProduct,
      inputEnganche,
      onChangeProduct,
      onChangeEnganche,
      handleChangeTiCo,
      typeProduct,
      cumpleIngreso,
      tieneCo,
      tipoCoacreditado,
      onChangeIngresoAprox,
      ingresoAprox,
      menSug
    } = this.props;

    let tiCos = "none",
      newIngreso = "none",
      newIngresVal = false,
      coacred = false,
      ticoaux = 24;

    if (tieneCo == "Si") {
      tiCos = "";
      ticoaux = 12;
      // req = true;
    } else {
      tiCos = "none";
      ticoaux = 24;
      // req = false;
    }

    if (this.state.checkIngresos) {
      newIngreso = "";
      newIngresVal = true;
    } else {
      newIngreso = "none";
      newIngresVal = false;
    }

    return (
      <Form onSubmit={this.handleSubmit}>
        <div>
          <AlingRAdioButton>
            <h3> Tipo de Crédito que te interesa</h3>
            <hr className="plecaAzul" />
            <Form.Item>
              {getFieldDecorator("typeProduct", {
                initialValue: typeProduct,
                rules: [
                  {
                    required: true,
                    message: "Seleccione tipo de producto a adquirir!"
                  }
                ]
              })(
                <RadioGroup onChange={this.OnchanceProduct} buttonStyle="solid">
                  <Row
                    type="flex"
                    justify="center"
                    align="top"
                    gutter={12}
                    className="icon-group-style"
                  >
                    <Col xs={12} sm={8}>
                      <Radio.Button
                        style={{ width: "100%", marginBottom: "12px" }}
                        className="align-flex-xy-center"
                        value={"casa"}
                      >
                        <span className="icon-casa" />
                        Casa
                      </Radio.Button>
                    </Col>
                    <Col xs={12} sm={8}>
                      <Radio.Button
                        style={{ width: "100%" }}
                        className="align-flex-xy-center"
                        value={"construccion"}
                      >
                        <span className="icon-construccion" />
                        Construccion
                      </Radio.Button>
                    </Col>
                    <Col xs={12} sm={8}>
                      <Radio.Button
                        style={{ width: "100%", marginBottom: "12px" }}
                        className="align-flex-xy-center"
                        value={"terreno"}
                      >
                        <span className="icon-terrain" />
                        Terreno
                      </Radio.Button>
                    </Col>
                    <Col xs={12} sm={8}>
                      <Radio.Button
                        style={{ width: "100%", fontSize: "0.9em" }}
                        className="align-flex-xy-center"
                        value={"terreno_Construccion"}
                      >
                        <span className="icon-terrenoconstruccion d-none d-sm-block" />
                        Terreno + Construcción
                      </Radio.Button>
                    </Col>
                    <Col xs={12} sm={8}>
                      <Radio.Button
                        style={{ width: "100%" }}
                        className="align-flex-xy-center"
                        value={"mejora_Hipoteca"}
                      >
                        <span className="icon-hipoteca" />
                        Mejora Hipoteca
                      </Radio.Button>
                    </Col>
                    <Col xs={12} sm={8}>
                      <Radio.Button
                        style={{ width: "100%" }}
                        className="align-flex-xy-center"
                        value={"liquidez"}
                      >
                        <span className="icon-liquidez" />
                        Liquidez
                      </Radio.Button>
                    </Col>
                  </Row>
                </RadioGroup>
              )}
            </Form.Item>
          </AlingRAdioButton>
        </div>
        <div>
          <div>
            <Row>
              <TxtAling>
                <h3>Datos generales del acreditado</h3>
                <hr className="plecaAzul" />
              </TxtAling>
            </Row>
          </div>
          <ContenForm>
            <Row type="flex" align="bottom" gutter={16}>
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <Row>¿Cuánto cuesta lo que quieres adquirir?</Row>
                <Row>
                  <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                    <Form.Item>
                      {getFieldDecorator("inputProduct", {
                        initialValue: inputProduct,
                        rules: [
                          {
                            required: true,
                            message: "describa el valor del producto!"
                          },
                          {
                            validator: this.validateProduct
                          }
                        ]
                      })(
                        <InputNumber
                          // min={1000000}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          parser={value => value.replace(/\$\s?|(,*)/g, "")}
                          onChange={onChangeProduct}
                          step={100000}
                        />
                      )}
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <Row>Enganche</Row>
                <Row>
                  <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                    <Form.Item>
                      {getFieldDecorator("inputEnganche", {
                        initialValue: inputEnganche,
                        rules: [
                          {
                            required: this.state.checkEnganche,
                            message: "describa el valor del enganche!"
                          },
                          {
                            validator: this.validateEnganche
                          }
                        ]
                      })(
                        <InputNumber
                          min={this.minimumValue(inputProduct)}
                          max={inputProduct * 0.9}
                          style={{ width: "100%" }}
                          formatter={value =>
                            `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          parser={value => value.replace(/\$\s?|(,*)/g, "")}
                          onChange={onChangeEnganche}
                          step={50000}
                          disabled={!this.state.checkEnganche}
                        />
                      )}
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
            </Row>
          </ContenForm>
          <ContenForm>
            <Row type="flex" align="middle">
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                Tu crédito sería por:
              </Col>

              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <DisableInput
                  style={{ marginLeft: 16, width: 120 }}
                  value={Numeral(inputProduct - inputEnganche).format("$0,0")}
                  onChange={onChangeEnganche}
                  disabled={true}
                />
              </Col>
            </Row>
          </ContenForm>
          <ContenForm>
            <Row type="flex" align="middle">
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                Tu mensualidad aproximada sería de:
              </Col>
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <DisableInput
                  style={{ marginLeft: 16, width: 120 }}
                  value={Numeral((inputProduct - inputEnganche) * 0.01).format(
                    "$0,0"
                  )}
                  disabled={true}
                />
              </Col>
            </Row>
          </ContenForm>
          <ContenForm>
            <Row type="flex" align="middle">
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                Te sugerimos que tu ingreso mensual sea minimo de:
              </Col>
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <DisableInput
                  style={{ marginLeft: 16, width: 120 }}
                  value={Numeral(
                    24000 * ((inputProduct - inputEnganche) / 1000000)
                  ).format("$0,0")}
                  disabled={true}
                />
              </Col>
            </Row>
          </ContenForm>
          <ContenForm>
            <Row type="flex" align="middle">
              <Col span={24} style={{ textAlign: "justify" }}>
                Tu capacidad de pago para la mensualidad del crédito, se
                calculará de tus ingresos menos tus deudas actuales en buró
              </Col>
            </Row>
          </ContenForm>
        </div>
        <div>
          <Row gutter={24} type="flex" align="middle">
            <Col xs={24}>
              Cumple con el ingreso sugerido ($ {menSug - (menSug % 1)} MXN):
            </Col>
          </Row>
          <Row gutter={24} type="flex" align="middle">
            <Col xs={24} sm={24} md={24} lg={24} xl={24} style={colStyle}>
              <Form.Item>
                {getFieldDecorator("cumpleIngreso", {
                  initialValue: cumpleIngreso,
                  rules: [
                    {
                      required: true,
                      message: "Favor de responder"
                    }
                  ]
                })(
                  <RadioGroup
                    onChange={this.handleonChangeCumpleIngreso}
                    setFieldsValue={cumpleIngreso}
                    buttonStyle="solid"
                  >
                    <Row gutter={16}>
                      {/* <Col xs={12}><Switch checkedChildren="si" unCheckedChildren="no" /></Col> */}
                      <Col xs={12}>
                        <Radio.Button style={{ width: "100%" }} value={"Si"}>
                          Si
                        </Radio.Button>
                      </Col>
                      <Col xs={12}>
                        <Radio.Button style={{ width: "100%" }} value={"No"}>
                          No
                        </Radio.Button>
                      </Col>
                    </Row>
                  </RadioGroup>
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row
            gutter={24}
            type="flex"
            align="middle"
            style={{ display: newIngreso }}
          >
            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
              introduzca su ingreso mensual aproximado :
            </Col>
            <Col
              xs={12}
              sm={12}
              md={12}
              lg={12}
              xl={12}
              // style={{ display: histc }}
            >
              Su crédito con 10% de enganche sería de:
            </Col>
          </Row>
          <Row
            gutter={24}
            type="flex"
            align="middle"
            style={{ display: newIngreso }}
          >
            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
              <Form.Item style={{ marginBottom: "0" }}>
                {getFieldDecorator("ingresoAprox", {
                  // initialValue: ingresoAprox,
                  rules: [
                    {
                      required: newIngresVal,
                      message: "Favor de responder"
                    }
                  ]
                })(
                  <InputNumber
                    min={0}
                    max={9000000}
                    style={{ width: "100px", marginLeft: 1 }}
                    // defaultValue={ingresoAprox}
                    formatter={value =>
                      `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                    parser={value => value.replace(/\$\s?|(,*)/g, "")}
                    onChange={onChangeIngresoAprox}
                  />
                )}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
              <InputNumber
                // Cambiar
                // min={0}
                // max={50000}
                // style={{ marginLeft: 16 }}
                // value={Numeral(montoMayorAprox).format('$0,0.00')}
                style={{ width: "100px", marginLeft: 1 }}
                value={(ingresoAprox * 1000000) / 24000 + ingresoAprox * 0.1}
                formatter={value =>
                  `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                parser={value => value.replace(/\$\s?|(,*)/g, "")}
                // onChange={onChangeAcreTotal}
                disabled={true}
              />
            </Col>
          </Row>

          <Row gutter={24} type="flex" align="middle">
            <Col
              xs={ticoaux}
              sm={ticoaux}
              md={ticoaux}
              lg={ticoaux}
              xl={ticoaux}
              style={{ textAlign: "left" }}
            >
              Quiere incluir otro participante:
            </Col>
            <Col
              xs={12}
              sm={12}
              md={12}
              lg={12}
              xl={12}
              style={{ display: tiCos }}
              // style={{ display: histc }}
            >
              Parentesco del co-acreditado:
            </Col>
          </Row>
          <Row gutter={24} type="flex" align="middle">
            <Col
              xs={ticoaux}
              sm={ticoaux}
              md={ticoaux}
              lg={ticoaux}
              xl={ticoaux}
              style={colStyle}
            >
              <Form.Item>
                {getFieldDecorator("tieneCo", {
                  initialValue: tieneCo,
                  rules: [
                    {
                      required: true,
                      message: "Favor de responder"
                    }
                  ]
                })(
                  <RadioGroup
                    onChange={this.handleTieneCo}
                    setFieldsValue={tieneCo}
                    buttonStyle="solid"
                  >
                    <Row gutter={16}>
                      <Col xs={12}>
                        <Radio.Button style={{ width: "100%" }} value={"Si"}>
                          Si
                        </Radio.Button>
                      </Col>
                      <Col xs={12}>
                        <Radio.Button style={{ width: "100%" }} value={"No"}>
                          No
                        </Radio.Button>
                      </Col>
                    </Row>
                  </RadioGroup>
                )}
              </Form.Item>
            </Col>
            <Col
              xs={12}
              sm={12}
              md={12}
              lg={12}
              xl={12}
              style={{ display: tiCos }}
            >
              <Form.Item>
                {getFieldDecorator("tipoCoacreditado", {
                  initialValue: tipoCoacreditado,
                  rules: [
                    {
                      required: this.state.checkCo,
                      message: "Favor de responder"
                    }
                  ]
                })(
                  <Select
                    setFieldsValue={tipoCoacreditado}
                    onChange={handleChangeTiCo}
                    style={{ width: "100%" }}
                  >
                    {/* <Option value="Ninguno">Ninguno</Option> */}
                    <Option value="Cónyuge">Cónyuge</Option>
                    <Option value="Concubino(a)">Concubino(a)</Option>
                    <Option value="Hermano(a)">Hermano(a)</Option>
                    <Option value="Hijo(a)">Hijo(a)</Option>
                    <Option value="Padres">Padres</Option>
                  </Select>
                )}
              </Form.Item>
            </Col>
          </Row>
        </div>

        <TxtAling>
          <Row type="flex" justify="center" align="middle" gutter={16}>
            <Col xs={12} sm={6} md={4}>
              <Button disabled={true} className="btn-navigation">
                Atras
              </Button>
            </Col>
            <Col xs={12} sm={6} md={4}>
              <Button
                type="primary"
                htmlType="submit"
                className="btn-navigation"
              >
                Siguiente
              </Button>
            </Col>
          </Row>
        </TxtAling>
      </Form>
    );
  }
}

export default Form.create()(FirstStep);
