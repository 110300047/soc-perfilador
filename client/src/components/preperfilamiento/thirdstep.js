import React, { Component } from "react";
import {
  Row,
  Col,
  InputNumber,
  Input,
  Card,
  Button,
  Select,
  Form,
  Radio,
  Icon
} from "antd";
import { TxtAling } from "./preperfilamiento.style";
import { Link } from "react-router-dom";
import ModalSoc from "../../components/perfilamiento/modal";
import Fechas from "./fechas";

const Option = Select.Option;
const RadioGroup = Radio.Group;
class ThirdStep extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkHistory: false,
      checkPaPuCre: false,
      visible: false,
      doesntWork: false,
      controllerWork: true,
      completeDate: false,
      checkIngresos: false,
      visibleCo: false,
      hasCo: true,
      hasntCo: false,
      checkCoHistory: false,
      reqModal: false,
      checkBank: true
    };
  }

  handleAltaHac = e => {
    const { onChangeAltaHac } = this.props;
    onChangeAltaHac(e);
    if (e.target.value === "No") {
      this.setState(
        {
          visible: true,
          doesntWork: true,
          controllerWork: false
        }

        // () => {
        //   this.props.form.validateFields(
        //     ['PaPuCre',
        //     // 'feApCreBan',
        //     'DeInCuBa',
        //     'baPri',],
        //     { force: true }
        //   );
        // }
      );
    } else if (e.target.value === "Si") {
      this.setState({
        doesntWork: false,
        controllerWork: true
      });
    }
  };

  handleOk = e => {
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  checkDate = (day, month, year, dd, mm, aaaa) => {
    if (day == true && month == true && year == true) {
      // this.state.completeDate = true;

      this.props.form.setFieldsValue({
        feApCreBan: this.props.feApCreBan
        // errors: [new Error('forbid ha')],
        // },
      });
    }
    // else {
    // this.state.completeDate = false;
    // }
  };

  // CocheckDate = (day, month, year, dd, mm, aaaa) => {
  //   if((day == true) && (month == true) && (year == true)){
  //     // this.state.completeDate = true;

  //     this.props.form.setFieldsValue({
  //       CofeApCreBan: this.props.CofeApCreBan
  //         // errors: [new Error('forbid ha')],
  //       // },
  //     });

  //   }
  //   // else {
  //     // this.state.completeDate = false;
  //   // }

  // }
  checkDateAltaHac = (day, month, year, dd, mm, aaaa) => {
    if (day == true && month == true && year == true) {
      this.props.form.setFieldsValue({
        feAlta: this.props.feAlta
      });
    }
  };

  handleSubmit = e => {
    const {
      nextStep,
      feApCreBan,
      feAlta,
      CofeApCreBan,
      DeInCuBa,
      HiCre,
      baPri
    } = this.props;
    // const yolo = this.props.form.getFieldsValue();
    // console.log('Received values of form2: ', yolo);
    console.log("feApCreBan: ", feApCreBan);
    console.log("hicre: ", baPri);
    console.log("feAlta: ", feAlta);
    // console.log('funciona', feApCreBan);
    e.preventDefault();

    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        nextStep();
        window.scrollTo({
          top: 0,
          behavior: "smooth"
        });
      } else {
        console.log(err);
      }
    });
  };

  handleonChangeHiCre = e => {
    const { onChangeHiCre, EmFoPa } = this.props;
    onChangeHiCre(e);
    const status = e.target.value === "No" ? true : false;
    this.setState(
      {
        checkHistory: status
      },
      () => {
        this.props.form.validateFields(["EmFoPa"], { force: true });
      }
    );

    if (status == false) {
      this.props.form.setFieldsValue({
        EmFoPa: "",
        altaHac: "",
        feAlta: ""
      });

      this.setState({
        doesntWork: false,
        controllerWork: true
      });
    }
  };

  handlePaPuCre = e => {
    const { onChangePaPuCre } = this.props;
    onChangePaPuCre(e);
    const status = e.target.value === "No" ? true : false;
    this.setState(
      {
        checkPaPuCre: status
      },
      () => {
        this.props.form.validateFields(
          ["causaMontoMayor", "frecuencia", "montoMayorAprox"],
          { force: true }
        );
      }
    );
  };

  handleEmFoPa = e => {
    const { onChangeEmFoPa } = this.props;
    onChangeEmFoPa(e);
    const status = e.target.value === "No" ? true : false;
    this.setState(
      {
        reqModal: status
      },
      () => {
        this.props.form.validateFields(["altaHac", "feAlta"], { force: true });
      }
    );
    if (status == false) {
      this.props.form.setFieldsValue({
        altaHac: "",
        feAlta: ""
      });
      this.setState({
        doesntWork: false,
        controllerWork: true
      });
    }
  };

  handleDeInCuBa = e => {
    const { onChangeDeInCuBa } = this.props;
    onChangeDeInCuBa(e);
    const status = e.target.value === "Si" ? true : false;
    this.setState(
      {
        checkBank: status
      },
      () => {
        this.props.form.validateFields(["baPri"], { force: true });
      }
    );
  };
  addAtrasofields = e =>{
    alert("agregar campo");
  };
  removeAtrasofields = e =>{
    alert("remover campo");
  };
  // prueba = () => {
  //   const { feApCreBan } = this.props;
  //   console.log('funciona ' + feApCreBan);
  //   // this.setState({
  //   //   feApCreBan: fechaFull,
  //   // });
  //   // console.log("fecha"+ this.state.feApCreBan);
  // };

  render() {
    const colStyle = {
      marginBottom: "16px"
    };
    const { getFieldDecorator } = this.props.form;
    const {
      // handleChangeFeApCreBan,
      onChangeDeInCuBa,
      handleChangeBaPri,
      feApCreBan,
      // cumpleIngreso,

      // onChangeIngresoAprox,
      // ingresoAprox,
      HiCre,
      onChangeEmFoPa,
      // tercer paso alta Sac
      altaHac,
      feAlta,
      // onChangeAltaHac,
      fechaAltaHac,

      EmFoPa,
      PaPuCre,
      DeInCuBa,
      baPri,
      previousStep,
      // menSug,
      causaMontoMayor,
      causaMontoText,
      frecuencia,
      montoMayorAprox,
      onChangeCausaText,
      handleChangeCausaMontoMayor,
      handleChangeFrecuencia,
      onChangeMontoMayorAprox,
      fechaApertura
    } = this.props;

    const { doesntWork } = this.state;
    let histc,
      Cohistc,
      aux,
      aux2,
      modDisp = "none",
      Depo,
      disa = true,
      Codisa = "",
      // reqModal = false,
      disaCo = true,
      newIngresVal = false,
      poQue,
      coReq = false,
      coacredDDisplay = "none",
      causa = "none",
      newIngreso = "none";

    if (HiCre == "No") {
      histc = "";
      aux = 12;
    } else {
      histc = "none";
      aux = 24;
    }

    if (DeInCuBa == "No") {
      Depo = "";
    } else {
      disa = false;
      Depo = "none";
    }

    // if (cumpleIngreso == '') {
    //   disaCo = true;
    // } else {
    //   disaCo = false;
    // }

    if (causaMontoMayor == "Otro") {
      causa = "";
    } else {
      causa = "none";
    }

    if (EmFoPa == "No") {
      modDisp = "";
      // this.state.reqModal = true;
    } else {
      modDisp = "none";
      // this.state.reqModal = false;
    }

    if (PaPuCre == "No") {
      poQue = "";
    } else {
      poQue = "none";
    }

    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          {/* <Row type="flex" justify="center" align="middle">
        <Col xs={22} sm={18} md={16} lg={14} xl={12}> */}
          <h3 style={{ textAlign: "center" }}>
            Comprobación de ingresos en México
          </h3>
          <hr className="plecaAzul" />
          <Row gutter={24} type="flex" align="middle">
            <Col xs={aux} sm={aux} md={aux} lg={aux} xl={aux}>
              Cuenta con historial crediticio con al menos 6 meses:
            </Col>
            <Col
              xs={aux}
              sm={aux}
              md={aux}
              lg={aux}
              xl={aux}
              style={{ display: histc }}
            >
              Eres empleado:
            </Col>
          </Row>
          <Row gutter={24} type="flex" align="middle">
            <Col xs={9} sm={12} md={12} lg={12} xl={12}>
              <Form.Item>
                {getFieldDecorator("HiCre", {
                  initialValue: HiCre,
                  rules: [
                    {
                      required: true,
                      message: "Favor de responder"
                    }
                  ]
                })(
                  <RadioGroup
                    onChange={this.handleonChangeHiCre}
                    setFieldsValue={HiCre}
                    // className = "align-flex-xy-center d-md-block"
                  >
                    <Row gutter={16}>
                      <Col xs={12}>
                        <Radio.Button style={{ width: "100%" }} value={"Si"}>
                          Si
                        </Radio.Button>
                      </Col>
                      <Col xs={12}>
                        <Radio.Button style={{ width: "100%" }} value={"No"}>
                          No
                        </Radio.Button>
                      </Col>
                    </Row>
                  </RadioGroup>
                )}
              </Form.Item>
            </Col>
            <Col
              xs={aux}
              sm={aux}
              md={aux}
              lg={aux}
              xl={aux}
              style={{ display: histc, colStyle }}
            >
              <Form.Item>
                {getFieldDecorator("EmFoPa", {
                  // initialValue: EmFoPa,
                  rules: [
                    {
                      required: this.state.checkHistory,
                      message: "Favor de responder"
                    }
                  ]
                })(
                  <RadioGroup
                    onChange={this.handleEmFoPa}
                    // onChange={onChangeEmFoPa }
                    setFieldsValue={EmFoPa}
                  >
                    <Row gutter={16}>
                      <Col xs={12}>
                        <Radio.Button style={{ width: "100%" }} value={"Si"}>
                          Si
                        </Radio.Button>
                      </Col>
                      <Col xs={12}>
                        <Radio.Button style={{ width: "100%" }} value={"No"}>
                          No
                        </Radio.Button>
                      </Col>
                    </Row>
                  </RadioGroup>
                )}
              </Form.Item>
            </Col>
          </Row>
          <div style={{ display: modDisp }}>
            <Row gutter={24} type="flex" align="middle">
              <Col
                xs={9}
                sm={12}
                md={12}
                lg={12}
                xl={12}
                style={{ paddingRight: 0 }}
              >
                Cuenta con alta en hacienda:
              </Col>
              <Col xs={15} sm={12} md={12} lg={12} xl={12}>
                Fecha de alta en hacienda:
              </Col>
            </Row>
            <Row gutter={24} type="flex" align="middle">
              <Col xs={9} sm={12} md={12} lg={12} xl={12} style={colStyle}>
                <ModalSoc
                  title={"Perfil Rechazado"}
                  TextModal={
                    "No se puede continuar con la asesoria ya que no cuenta con un trabajo con forma de pago para corroborar sus ingresos"
                  }
                  visibleModal={this.state.visible}
                  handleOk={this.handleOk}
                  handleCancel={this.handleCancel}
                />
                <Form.Item>
                  {getFieldDecorator("altaHac", {
                    initialValue: altaHac,
                    rules: [
                      {
                        required:
                          this.state.reqModal && this.state.checkHistory,
                        message: "Favor de responder"
                      }
                    ]
                  })(
                    <RadioGroup
                      onChange={this.handleAltaHac}
                      // setFieldsValue={altaHac}
                    >
                      <Row gutter={16}>
                        <Col xs={12}>
                          <Radio.Button style={{ width: "100%" }} value={"Si"}>
                            Si
                          </Radio.Button>
                        </Col>
                        <Col xs={12}>
                          <Radio.Button style={{ width: "100%" }} value={"No"}>
                            No
                          </Radio.Button>
                        </Col>
                      </Row>
                    </RadioGroup>
                  )}
                </Form.Item>
              </Col>

              <Col
                xs={15}
                sm={12}
                md={12}
                lg={12}
                xl={12}
                // style={{ marginBottom: '16px', paddingRight: '0px' }}
              >
                {/* <h3>{exper}</h3> */}
                <Form.Item>
                  {getFieldDecorator("feAlta", {
                    // initialValue: feApCreBan,
                    rules: [
                      {
                        required:
                          !doesntWork &&
                          this.state.reqModal &&
                          this.state.checkHistory,
                        // required: (!this.state.completeDate),
                        message: "Favor ingresar fecha completa"
                      }
                    ]
                  })(
                    <Fechas
                      fecha={fechaAltaHac}
                      checkDate={this.checkDateAltaHac}
                      monthPicker={true}
                      disa={doesntWork}
                    />
                  )}
                </Form.Item>
              </Col>
            </Row>
          </div>

          <Row gutter={24} type="flex" align="middle">
            <Col
              xs={9}
              sm={12}
              md={12}
              lg={12}
              xl={12}
              style={{ paddingRight: 0 }}
            >
              Paga sus creditos a tiempo:
            </Col>
            <Col xs={15} sm={12} md={12} lg={12} xl={12}>
              Fecha de apertura de credito mas antiguo:
            </Col>
          </Row>
          <Row gutter={24} type="flex" align="middle">
            <Col xs={9} sm={12} md={12} lg={12} xl={12}>
              <Form.Item>
                {getFieldDecorator("PaPuCre", {
                  initialValue: PaPuCre,
                  rules: [
                    {
                      required:
                        this.state.controllerWork && !this.state.checkHistory,
                      message: "Favor de responder"
                    }
                  ]
                })(
                  <RadioGroup
                    onChange={this.handlePaPuCre}
                    disabled={doesntWork || this.state.checkHistory}
                  >
                    <Row gutter={16}>
                      <Col xs={12}>
                        <Radio.Button style={{ width: "100%" }} value={"Si"}>
                          Si
                        </Radio.Button>
                      </Col>
                      <Col xs={12}>
                        <Radio.Button style={{ width: "100%" }} value={"No"}>
                          No
                        </Radio.Button>
                      </Col>
                    </Row>
                  </RadioGroup>
                )}
              </Form.Item>
            </Col>

            <Col xs={15} sm={12} md={12} lg={12} xl={12}>
              <Form.Item>
                {getFieldDecorator("feApCreBan", {
                  rules: [
                    {
                      required:
                        this.state.controllerWork && !this.state.checkHistory,
                      // required: (!this.state.completeDate),
                      message: "Favor introducir fecha completa"
                    }
                  ]
                })(
                  // <MonthPicker
                  //   format={'MM/YYYY'}
                  //   disabled={doesntWork}
                  //   onChange={handleChangeFeApCreBan}
                  //   placeholder="Mes de apertura"
                  // />
                  <Fechas
                    fecha={fechaApertura}
                    checkDate={this.checkDate}
                    monthPicker={true}
                    disa={doesntWork || this.state.checkHistory}
                  />
                )}
              </Form.Item>

              {/* <Fechas 
              fecha={fechaApertura}
            /> */}
              {/* <Button onClick={this.prueba}>Prueba 1 Men</Button> */}
            </Col>
          </Row>
          <div style={{ display: poQue }}>
            <Row
              gutter={24}
              type="flex"
              align="middle"
              style={{ marginBottom: "1.5rem" }}
            >
              <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                {/* Cuenta con historial crediticio: */}
                <Card title="Especifica datos del atraso con el mayor monto">
                  <Row gutter={24} type="flex" align="middle">
                    <Col xs={24} sm={8}>
                      <Row>
                        <Col
                          xs={24}
                          sm={24}
                          // style={colStyle}
                        >
                          Proporciona causa:
                        </Col>
                        <Col
                          xs={24}
                          sm={24}
                          // style={colStyle}
                        >
                          <Form.Item>
                            {getFieldDecorator("causaMontoMayor", {
                              initialValue: causaMontoMayor,
                              rules: [
                                {
                                  required: this.state.checkPaPuCre,
                                  message: "Favor de responder"
                                }
                              ]
                            })(
                              <Select
                                onChange={handleChangeCausaMontoMayor}
                                // style={{ minWidth: '180px', maxWidth: '200px' }}
                              >
                                <Option value="flojera">
                                  Me dio mucha flojera
                                </Option>
                                <Option value="tiempo">
                                  Se me fue el tiempo
                                </Option>
                                <Option value="Dinero">Me falto dinero</Option>
                                <Option value="Otro">Otro</Option>
                              </Select>
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                    </Col>
                    <Col xs={24} sm={8}>
                      <Row gutter={24} type="flex" align="middle">
                        <Col
                          xs={24}
                          sm={24}
                          // style={colStyle}
                        >
                          Frecuencia:
                        </Col>
                        <Col
                          xs={24}
                          sm={24}
                          // style={colStyle}
                        >
                          <Form.Item>
                            {getFieldDecorator("frecuencia", {
                              initialValue: frecuencia,
                              rules: [
                                {
                                  required: this.state.checkPaPuCre,
                                  message: "Favor de responder"
                                }
                              ]
                            })(
                              <Select
                                onChange={handleChangeFrecuencia}
                                // style={{ minWidth: '180px', maxWidth: '200px' }}
                              >
                                <Option value="1">Una vez al año</Option>
                                <Option value="2">Dos veces al año</Option>
                                <Option value="3">
                                  Tres o más veces al año
                                </Option>
                              </Select>
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row
                        gutter={24}
                        style={{ display: causa }}
                        type="flex"
                        align="middle"
                      >
                        <Col
                          xs={24}
                          sm={24}
                          // style={colStyle}
                        >
                          Especifica:
                        </Col>
                        <Col
                          xs={24}
                          sm={24}
                          // style={colStyle}
                        >
                          <Form.Item>
                            {getFieldDecorator("causaMontoText", {
                              initialValue: causaMontoText,
                              rules: [
                                {
                                  required: false,
                                  message: "Favor de responder"
                                }
                              ]
                            })(
                              <Input
                                onChange={onChangeCausaText}
                                // style={{ minWidth: '180px', maxWidth: '200px' }}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                    </Col>
                    <Col xs={24} sm={8}>
                      <Row gutter={24} type="flex" align="middle">
                        <Col xs={24} sm={24}>
                          Monto aproximado:
                        </Col>
                        <Col xs={24} sm={24}>
                          <Form.Item>
                            {getFieldDecorator("montoMayorAprox", {
                              initialValue: montoMayorAprox,
                              rules: [
                                {
                                  required: this.state.checkPaPuCre,
                                  message: "Favor de responder"
                                }
                              ]
                            })(
                              <InputNumber
                                min={0}
                                max={50000}
                                // style={{ minWidth: '180px', maxWidth: '200px' }}
                                formatter={value =>
                                  `$ ${value}`.replace(
                                    /\B(?=(\d{3})+(?!\d))/g,
                                    ","
                                  )
                                }
                                parser={value =>
                                  value.replace(/\$\s?|(,*)/g, "")
                                }
                                onChange={onChangeMontoMayorAprox}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                    </Col>
                  </Row>                 
                </Card>
              </Col>
            </Row>
          </div>

          {/* fin de wea esa */}
          <Row gutter={24} type="flex" align="middle">
            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
              Deposita sus ingresos en alguna cuenta bancaria:
            </Col>
            <Col xs={9} sm={12} md={12} lg={12} xl={12}>
              En que banco:
            </Col>
          </Row>
          <Row gutter={24} type="flex" align="middle">
            <Col xs={12} sm={12} md={12} lg={12} xl={12} style={colStyle}>
              <Form.Item>
                {getFieldDecorator("DeInCuBa", {
                  initialValue: DeInCuBa,
                  rules: [
                    {
                      required: this.state.controllerWork,
                      message: "Favor de responder"
                    }
                  ]
                })(
                  <RadioGroup
                    onChange={this.handleDeInCuBa}
                    setFieldsValue={DeInCuBa}
                    disabled={doesntWork}
                  >
                    <Row gutter={16}>
                      <Col xs={12}>
                        <Radio.Button style={{ width: "100%" }} value={"Si"}>
                          Si
                        </Radio.Button>
                      </Col>
                      <Col xs={12}>
                        <Radio.Button style={{ width: "100%" }} value={"No"}>
                          No
                        </Radio.Button>
                      </Col>
                    </Row>
                  </RadioGroup>
                )}
              </Form.Item>
            </Col>
            <Col xs={9} sm={12} md={12} lg={12} xl={12}>
              <div gutter={16}>
                <Form.Item>
                  {getFieldDecorator("baPri", {
                    initialValue: baPri,
                    rules: [
                      {
                        required: this.state.checkBank,
                        message: "Favor de responder"
                      }
                    ]
                  })(
                    <Select
                      setFieldsValue={baPri}
                      onChange={handleChangeBaPri}
                      style={{ width: "100%", paddingBottom: "2.2rem" }}
                      disabled={disa || doesntWork}
                    >
                      <Option value="Afirme">Afirme</Option>
                      <Option value="Banamex">Banamex</Option>
                      <Option value="Bancomer">Bancomer</Option>
                      <Option value="Banorte">Banorte</Option>
                      <Option value="Banregio">Banregio</Option>
                      <Option value="HSBC">HSBC</Option>
                      <Option value="Santander">Santander</Option>
                      <Option value="Scotiabank">Scotiabank</Option>
                    </Select>
                  )}
                </Form.Item>
              </div>
            </Col>
          </Row>
          <div style={{ display: Depo }}>
            <Row gutter={24} type="flex" align="middle">
              <Col xs={24} sm={24} md={24} lg={24} xl={24} style={colStyle}>
                El banco no podra corroborar sus ingresos...
              </Col>
            </Row>
          </div>

          <TxtAling>
            <Row type="flex" justify="center" align="middle" gutter={16}>
              <Col xs={12} sm={6} md={3} lg={3} xl={3}>
                <Button
                  onClick={previousStep}
                  disabled={false}
                  className="btn-navigation"
                >
                  Atras
                </Button>
              </Col>
              <Col xs={12} sm={6} md={4}>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="btn-navigation"
                >
                  Siguiente
                </Button>
              </Col>
            </Row>
          </TxtAling>

          {/* </Col>
        </Row> */}
        </Form>
      </div>
    );
  }
}

export default Form.create()(ThirdStep);
