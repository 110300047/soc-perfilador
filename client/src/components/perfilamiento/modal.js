import React from 'react';
import { Modal, Button, Row, Col } from 'antd';

const ModalSoc = ({
  title,
  TextModal,
  visibleModal,
  handleOk,
  handleCancel,
}) => (
  <div>
    <Modal
      title={title}
      visible={visibleModal}
      footer={null}
      onCancel={handleCancel}
    >
      <p>{TextModal}</p>
      <Row type="flex" justify="end">
        <Col span={4}>
          <Button onClick={handleOk}>Acceptar</Button>
        </Col>
      </Row>
    </Modal>
  </div>
);

export default ModalSoc;
